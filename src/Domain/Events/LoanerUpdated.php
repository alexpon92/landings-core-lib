<?php

namespace LandingsCore\Domain\Events;

use Carbon\Carbon;

class LoanerUpdated
{
    /**
     * @var string
     */
    private $sessionId;

    /**
     * @var null|string
     */
    private $utmSource;

    /**
     * @var null|string
     */
    private $utmCampaign;

    /**
     * @var null|string
     */
    private $utmMedium;

    /**
     * @var null|string
     */
    private $phone;

    /**
     * @var null|string
     */
    private $email;

    /**
     * @var null|string
     */
    private $agreementAcceptedAt;

    /**
     * @var string|null
     */
    private $firstName;
    /**
     * @var string|null
     */
    private $lastName;
    /**
     * @var string|null
     */
    private $patronymic;
    /**
     * @var string|null
     */
    private $birthday;
    /**
     * @var string|null
     */
    private $gender;
    /**
     * @var int|null
     */
    private $loanSum;
    /**
     * @var string|null
     */
    private $loanAim;
    /**
     * @var string|null
     */
    private $passportSerial;
    /**
     * @var string|null
     */
    private $passportNumber;
    /**
     * @var string|null
     */
    private $passportIssuedBy;
    /**
     * @var string|null
     */
    private $passportDepCode;
    /**
     * @var
     */
    private $birthPlace;
    /**
     * @var string|null
     */
    private $idCardNumber;
    /**
     * @var string|null
     */
    private $idCardExpireDate;
    /**
     * @var string|null
     */
    private $inn;
    /**
     * @var string|null
     */
    private $region;
    /**
     * @var string|null
     */
    private $city;
    /**
     * @var string|null
     */
    private $street;
    /**
     * @var string|null
     */
    private $house;
    /**
     * @var string|null
     */
    private $flat;
    /**
     * @var string|null
     */
    private $regionFact;
    /**
     * @var string|null
     */
    private $cityFact;
    /**
     * @var string|null
     */
    private $streetFact;
    /**
     * @var string|null
     */
    private $houseFact;
    /**
     * @var string|null
     */
    private $flatFact;
    /**
     * @var string|null
     */
    private $creditHistory;
    /**
     * @var string|null
     */
    private $employment;
    /**
     * @var string|null
     */
    private $bankCardToken;
    /**
     * @var string|null
     */
    private $bankCardFirstSix;
    /**
     * @var string|null
     */
    private $bankCardLastFour;

    /**
     * @var string|null
     */
    private $tokenCreatedAt;

    /**
     * @var array|null
     */
    private $queryArr;

    /**
     * @var int|null
     */
    private $formStep;

    /**
     * @var array
     */
    private $rawPayload;

    /**
     * LoanerUpdated constructor.
     *
     * @param string $sessionId
     * @param array  $payload
     */
    public function __construct(string $sessionId, array $payload)
    {
        $this->rawPayload = $payload;

        $this->sessionId   = $sessionId;
        $this->utmCampaign = $payload['utm_campaign'] ?? null;
        $this->utmMedium   = $payload['utm_medium'] ?? null;
        $this->utmSource   = $payload['utm_source'] ?? null;
        $this->queryArr    = $payload['query_arr'] ?? null;

        $this->formStep = $payload['current_step'] ?? null;

        //step 1
        $this->phone               = $payload['lead']['phone'] ?? null;
        $this->email               = $payload['lead']['email'] ?? null;
        if (!empty($payload['lead']['agreement'])) {
            $aggrDate = Carbon::createFromFormat('Y-m-d H:i:sO', $payload['lead']['agreement']);
            $this->agreementAcceptedAt = $aggrDate->format('Y-m-d H:i:sO');
        } else {
            $this->agreementAcceptedAt = null;
        }

        //PERSONAL DATA
        $this->firstName  = $payload['lead']['first_name'] ?? null;
        $this->lastName   = $payload['lead']['last_name'] ?? null;
        $this->patronymic = $payload['lead']['patronymic'] ?? null;
        $this->birthday   = $payload['lead']['birthday'] ?? null;
        $this->gender     = $payload['lead']['gender'] ?? null;
        $this->loanAim    = $payload['lead']['loan_aim'] ?? null;
        if (isset($payload['lead']['loan_sum']) && !empty($payload['lead']['loan_sum'])) {
            $sum = preg_replace('/[^0-9]/', '', $payload['lead']['loan_sum']);
            if ($sum && is_numeric($sum)) {
                $this->loanSum = (int) $sum;
            }
        }

        //PASSPORT DATA
        $this->passportNumber   = $payload['lead']['passport_number'] ?? null;
        $this->passportSerial   = $payload['lead']['passport_serial'] ?? null;
        $this->passportDepCode  = $payload['lead']['passport_dep_code'] ?? null;
        $this->passportIssuedBy = $payload['lead']['passport_issued_by'] ?? null;
        $this->birthPlace       = $payload['lead']['birth_place'] ?? null;
        $this->idCardNumber     = $payload['lead']['id_card_number'] ?? null;
        $this->idCardExpireDate = $payload['lead']['id_card_expire_date'] ?? null;
        $this->inn              = $payload['lead']['inn'] ?? null;
        $this->creditHistory    = $payload['lead']['credit_history'] ?? null;
        $this->employment       = $payload['lead']['employment'] ?? null;


        //ADDRESS DATA
        $this->region = $payload['lead']['region'] ?? null;
        $this->city   = $payload['lead']['city'] ?? null;
        $this->street = $payload['lead']['street'] ?? null;
        $this->house  = $payload['lead']['house'] ?? null;
        $this->flat   = $payload['lead']['flat'] ?? null;

        $this->regionFact = $payload['lead']['region_fact'] ?? null;
        $this->cityFact   = $payload['lead']['city_fact'] ?? null;
        $this->streetFact = $payload['lead']['street_fact'] ?? null;
        $this->houseFact  = $payload['lead']['house_fact'] ?? null;
        $this->flatFact   = $payload['lead']['flat_fact'] ?? null;


        //CARD DATA
        $this->bankCardToken    = $payload['lead']['bank_card_token'] ?? null;
        $this->bankCardFirstSix = $payload['lead']['bank_card_first_six'] ?? null;
        $this->bankCardLastFour = $payload['lead']['bank_card_last_four'] ?? null;

        if (!empty($payload['lead']['bank_card_token_created_at'])) {
            $tokenDate            =
                Carbon::createFromFormat('Y-m-d H:i:sO', $payload['lead']['bank_card_token_created_at']);
            $this->tokenCreatedAt = $tokenDate->format('Y-m-d H:i:sO');
        } else {
            $this->tokenCreatedAt = null;
        }
    }

    /**
     * @return string
     */
    public function getSessionId(): string
    {
        return $this->sessionId;
    }

    /**
     * @return string|null
     */
    public function getUtmSource(): ?string
    {
        return $this->utmSource;
    }

    /**
     * @return string|null
     */
    public function getUtmCampaign(): ?string
    {
        return $this->utmCampaign;
    }

    /**
     * @return string|null
     */
    public function getUtmMedium(): ?string
    {
        return $this->utmMedium;
    }

    /**
     * @return string|null
     */
    public function getPhone(): ?string
    {
        return $this->phone;
    }

    /**
     * @return string|null
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * @return string|null
     */
    public function getAgreementAcceptedAt(): ?string
    {
        return $this->agreementAcceptedAt;
    }

    /**
     * @return string|null
     */
    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    /**
     * @return string|null
     */
    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    /**
     * @return string|null
     */
    public function getPatronymic(): ?string
    {
        return $this->patronymic;
    }

    /**
     * @return string|null
     */
    public function getBirthday(): ?string
    {
        return $this->birthday;
    }

    /**
     * @return string|null
     */
    public function getGender(): ?string
    {
        return $this->gender;
    }

    /**
     * @return int|null
     */
    public function getLoanSum(): ?int
    {
        $sum = preg_replace('/[^0-9]/', '', $this->loanSum);
        if ($sum && is_numeric($sum)) {
            return (int)$sum;
        }
        return null;
    }

    /**
     * @return string|null
     */
    public function getLoanAim(): ?string
    {
        return $this->loanAim;
    }

    /**
     * @return string|null
     */
    public function getPassportSerial(): ?string
    {
        return $this->passportSerial;
    }

    /**
     * @return string|null
     */
    public function getPassportNumber(): ?string
    {
        return $this->passportNumber;
    }

    /**
     * @return string|null
     */
    public function getIdCardNumber(): ?string
    {
        return $this->idCardNumber;
    }

    /**
     * @return string|null
     */
    public function getIdCardExpireDate(): ?string
    {
        return $this->idCardExpireDate;
    }

    /**
     * @return string|null
     */
    public function getInn(): ?string
    {
        return $this->inn;
    }

    /**
     * @return string|null
     */
    public function getRegion(): ?string
    {
        return $this->region;
    }

    /**
     * @return string|null
     */
    public function getCity(): ?string
    {
        return $this->city;
    }

    /**
     * @return string|null
     */
    public function getCreditHistory(): ?string
    {
        return $this->creditHistory;
    }

    /**
     * @return string|null
     */
    public function getEmployment(): ?string
    {
        return $this->employment;
    }

    /**
     * @return string|null
     */
    public function getBankCardToken(): ?string
    {
        return $this->bankCardToken;
    }

    /**
     * @return string|null
     */
    public function getBankCardFirstSix(): ?string
    {
        return $this->bankCardFirstSix;
    }

    /**
     * @return string|null
     */
    public function getBankCardLastFour(): ?string
    {
        return $this->bankCardLastFour;
    }

    /**
     * @return string|null
     */
    public function getTokenCreatedAt(): ?string
    {
        return $this->tokenCreatedAt;
    }

    /**
     * @return array|null
     */
    public function getQueryArr(): ?array
    {
        return $this->queryArr;
    }

    /**
     * @return string|null
     */
    public function getStreet(): ?string
    {
        return $this->street;
    }

    /**
     * @return string|null
     */
    public function getHouse(): ?string
    {
        return $this->house;
    }

    /**
     * @return string|null
     */
    public function getFlat(): ?string
    {
        return $this->flat;
    }

    /**
     * @return string|null
     */
    public function getRegionFact(): ?string
    {
        return $this->regionFact;
    }

    /**
     * @return string|null
     */
    public function getCityFact(): ?string
    {
        return $this->cityFact;
    }

    /**
     * @return string|null
     */
    public function getStreetFact(): ?string
    {
        return $this->streetFact;
    }

    /**
     * @return string|null
     */
    public function getHouseFact(): ?string
    {
        return $this->houseFact;
    }

    /**
     * @return string|null
     */
    public function getFlatFact(): ?string
    {
        return $this->flatFact;
    }

    /**
     * @return string|null
     */
    public function getPassportIssuedBy(): ?string
    {
        return $this->passportIssuedBy;
    }

    /**
     * @return string|null
     */
    public function getPassportDepCode(): ?string
    {
        return $this->passportDepCode;
    }

    /**
     * @return mixed
     */
    public function getBirthPlace()
    {
        return $this->birthPlace;
    }

    /**
     * @return int|null
     */
    public function getFormStep(): ?int
    {
        return $this->formStep;
    }

    /**
     * @return array
     */
    public function getRawPayload(): array
    {
        return $this->rawPayload;
    }
}