<?php
declare(strict_types=1);

namespace LandingsCore\Domain\Services;


use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Str;

class AssetManager
{
    private const KEY = 'broker_asset_version';

    public static function getVersion(): string
    {
        return Cache::get(self::KEY) ?? '__Version';
    }

    public static function changeVersion(): void
    {
        Cache::forever(self::KEY, Str::random(8));
    }
}