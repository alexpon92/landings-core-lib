<?php
declare(strict_types=1);

namespace LandingsCore\Console\Commands;

use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Collection;
use LandingsCore\Domain\Entity\Session;
use LandingsCore\Domain\Events\LoanerUpdated;

class ResendSessions extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'core:synch-sessions {--date=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Synchronize sessions with core';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }


    public function handle()
    {
        $date = $this->option('date');
        try {
            $date = Carbon::createFromFormat('Y-m-d', $date);
        } catch (\Throwable $e) {
            $this->error($e->getMessage());
            return;
        }

        $this->info('Getting sessions...');

        $sessions = Session::select('sessions.*')
            ->rightJoin(
                'loaner_payouts',
                'loaner_payouts.session_id',
                '=',
                'sessions.id'
            )
            ->whereRaw("loaner_payouts.created_at::date = '{$date->format('Y-m-d')}'")
            ->get();

        if ($sessions->isEmpty()) {
            return;
        }

        $this->info('Sending updates...');

        foreach ($sessions as $session) {
            try {
                if (empty($session->payload)) {
                    event(new LoanerUpdated($session->id, $session->getPayloadAsArray()));
                }
            } catch (\Throwable $exception) {
                $this->warn("Session {$session->id}. Exception: " . $exception->getMessage());
            }
        }
    }
}