<?php
declare(strict_types=1);

namespace LandingsCore\Domain\CorePackage\CoreClient\StoreLoaner;

class PersonalDataDto
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $hash;

    /**
     * PersonalDataDto constructor.
     *
     * @param int    $id
     * @param string $hash
     */
    public function __construct(int $id, string $hash)
    {
        $this->id   = $id;
        $this->hash = $hash;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getHash(): string
    {
        return $this->hash;
    }
}