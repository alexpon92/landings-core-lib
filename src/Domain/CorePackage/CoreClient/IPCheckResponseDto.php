<?php

declare(strict_types=1);

namespace LandingsCore\Domain\CorePackage\CoreClient;

class IPCheckResponseDto
{
    private string  $ipAddress;
    private ?string $city;
    private ?string $country;
    private ?string $countryCode;
    private ?string $lat;
    private ?string $lon;
    private ?string $region;
    private ?string $regionName;
    private ?string $timezone;

    /**
     * IPCheckResponseDto constructor.
     *
     * @param string      $ipAddress
     * @param string|null $city
     * @param string|null $country
     * @param string|null $countryCode
     * @param string|null $lat
     * @param string|null $lon
     * @param string|null $region
     * @param string|null $regionName
     * @param string|null $timezone
     */
    public function __construct(
        string $ipAddress,
        ?string $city,
        ?string $country,
        ?string $countryCode,
        ?string $lat,
        ?string $lon,
        ?string $region,
        ?string $regionName,
        ?string $timezone
    ) {
        $this->ipAddress   = $ipAddress;
        $this->city        = $city;
        $this->country     = $country;
        $this->countryCode = $countryCode;
        $this->lat         = $lat;
        $this->lon         = $lon;
        $this->region      = $region;
        $this->regionName  = $regionName;
        $this->timezone    = $timezone;
    }

    /**
     * @return string
     */
    public function getIpAddress(): string
    {
        return $this->ipAddress;
    }

    /**
     * @return string|null
     */
    public function getCity(): ?string
    {
        return $this->city;
    }

    /**
     * @return string|null
     */
    public function getCountry(): ?string
    {
        return $this->country;
    }

    /**
     * @return string|null
     */
    public function getCountryCode(): ?string
    {
        return $this->countryCode;
    }

    /**
     * @return string|null
     */
    public function getLat(): ?string
    {
        return $this->lat;
    }

    /**
     * @return string|null
     */
    public function getLon(): ?string
    {
        return $this->lon;
    }

    /**
     * @return string|null
     */
    public function getRegion(): ?string
    {
        return $this->region;
    }

    /**
     * @return string|null
     */
    public function getRegionName(): ?string
    {
        return $this->regionName;
    }

    /**
     * @return string|null
     */
    public function getTimezone(): ?string
    {
        return $this->timezone;
    }
}