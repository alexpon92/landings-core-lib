<?php
declare(strict_types=1);

namespace Tests;

use Illuminate\Foundation\Testing\RefreshDatabase;
use LandingsCore\LandingsCoreServiceProvider;
use Orchestra\Testbench\TestCase as BaseTestCase;

class TestCase extends BaseTestCase
{
    use RefreshDatabase;

    protected function getEnvironmentSetUp($app): void
    {
        parent::getEnvironmentSetUp($app);

        $app['config']->set('landings-core-lib.project-type', 'broker');
    }

    protected function getPackageProviders($app): array
    {
        return [
            LandingsCoreServiceProvider::class,
        ];
    }
}
