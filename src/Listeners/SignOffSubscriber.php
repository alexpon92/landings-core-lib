<?php
declare(strict_types=1);

namespace LandingsCore\Listeners;

use LandingsCore\Domain\CorePackage\CoreClient\ClientExceptions\BadResponseCode;
use LandingsCore\Domain\CorePackage\CoreClient\ClientExceptions\BadResponseFormat;
use LandingsCore\Domain\CorePackage\CoreClient\ICoreClient;
use LandingsCore\Domain\CorePackage\Enums\SignOffTypeEnum;
use LandingsCore\Domain\Entity\LeadSignOutAttempt;
use LandingsCore\Domain\Events\LoanerSignOffAllPaid;
use LandingsCore\Domain\Events\LoanerSignOffBadPayments;
use LandingsCore\Domain\Events\LoanerSignOffCore;
use LandingsCore\Domain\Events\LoanerSignOffSelf;
use Psr\Log\LoggerInterface;

class SignOffSubscriber extends AbstractListener
{
    /**
     * @var ICoreClient
     */
    private $coreClient;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * LoanerSubscriber constructor.
     *
     * @param ICoreClient     $coreClient
     * @param LoggerInterface $logger
     */
    public function __construct(ICoreClient $coreClient, LoggerInterface $logger)
    {
        $this->coreClient = $coreClient;
        $this->logger     = $logger;
    }

    public function onSignOffBadPayments(LoanerSignOffBadPayments $event): void
    {
        $this->coreClient->signOffLoaner(
            $event->getPhone(),
            SignOffTypeEnum::TYPE_BAD_PAYMENTS
        );
        LeadSignOutAttempt::createBulkByPhones([$event->getPhone()]);
    }

    /**
     * @param LoanerSignOffSelf $event
     *
     * @throws BadResponseCode
     * @throws BadResponseFormat
     */
    public function onSignOffSelf(LoanerSignOffSelf $event): void
    {
        $this->coreClient->signOffLoaner(
            $event->getPhone(),
            SignOffTypeEnum::TYPE_CLIENT_INITIATIVE
        );
        LeadSignOutAttempt::createBulkByPhones([$event->getPhone()]);
    }

    public function onSignOffCore(LoanerSignOffCore $event): void
    {
        LeadSignOutAttempt::createBulkByPhones([$event->getPhone()]);
    }

    public function onSignOffAllPaid(LoanerSignOffAllPaid $event): void
    {
        $this->coreClient->signOffLoaner(
            $event->getPhone(),
            SignOffTypeEnum::TYPE_ALL_PAID
        );
        LeadSignOutAttempt::createBulkByPhones([$event->getPhone()]);
    }

    public function getConnection(): string
    {
        return (string)config('landings-core-lib.loaners_sign_off_queue.connection');
    }

    public function getQueue(): string
    {
        return (string)config('landings-core-lib.loaners_sign_off_queue.name');
    }

    /**
     * Register the listeners for the subscriber.
     *
     * @param \Illuminate\Events\Dispatcher $events
     */
    public function subscribe($events)
    {
        $events->listen(
            LoanerSignOffBadPayments::class,
            self::class . '@onSignOffBadPayments'
        );

        $events->listen(
            LoanerSignOffSelf::class,
            self::class . '@onSignOffSelf'
        );

        $events->listen(
            LoanerSignOffCore::class,
            self::class . '@onSignOffCore'
        );

        $events->listen(
            LoanerSignOffAllPaid::class,
            self::class . '@onSignOffAllPaid'
        );
    }
}