<?php
declare(strict_types=1);

namespace LandingsCore\Domain\Services;

use LandingsCore\Domain\Entity\LeadSignOutAttempt;
use LandingsCore\Domain\Entity\Session;
use LandingsCore\Domain\Events\LoanerSignOffCore;

class SignOffLoanerService
{
    /**
     * @param array $phones
     */
    public function signOffBulk(array $phones): void
    {
        if (!empty($phones)) {
            foreach ($phones as $phone) {
                event(new LoanerSignOffCore($phone));
            }
        }
    }

    /**
     * @param string $phone
     */
    public function signOffSingle(string $phone): void
    {
        $this->signOffBulk([$phone]);
    }
}