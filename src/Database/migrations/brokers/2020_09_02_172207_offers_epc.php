<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class OffersEpc extends Migration
{
    /**
     * {@inheritDoc}
     */
    public function up(): void
    {
        Schema::table('offers', function (Blueprint $table) {
            $table->float('epc')->nullable();
        });
    }

    /**
     * {@inheritDoc}
     */
    public function down(): void
    {
        Schema::table('offers', function (Blueprint $table) {
            $table->dropColumn('epc');
        });
    }
}
