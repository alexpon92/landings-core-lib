<?php
declare(strict_types=1);

namespace LandingsCore\Http\Controllers\Api\Loaners;

use Illuminate\Routing\Controller;
use LandingsCore\Domain\Entity\LeadCardLink;
use LandingsCore\Domain\Entity\Session;
use LandingsCore\Domain\Services\LoanersApi\ApiProcessor\Exceptions\MapperNotFoundException;
use LandingsCore\Domain\Services\LoanersApi\ApiProcessor\LoanerApiProcessor;
use LandingsCore\Domain\Services\LoanersApi\Validators\RulesRepository;
use Illuminate\Http\Request;
use Psr\Log\LoggerInterface;
use Validator;

class LoanerApiController extends Controller
{
    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * LoanerApiController constructor.
     *
     * @param LoggerInterface $logger
     */
    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    public function storeLoaner(
        Request $request,
        RulesRepository $rulesRepository,
        LoanerApiProcessor $apiProcessor
    ) {
        $header = $request->header('auth-key');
        if ($header !== config('landings-core-lib.loaners_api.auth_key')) {
            $this->logger->warning(
                'Request to store loaner via api with incorrect header',
                [
                    'request'  => $request->all(),
                    'auth-key' => $header
                ]
            );
            return response('', 403);
        }

        $ruleSet = $rulesRepository->findByGeo(config('landings-core-lib.loaners_api.geo'));
        if (!$ruleSet) {
            $this->logger->warning(
                'Geo Validation rules for geo is not found',
                [
                    'request' => $request->all(),
                    'geo'     => config('loaners_api.geo')
                ]
            );
            return response('', 406);
        }

        $validator = Validator::make(
            $request->all(),
            array_merge(
                [
                    'loaner'         => 'required|array',
                    'source'         => 'required|string|min:1|max:100',
                    'web'            => 'required|string|min:1|max:100',
                    'application_id' => 'required|string|min:1|max:100',
                ],
                $ruleSet->getRules()
            )
        );

        if ($validator->fails()) {
            return response()->json(
                [
                    'errors' => $validator->errors()
                ],
                422
            );
        }

        $loanerData = $request->get('loaner');
        if (!isset($loanerData['phone'])) {
            return response()->json(
                [
                    'errors' => [
                        'loaner.phone' => [
                            'validation.required'
                        ]
                    ]
                ]
            );
        }

        $link = LeadCardLink::findByPhone($loanerData['phone']);
        if ($link) {
            return response('', 409);
        }

        $session = Session::getByPhone($loanerData['phone']);
        if ($session->isNotEmpty()) {
            return response('', 409);
        }

        try {
            $apiProcessor->execute(
                $loanerData,
                $request->get('source'),
                $request->get('web'),
                $request->get('application_id'),
                config('landings-core-lib.loaners_api.geo')
            );
        } catch (MapperNotFoundException $e) {
            return response('', 406);
        }

        return response('', 202);
    }
}