<?php

declare(strict_types=1);

namespace LandingsCore\Domain\Services\Validators;

use Carbon\Carbon;
use LandingsCore\Domain\Entity\RuRegionCode;

class RuPassportValidator
{

    public function checkPassportSeria($attribute, $value, $parameters)
    {
        if (!$value) {
            return false;
        }

        [$okatoCode, $printYear] = explode(' ', (string) $value);
        $existsOkato = RuRegionCode::checkOkatoCode($okatoCode);

        if (!$okatoCode || !$printYear) {
            return false;
        }

        $twentyCenturyYears = [97, 98, 99];
        $currentYear        = (int) date('y');
        $requestYear        = (int) $printYear;

        $correctPrintYear = ($requestYear <= $currentYear) || \in_array($requestYear, $twentyCenturyYears, true);

        return $existsOkato && $correctPrintYear;
    }

    public function checkPassportNumber($attribute, $value, $parameters)
    {
        if (!$value) {
            return false;
        }

        $number    = (int) str_replace(' ', '', $value);
        $minNumber = '000101';
        $maxNumber = '999999';

        return ($number >= (int) $minNumber) && ($number <= (int) $maxNumber);
    }

    public function checkPassportDate($attribute, $value, $parameters)
    {
        if (!$value) {
            return false;
        }

        try {
            $passportDate = Carbon::createFromFormat($parameters[0], $value)->startOfDay();
        } catch (\Throwable $exception) {
            return false;
        }

        return $passportDate >= Carbon::createFromFormat('Y-m-d', '1997-01-01')->startOfDay()
            && $passportDate < now()->endOfDay();
    }

    public function checkAge($attribute, $value, $parameters)
    {
        if (!$value) {
            return false;
        }

        try {
            $birthday = Carbon::createFromFormat($parameters[0], $value);
        } catch (\Throwable $exception) {
            return false;
        }

        return ($birthday->age >= 16) && ($birthday->age <= 75);
    }

    public function checkPassportDepCode($attribute, $value, $parameters)
    {
        if (!$value) {
            return false;
        }

        [$firstPart, $secondPart] = explode('-', (string) $value);
        $organisationLevels = [0, 1, 2, 3];

        if (!$firstPart || !$secondPart) {
            return false;
        }

        $gibddCode         = mb_substr($firstPart, 0, 2);
        $organisationLevel = (int) mb_substr($firstPart, 2, 1);

        $existsGibdd = RuRegionCode::checkGibddCode($gibddCode);

        return $existsGibdd && \in_array($organisationLevel, $organisationLevels);
    }
}
