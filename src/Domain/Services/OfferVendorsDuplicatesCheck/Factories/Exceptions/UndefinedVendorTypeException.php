<?php
declare(strict_types=1);

namespace LandingsCore\Domain\Services\OfferVendorsDuplicatesCheck\Factories\Exceptions;

class UndefinedVendorTypeException extends \Exception
{

}