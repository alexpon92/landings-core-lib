<?php
declare(strict_types=1);

namespace LandingsCore\Domain\Services\LoanersApi\LoanerDataMappers\Mappers;

use Carbon\Carbon;
use LandingsCore\Domain\Entity\Session;
use LandingsCore\Domain\Services\LoanersApi\LoanerDataMappers\IMapper;

class UaDataMapper implements IMapper
{
    public function map(array $data, Session $session): Session
    {
        $payload = [];
        if (isset($data['phone'])) {
            $session->phone = $data['phone'];
            $payload['lead.phone'] = $data['phone'];
        }

        if (isset($data['email'])) {
            $payload['lead.email'] = $data['email'];
        }

        if (isset($data['last_name'])) {
            $payload['lead.last_name'] = $data['last_name'];
        }

        if (isset($data['first_name'])) {
            $payload['lead.first_name'] = $data['first_name'];
        }

        if (isset($data['patronymic'])) {
            $payload['lead.patronymic'] = $data['patronymic'];
        }

        if (isset($data['birthday'])) {
            $payload['lead.birthday'] = $data['birthday'];
        }

        if (isset($data['loan_sum'])) {
            $payload['lead.loan_sum'] = $data['loan_sum'];
        }

        if (isset($data['passport_number'])) {
            [$serial, $number] = explode('/', $data['passport_number']);
            $payload['lead.passport_serial'] = $serial;
            $payload['lead.passport_number'] = $number;
        }

        if (isset($data['id_card_number'])) {
            $payload['lead.id_card_number'] = $data['id_card_number'];
        }

        if (isset($data['id_card_expire_date'])) {
            $payload['lead.id_card_expire_date'] = $data['id_card_expire_date'];
        }

        if (isset($data['inn'])) {
            $payload['lead.inn'] = $data['inn'];
        }

        if (isset($data['region'])) {
            $payload['lead.region'] = $data['region'];
        }

        if (isset($data['city'])) {
            $payload['lead.city'] = $data['city'];
        }

        $session->addToPayload($payload);

        return $session;
    }
}