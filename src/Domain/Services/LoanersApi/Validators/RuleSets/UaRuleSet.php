<?php
declare(strict_types=1);

namespace LandingsCore\Domain\Services\LoanersApi\Validators\RuleSets;

use LandingsCore\Domain\Services\LoanersApi\Validators\IRuleSet;

class UaRuleSet implements IRuleSet
{
    public function getRules(): array
    {
        return [
            'loaner.phone'               => [
                'required',
                'regex:/^\+380[0-9]{9}$/'
            ],
            'loaner.email'               => [
                'nullable',
                'string',
                'email',
                'max:255',
                'min:5'
            ],
            'loaner.last_name'           => [
                'nullable',
                'string',
                'min:2'
            ],
            'loaner.first_name'          => [
                'nullable',
                'string',
                'min:2'
            ],
            'loaner.patronymic'          => [
                'nullable',
                'string',
                'min:2'
            ],
            'loaner.birthday'            => [
                'nullable',
                'string',
                'date_format:d.m.Y'
            ],
            'loaner.loan_sum'            => [
                'nullable',
                'integer'
            ],
            'loaner.passport_number'     => [
                'nullable',
                'regex:/^[a-zA-Zа-яА-ЯйЙёЁ]{2}\/[0-9]{6}$/u'
            ],
            'loaner.id_card_number'      => [
                'nullable',
                'regex:/^[0-9]{9}$/'
            ],
            'loaner.id_card_expire_date' => [
                'nullable',
                'date_format:d.m.Y'
            ],
            'loaner.inn'                 => [
                'nullable',
                'regex:/^[0-9]{10}$/'
            ],
            'loaner.region'              => [
                'nullable',
                'string',
                'max:100',
                'min:3'
            ],
            'loaner.city'                => [
                'nullable',
                'string',
                'max:100',
                'min:3'
            ],
        ];
    }

    public function getGeoKey(): string
    {
        return 'UA';
    }

}