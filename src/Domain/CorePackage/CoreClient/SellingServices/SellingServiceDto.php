<?php
declare(strict_types=1);

namespace LandingsCore\Domain\CorePackage\CoreClient\SellingServices;


class SellingServiceDto
{
    /**
     * @var int
     */
    private $externalId;

    /**
     * @var string
     */
    private $type;

    /**
     * @var string|null
     */
    private $name;

    /**
     * @var string|null
     */
    private $description;

    /**
     * @var string|null
     */
    private $logoUrl;

    /**
     * SellingServiceDto constructor.
     * @param int         $externalId
     * @param string      $type
     * @param string|null $name
     * @param string|null $description
     * @param string|null $logoUrl
     */
    public function __construct(
        int $externalId,
        string $type,
        ?string $name,
        ?string $description,
        ?string $logoUrl
    ) {
        $this->externalId  = $externalId;
        $this->type        = $type;
        $this->name        = $name;
        $this->description = $description;
        $this->logoUrl     = $logoUrl;
    }

    /**
     * @return int
     */
    public function getExternalId(): int
    {
        return $this->externalId;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @return string|null
     */
    public function getLogoUrl(): ?string
    {
        return $this->logoUrl;
    }

}