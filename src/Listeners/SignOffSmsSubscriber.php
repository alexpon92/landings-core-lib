<?php
declare(strict_types=1);

namespace LandingsCore\Listeners;

use LandingsCore\Domain\CorePackage\CoreClient\ICoreClient;
use LandingsCore\Domain\Events\LoanerSignOffSms;
use Psr\Log\LoggerInterface;

class SignOffSmsSubscriber extends AbstractListener
{
    /**
     * @var ICoreClient
     */
    private $coreClient;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * LoanerSubscriber constructor.
     *
     * @param ICoreClient     $coreClient
     * @param LoggerInterface $logger
     */
    public function __construct(ICoreClient $coreClient, LoggerInterface $logger)
    {
        $this->coreClient = $coreClient;
        $this->logger     = $logger;
    }

    public function onSmsSignOff(LoanerSignOffSms $event)
    {
        $this->coreClient->signOffLoanerSmsSubscription($event->getPhone());
    }

    public function getConnection(): string
    {
        return (string)config('landings-core-lib.loaner_sign_off_sms_queue.connection');
    }

    public function getQueue(): string
    {
        return (string)config('landings-core-lib.loaner_sign_off_sms_queue.name');
    }

    /**
     * Register the listeners for the subscriber.
     *
     * @param \Illuminate\Events\Dispatcher $events
     */
    public function subscribe($events)
    {
        $events->listen(
            LoanerSignOffSms::class,
            self::class . '@onSmsSignOff'
        );

    }
}