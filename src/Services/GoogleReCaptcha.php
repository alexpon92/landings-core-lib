<?php
declare(strict_types=1);

namespace LandingsCore\Services;


use GuzzleHttp\Client;
use Illuminate\Http\Response;
use Psr\Log\LoggerInterface;

class GoogleReCaptcha
{
    public const RECAPTCHA_BAD_RESPONSE_CODE = 422;
    public const RECAPTCHA_BAD_FORBIDDEN_CODE = 403;
    public const RECAPTCHA_SUCCESS_CODE       = 200;

    private $client;
    private $logger;

    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
        $this->client = new Client();
    }

    /**
     * @param string|null $token
     * @param string|null $clientIp
     * @return int|null
     */
    public function execute(?string $token, ?string $clientIp): ?int
    {
        $response = $this->client->post(
            'https://www.google.com/recaptcha/api/siteverify',
            [
                'form_params' => [
                    'secret'   => config('landing.recaptcha_back_key'),
                    'response' => $token,
                    'remoteip' => $clientIp ?? ''
                ],
                'http_errors' => false
            ]
        );
        if ($response->getStatusCode() !== Response::HTTP_OK) {
            $this->logger->error(
                'Bad recaptcha response code',
                [
                    'form_params' => [
                        'secret'   => config('landing.recaptcha_back_key'),
                        'response' => $token,
                        'remoteip' => $clientIp ?? ''
                    ],
                    'code'        => $response->getStatusCode()
                ]
            );
            return self::RECAPTCHA_BAD_RESPONSE_CODE;
        }

        $content = $response->getBody()->getContents();
        $data    = json_decode($content, true);
        if (empty($data) || empty($data['success'])) {
            $this->logger->error(
                'Bad recaptcha response format',
                [
                    'form_params' => [
                        'secret'   => config('landing.recaptcha_back_key'),
                        'response' => $token,
                        'remoteip' => $clientIp ?? ''
                    ],
                    'content'     => $content
                ]
            );
            return self::RECAPTCHA_BAD_RESPONSE_CODE;
        }

        if (!isset($data['score']) || $data['score'] < config('landing.recaptcha_score')) {
            $this->logger->warning('Bad user score', ['data' => $data]);
            return self::RECAPTCHA_BAD_FORBIDDEN_CODE;
        }

        return self::RECAPTCHA_SUCCESS_CODE;
    }
}
