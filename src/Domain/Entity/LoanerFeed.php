<?php
declare(strict_types=1);

namespace LandingsCore\Domain\Entity;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * LandingsCore\Domain\Entity
 * /**
 * App\Domain\Entity\LoanerFeed
 *
 * @property int         $id
 * @property int|null    $external_id
 * @property string      $phone
 * @property string      $type
 * @property int         $core_loaner_id
 * @property mixed|null  $payload
 * @property Carbon|null $triggered_at
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 */
class LoanerFeed extends Model
{

    protected $table = 'loaner_feed';

    protected $dates = ['triggered_at'];

    /**
     * @param string     $phone
     * @param array|null $types
     *
     * @return Builder[]|\Illuminate\Database\Eloquent\Collection|LoanerFeed[]|array
     */
    public static function getByPhone(string $phone, array $types = null)
    {
        $query = self::query()->where('phone', $phone);

        if (null !== $types) {
            $query->whereIn('type', $types);
        }

        return $query->orderBy('triggered_at', 'DESC')->get();
    }

    public function getPayload(): array
    {
        if (null === $this->payload) {
            return [];
        }

        return json_decode($this->payload, true);
    }

    public function setPayload(array $payload): void
    {
        $this->payload = json_encode($payload, JSON_UNESCAPED_UNICODE);
    }

    public static function getMaxId(): int
    {
        $maxId = self::query()->max('external_id');
        return $maxId ?? 0;
    }
}
