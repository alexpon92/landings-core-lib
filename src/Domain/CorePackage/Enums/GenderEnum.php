<?php
declare(strict_types=1);

namespace LandingsCore\Domain\CorePackage\Enums;

class GenderEnum
{
    public const FEMALE = 'female';
    public const MALE   = 'male';

    public static function getAll(): array
    {
        return [
            self::MALE,
            self::FEMALE,
        ];
    }
}