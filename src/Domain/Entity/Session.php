<?php
declare(strict_types=1);

namespace LandingsCore\Domain\Entity;

use Carbon\Carbon;
use Illuminate\Contracts\Encryption\DecryptException;
use Illuminate\Contracts\Encryption\Encrypter;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Session\DatabaseSessionHandler;
use Illuminate\Support\Arr;
use LandingsCore\Domain\CorePackage\CoreClient\IPCheckResponseDto;
use LandingsCore\Domain\CorePackage\CoreClient\StoreLoaner\LoanerDto;
use LandingsCore\Domain\Dto\OfferVendorDuplicateCheck;
use Ramsey\Uuid\Uuid;

/**
 * App\Domain\Entity\Session
 *
 * @property string      $id
 * @property int|null    $user_id
 * @property string|null $ip_address
 * @property string|null $user_agent
 * @property string      $payload
 * @property int         $last_activity
 * @property string|null $phone
 * @property boolean     $is_sms_signed
 * @method static Builder|Session newModelQuery()
 * @method static Builder|Session newQuery()
 * @method static Builder|Session query()
 * @method static Builder|Session whereId($value)
 * @method static Builder|Session whereIpAddress($value)
 * @method static Builder|Session whereLastActivity($value)
 * @method static Builder|Session wherePayload($value)
 * @method static Builder|Session wherePhone($value)
 * @method static Builder|Session whereUserAgent($value)
 * @method static Builder|Session whereUserId($value)
 * @mixin \Eloquent
 * @property string|null $token
 * @property bool        $signed_off
 * @property bool        $transition_sent
 * @method static Builder|Session whereToken($value)
 * @property int|null    $core_loaner_id
 * @property int|null    $core_personal_data_id
 * @property string|null $core_hash
 * @property string|null $core_loaner_data
 * @property string      $type
 * @property string|null $offer_vendors_checks
 * @property string|null $ip_region
 */
class Session extends Model
{
    public const TYPE_API  = 'api';
    public const TYPE_FORM = 'form';

    public $incrementing = false;
    public $timestamps   = false;

    public static function setPhone(string $sessionId, string $phone): void
    {
        Session::where('id', $sessionId)
            ->update(['phone' => $phone]);
    }

    public static function setToken(string $sessionId, string $token): void
    {
        Session::where('id', $sessionId)
            ->update(['token' => $token]);
    }

    public static function findWithTokenByPhone(string $phone): ?Session
    {
        /** @var Session $session */
        $session = Session::query()
            ->where('phone', $phone)
            ->whereNotNull('token')
            ->first();

        return $session;
    }

    public function markAsTransitionSent(): void
    {
        $this->transition_sent = true;
        $this->save();
    }

    public static function getByPhone(string $phone)
    {
        return Session::query()
            ->where('phone', $phone)
            ->get();
    }

    public function markAsSignedOff(): void
    {
        $this->signed_off = true;
        $this->payload    = '';
        $this->save();
    }

    public static function getEmptySessionsIds(Carbon $borderDate, int $limit): array
    {
        return Session::query()
            ->select('id')
            ->where(
                static function ($query) {
                    /** @var Builder $query */
                    $query->whereNull('token')
                        ->orWhere('token', '');
                }
            )
            ->where('last_activity', '<=', $borderDate->timestamp)
            ->where('type', self::TYPE_FORM)
            ->limit($limit)
            ->get()
            ->pluck('id')
            ->all();
    }

    public static function deleteByIds(array $ids): void
    {
        Session::whereIn('id', $ids)->delete();
    }

    public static function markAsSignedOffByPhone(string $phone): void
    {
        Session::where('phone', $phone)
            ->update(['signed_off' => true, 'payload' => '']);
    }

    public static function setStep(string $id, int $stepCount)
    {
        Session::where('id', $id)->update(['current_step' => $stepCount]);
    }

    public static function setNames(
        string $id,
        ?string $firstName,
        ?string $lastName,
        ?string $transliteratedFirstName,
        ?string $transliteratedLastName
    ) {
        Session::where('id', $id)
            ->update(
                [
                    'first_name' => $firstName,
                    'last_name'  => $lastName,
                    'latin_name' => $transliteratedFirstName . ' ' . $transliteratedLastName
                ]
            );
    }

    public function getPayloadAsArray(): array
    {
        $payload        = $this->payload;
        $decodedPayload = base64_decode($payload);

        if (config('session.encrypt')) {
            try {
                /** @var Encrypter $encrypter */
                $encrypter = app(Encrypter::class);
                $array     = unserialize($encrypter->decrypt($decodedPayload));
            } catch (DecryptException $exception) {
                return [];
            }
        } else {
            $array = unserialize($decodedPayload);
        }

        if (!is_array($array)) {
            return [];
        }

        return $array;
    }

    /**
     * Return payload in dot notation
     *
     * @return array
     */
    public function getPayloadAsDotArray(): array
    {
        return Arr::dot($this->getPayloadAsArray());
    }

    /**
     * Must be one dimensional dot notation array
     *
     * @param array $array
     */
    public function addToPayload(array $array): void
    {
        $payload = $this->getPayloadAsArray();
        foreach ($array as $key => $value) {
            Arr::set($payload, $key, $value);
        }

        if (config('session.encrypt')) {
            /** @var Encrypter $encrypter */
            $encrypter = app(Encrypter::class);
            $payload   = base64_encode($encrypter->encrypt(serialize($payload)));
        }
        $this->payload = $payload;

        $this->save();
    }

    /**
     * @param string $sessionPrefix
     *
     * @return Session
     */
    public static function getBySessionPrefixWithToken(string $sessionPrefix): ?self
    {
        /** @var self $session */
        $session = Session::query()
            ->where('id', 'LIKE', $sessionPrefix . '%')
            ->whereNotNull('token')
            ->first();

        return $session;
    }

    /**
     * @param string $sessionPrefix
     *
     * @return Session
     */
    public static function getBySessionPrefixWithPhone(string $sessionPrefix): ?self
    {
        /** @var self $session */
        $session = Session::query()
            ->where('id', 'LIKE', $sessionPrefix . '%')
            ->whereNotNull('phone')
            ->first();

        return $session;
    }

    public static function getByPrefix(string $prefix): ?Session
    {
        /** @var self $session */
        $session = Session::query()
            ->where('id', 'LIKE', $prefix . '%')
            ->first();

        return $session;
    }

    public static function markSessionsAsSignedOffSmsByPhone(string $phone): void
    {
        Session::where('phone', $phone)
            ->update(['is_sms_signed' => false]);
    }

    public function setLoanerDataFromCoreResponse(LoanerDto $dto): void
    {
        $this->core_loaner_id        = $dto->getId();
        $this->core_personal_data_id = $dto->getPersonalData()->getId();
        $this->core_hash             = $dto->getPersonalData()->getHash();

        $this->save();
    }

    public static function buildForApi(): self
    {
        $session = new Session();

        $payload = [];
        if (config('session.encrypt')) {
            /** @var Encrypter $encrypter */
            $encrypter = app(Encrypter::class);
            $payload   = base64_encode($encrypter->encrypt(serialize($payload)));
        }

        $session->payload       = $payload;
        $session->last_activity = now()->timestamp;
        $session->id            = Uuid::uuid4()->toString();
        $session->type          = self::TYPE_API;
        $session->save();

        return $session;
    }

    public static function findApiByLoanerHash(string $hash): ?self
    {
        /** @var self|null $session */
        $session = Session::where('core_hash', $hash)
            ->where('type', self::TYPE_API)
            ->first();

        return $session;
    }

    /**
     * @return array|OfferVendorDuplicateCheck[]
     */
    public function getOfferVendorsChecks(): array
    {
        if (!$this->offer_vendors_checks) {
            return [];
        }

        $data = json_decode($this->offer_vendors_checks, true);
        $res = [];
        foreach ($data as $key => $service) {
            $res[$key] = new OfferVendorDuplicateCheck(
                $service['status'],
                $service['service'],
                Carbon::parse($service['check_time'])
            );
        }
        return $res;
    }

    public function addOfferVendorsCheck(OfferVendorDuplicateCheck $check): void
    {
        $checks = $this->getOfferVendorsChecks();
        $checks[$check->getServiceKey()] = $check;
        $this->offer_vendors_checks = json_encode($checks);
        $this->save();
    }

    public function findOfferCheck(string $service): ?OfferVendorDuplicateCheck
    {
        $checks = $this->getOfferVendorsChecks();
        return $checks[$service] ?? null;
    }

    /**
     * @return array|string[]
     */
    public function getOfferVendorKeysForDuplicates(): array
    {
        $checks = $this->getOfferVendorsChecks();
        $res = [];
        foreach ($checks as $check) {
            if ($check->getStatus() === OfferVendorDuplicateCheck::STATUS_EXISTS) {
                $res[] = $check->getServiceKey();
             }
        }

        return $res;
    }

    public function addIPRegion(string $regionName)
    {
        $this->ip_region = $regionName;
        $this->save();
    }
}
