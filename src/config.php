<?php
return [
    /**
     * Broker or landing
     */
    'project-type' => 'broker',

    'domain_name' => 'babules.su',

    'total_offers_in_list'           => 14,
    'offers_on_first_page'           => 7,
    'offers_min_probability_percent' => 50,
    'young_offers_period'            => 'P2D',
    'offers_ordering'                => 'epc',

    'wrapper_url_pattern' => env('WRAPPER_URL_PATTERN', 'https://dark.click/go2.php?to={url}'),

    'core_client' => [
        'base_uri'     => env('CORE_CLIENT_BASE_URI'),
        'landing_name' => env('LANDING_NAME'),
        'auth_token'   => env('LANDING_AUTH_TOKEN'),
    ],

    'locales' => [
        'default'   => env('DEFAULT_LOCALE', 'ru'),
        'available' => ['ru', 'uk'],
    ],

    //Brokers section
    'loaners_send_queue' => [
        'name'       => 'loaners-send-queue',
        'connection' => 'redis',
    ],

    'loaners_sign_off_queue' => [
        'name'       => 'loaners-sign-off-queue',
        'connection' => 'redis',
    ],

    'transition_send_queue' => [
        'name'       => 'transition-send-queue',
        'connection' => 'redis',
    ],

    'sessions' => [
        'empty_life_time'     => 'P30D',
        'delete_bulk_size'    => 1000,
        'cabinet_logout_idle' => env('BROKER_CABINET_LOGOUT_IDLE', 'PT4H'),
    ],

    'loaner_score' => [
        'min_score'       => 380,
        'max_score'       => 1000,
        'score_life_time' => 'P30D',
    ],

    'enable_cabinet'         => env('BROKER_CABINET_ENABLE', false),
    'redirect_cabinet_route' => 'broker.cabinet_scoring',
    'general_offers_route'   => 'broker.offers',
    'personal_offers_route'  => 'broker.cabinet_offers',
    'offers_cabinet_view'    => 'offers_page.index',

    'feedback_email'        => env('FEEDBACK_EMAIL'),
    'feedback_mail_subject' => env('FEEDBACK_EMAIL_SUBJECT', 'Обратная связь'),

    'loaner_sign_off_sms_queue' => [
        'name'       => 'loaners-sign-off-sms-queue',
        'connection' => 'redis',
    ],

    'failed_transactions_queue' => [
        'name'       => 'failed-transactions-queue',
        'connection' => 'redis',
    ],

    'count_offer_clicks' => [
        'name'       => 'count-offer-clicks-queue',
        'connection' => 'redis',
    ],

    'loaners_api' => [
        'geo'             => 'UA',
        'auth_key'        => env('LANDING_API_KEY'),
        'form_route_name' => 'broker.form',
        'register_routes' => true,
    ],

    'offer_vendors_duplicate_check' => [
        'queue'           => [
            'name'       => 'offer-vendors-duplicate-check-queue',
            'connection' => 'redis',
        ],
        'check_life_time' => 'PT24H',

        'creditplus' => [
            'base_uri' => env('CREDIT_PLUS_BASE_URI'),
            'login'    => env('CREDIT_PLUS_LOGIN'),
            'secret'   => env('CREDIT_PLUS_SECRET'),
        ],
    ]
    // -------------
];
