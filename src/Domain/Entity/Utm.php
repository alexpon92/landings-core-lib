<?php
declare(strict_types=1);

namespace LandingsCore\Domain\Entity;

use Illuminate\Database\Eloquent\Model;

class Utm extends Model
{
    protected $guarded = ['id'];

    /**
     * @param string $utm
     * @param string $value
     * @return Utm|null
     */
    public static function findByUtm(string $utm, string $value): ?Utm
    {
        /** @var Utm $utm */
        $utm = self::where('utm', $utm)->where('utm_value', $value)->first();
        return $utm;
    }
}
