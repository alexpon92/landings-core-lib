<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLoanerFeedTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('loaner_feed', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('phone');
            $table->string('type');
            $table->integer('core_loaner_id');
            $table->jsonb('payload')->nullable();
            $table->timestamp('triggered_at')->nullable();
            $table->timestamps();

            $table->index('phone');
            $table->index('triggered_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('loaner_feed');
    }
}
