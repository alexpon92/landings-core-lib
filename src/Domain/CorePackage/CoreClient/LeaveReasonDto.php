<?php
declare(strict_types=1);

namespace LandingsCore\Domain\CorePackage\CoreClient;

class LeaveReasonDto
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var array
     */
    private $texts;

    /**
     * @var string
     */
    private $urlPattern;

    /**
     * LeaveReasonDto constructor.
     *
     * @param int    $id
     * @param array  $texts
     * @param string $urlPattern
     */
    public function __construct(int $id, array $texts, string $urlPattern)
    {
        $this->id         = $id;
        $this->texts      = $texts;
        $this->urlPattern = $urlPattern;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return array
     */
    public function getTexts(): array
    {
        return $this->texts;
    }

    /**
     * @return string
     */
    public function getUrlPattern(): string
    {
        return $this->urlPattern;
    }

}