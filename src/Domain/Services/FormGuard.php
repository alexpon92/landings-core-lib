<?php
declare(strict_types=1);

namespace LandingsCore\Domain\Services;

class FormGuard
{
    /**
     * @param string|null $token
     *
     * @return bool
     */
    public function isFormVisitAllowed(?string $token): bool
    {
        return !$token;
    }
}