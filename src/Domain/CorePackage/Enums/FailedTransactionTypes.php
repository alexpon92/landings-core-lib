<?php
declare(strict_types=1);

namespace LandingsCore\Domain\CorePackage\Enums;

class FailedTransactionTypes
{
    public const TYPE_CARD_LINK = 'card_link';
    public const TYPE_RECURRENT = 'recurrent';
}