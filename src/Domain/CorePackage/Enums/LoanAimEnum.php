<?php
declare(strict_types=1);

namespace LandingsCore\Domain\CorePackage\Enums;

class LoanAimEnum
{
    public const TECH         = 'tech';
    public const CURE         = 'cure';
    public const OTHER_CREDIT = 'other_credit';
    public const UNTIL_SALARY = 'until_salary';
    public const OTHER        = 'other';
    public const VACANCIES    = 'vacancies';
    public const RENT         = 'rent';
    public const CAR_REPAIR   = 'car_repair';
    public const HOUSE_REPAIR = 'house_repair';

    public static function getAll(): array
    {
        return [
            self::TECH,
            self::CURE,
            self::OTHER_CREDIT,
            self::UNTIL_SALARY,
            self::OTHER,
            self::VACANCIES,
            self::RENT,
            self::CAR_REPAIR,
            self::HOUSE_REPAIR
        ];
    }
}