<?php
declare(strict_types=1);

namespace LandingsCore\Domain\Services\Cabinet;


use LandingsCore\Domain\Entity\PersonalData;
use LandingsCore\Domain\Events\LoanerUpdated;
use Carbon\Carbon;

class ProcessPersonalData
{

    public function execute(string $phone, string $sessionId, array $payload): PersonalData
    {

        $personalData = PersonalData::findByPhone($phone);

        if (null === $personalData) {
            $personalData = $this->createPersonalData($phone, $sessionId);
        }

        $decodedPersonalData = $personalData->getPayload();

        $loanerScore             = $this->personalDataScore($decodedPersonalData) ?? $decodedPersonalData['loaner_score'];
        $payload['loaner_score'] = $loanerScore;

        $currentStep             = $payload['current_step'] ?? null;
        $personalDataCurrentStep = $decodedPersonalData['current_step'] ?? null;

        $bankCardToken = $payload['lead']['bank_card_token'] ?? null;

        if (null !== $bankCardToken && !$personalData->conversion) {
            $personalData->markAsConversion();
            $personalData->setPayload($payload);
            $personalData->session_id = $sessionId;
        } elseif ($currentStep >= $personalDataCurrentStep && !$personalData->conversion) {
            $personalData->setPayload($payload);
            $personalData->session_id = $sessionId;
        } elseif ($personalData->session_id === $sessionId) {
            $personalData->setPayload($payload);
        }

        $personalData->save();

        return $personalData;
    }

    private function createPersonalData(string $phone, string $sessionId): PersonalData
    {
        $personalData             = new PersonalData();
        $personalData->phone      = $phone;
        $personalData->session_id = $sessionId;
        return $personalData;
    }

    private function personalDataScore(array $currentPayload): ?array
    {

        $score = [
            'generated_at' => Carbon::now()->toDateTimeString(),
            'score'        => random_int(
                (int)config('landings-core-lib.loaner_score.min_score'),
                (int)config('landings-core-lib.loaner_score.max_score')
            ),
        ];

        if (!isset($currentPayload['loaner_score'])) {
            return $score;
        }

        $generatedAt = Carbon::parse($currentPayload['loaner_score']['generated_at']);

        if ($generatedAt > Carbon::now()->sub(new \DateInterval(config('landings-core-lib.loaner_score.score_life_time')))) {
            return $score;
        }

        return null;
    }


}