<?php
declare(strict_types=1);

namespace LandingsCore\Domain\Services\LoanersApi\Validators\RuleSets;

use LandingsCore\Domain\Services\LoanersApi\Validators\IRuleSet;

class RuRuleSet implements IRuleSet
{
    public function getRules(): array
    {
        return [
            'loaner.referer'           => [
                'required',
                'min:5',
            ],
            'loaner.phone'             => [
                'required',
                'regex:/^\+79[0-9]{9}$/',
            ],
            'loaner.email'             => [
                'nullable',
                'string',
                'email',
                'max:255',
                'min:5',
            ],
            'loaner.last_name'         => [
                'required',
                'string',
                'min:2',
            ],
            'loaner.first_name'        => [
                'required',
                'string',
                'min:2',
            ],
            'loaner.patronymic'        => [
                'nullable',
                'string',
                'min:2',
            ],
            'loaner.birthday'          => [
                'nullable',
                'string',
                'date_format:d.m.Y',
            ],
            'loaner.loan_sum'          => [
                'nullable',
                'integer',
            ],
            'loaner.passport_serial'   => [
                'nullable',
                'regex:/^[0-9]{2}\s?[0-9]{2}$/u',
            ],
            'loaner.passport_number'   => [
                'nullable',
                'regex:/^[0-9]{3}\s?[0-9]{3}$/u',
            ],
            'loaner.passport_dep_code' => [
                'nullable',
                'regex:/^[0-9]{3}\-[0-9]{3}$/u',
            ],
            'loaner.birth_place'       => [
                'nullable',
                'string',
                'min:2',
            ],
            'loaner.region'            => [
                'nullable',
                'string',
                'max:100',
                'min:3',
            ],
            'loaner.city'              => [
                'nullable',
                'string',
                'max:100',
                'min:3',
            ],
            'loaner.street'            => [
                'nullable',
                'string',
                'max:100',
                'min:3',
            ],
            'loaner.house'             => [
                'nullable',
                'string',
                'max:10',
                'min:1',
            ],
            'loaner.flat'              => [
                'nullable',
                'string',
                'max:10',
                'min:1',
            ],
            'loaner.address_not_equal' => [
                'nullable',
                'integer',
            ],
            'loaner.region_fact'       => [
                'nullable',
                'required_if:address_not_equal,1',
                'string',
                'max:100',
                'min:3',
            ],
            'loaner.city_fact'         => [
                'nullable',
                'required_if:address_not_equal,1',
                'string',
                'max:100',
                'min:3',
            ],
            'loaner.street_fact'       => [
                'nullable',
                'required_if:address_not_equal,1',
                'string',
                'max:100',
                'min:3',
            ],
            'loaner.house_fact'        => [
                'nullable',
                'required_if:address_not_equal,1',
                'string',
                'max:10',
                'min:1',
            ],
            'loaner.flat_fact'         => [
                'nullable',
                'string',
                'max:10',
                'min:1',
            ],
        ];
    }

    public function getGeoKey(): string
    {
        return 'RU';
    }

}
