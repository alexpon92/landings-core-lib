<?php
declare(strict_types=1);

namespace LandingsCore\Console\Commands;

use Illuminate\Console\Command;
use LandingsCore\Domain\Services\ClearEmptySessions as ClearService;

class ClearEmptySessions extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'core:clear-empty-sessions';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Clear empty sessions from database';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function handle(ClearService $service): void
    {
        $service->execute(
            (string)config('landings-core-lib.sessions.empty_life_time'),
            (int)config('landings-core-lib.sessions.delete_bulk_size')
        );
    }
}