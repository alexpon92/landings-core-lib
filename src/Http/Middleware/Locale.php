<?php
declare(strict_types=1);

namespace LandingsCore\Http\Middleware;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Closure;
use Illuminate\Support\Facades\App;

class Locale
{
    /**
     * @param Request $request
     * @param Closure $next
     *
     * @return Response
     */
    public function handle(Request $request, Closure $next)
    {
        $locale = $request->get('lang');
        if (!$locale) {
            $locale = $request->session()->get('locale');
            if (!$locale) {
                $locale = config('landings-core-lib.locales.default');
            }
        } else {
            if (!in_array($locale, config('landings-core-lib.locales.available'), true)) {
                $locale = config('landings-core-lib.locales.default');
            }
        }

        $request->session()->put('locale', $locale);
        $request->session()->put('query_arr.locale', $locale);

        App::setLocale((string)$locale);

        return $next($request);
    }
}