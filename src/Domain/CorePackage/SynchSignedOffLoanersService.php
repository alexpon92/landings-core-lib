<?php
declare(strict_types=1);

namespace LandingsCore\Domain\CorePackage;

use LandingsCore\Domain\CorePackage\CoreClient\ICoreClient;
use LandingsCore\Domain\Services\SignOffLoanerService;

class SynchSignedOffLoanersService
{
    /**
     * @var ICoreClient
     */
    private $client;

    /**
     * @var SignOffLoanerService
     */
    private $signOffService;

    /**
     * SynchSignedOffLoanersService constructor.
     *
     * @param ICoreClient          $client
     * @param SignOffLoanerService $signOffService
     */
    public function __construct(ICoreClient $client, SignOffLoanerService $signOffService)
    {
        $this->client         = $client;
        $this->signOffService = $signOffService;
    }

    /**
     * @throws CoreClient\ClientExceptions\BadResponseCode
     * @throws CoreClient\ClientExceptions\BadResponseFormat
     */
    public function execute(): void
    {
        $phones = $this->client->getSignedOffLoaners();
        $this->signOffService->signOffBulk($phones);
    }
}