<?php
declare(strict_types=1);

namespace LandingsCore\Listeners;

use LandingsCore\Domain\CorePackage\CoreClient\ClientExceptions\BadResponseCode;
use LandingsCore\Domain\CorePackage\CoreClient\ClientExceptions\BadResponseFormat;
use LandingsCore\Domain\Entity\PersonalData;
use LandingsCore\Domain\Entity\Session;
use LandingsCore\Domain\Events\LoanerUpdated;
use LandingsCore\Domain\Services\Cabinet\ProcessPersonalData;
use Psr\Log\LoggerInterface;

class PersonalDataSubscriber extends AbstractListener
{
    /**
     * @var ProcessPersonalData
     */
    private $processPersonalData;

    /**
     * PersonalDataSubscriber constructor.
     * @param ProcessPersonalData $processPersonalData
     */
    public function __construct(
        ProcessPersonalData $processPersonalData
    ) {
        $this->processPersonalData = $processPersonalData;
    }

    /**
     * @param LoanerUpdated $event
     *
     * @throws BadResponseCode
     * @throws BadResponseFormat
     */
    public function onLoanerUpdated(LoanerUpdated $event): void
    {
        if ($event->getPhone()) {
            $this->processPersonalData->execute(
                $event->getPhone(),
                $event->getSessionId(),
                $event->getRawPayload()
            );
        }
    }

    public function getConnection(): string
    {
        return (string)config('landings-core-lib.loaners_send_queue.connection');
    }

    public function getQueue(): string
    {
        return (string)config('landings-core-lib.loaners_send_queue.name');
    }

    /**
     * Register the listeners for the subscriber.
     *
     * @param \Illuminate\Events\Dispatcher $events
     */
    public function subscribe(
        $events
    ) {
        $events->listen(
            LoanerUpdated::class,
            self::class . '@onLoanerUpdated'
        );

    }
}