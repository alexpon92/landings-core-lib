<?php
declare(strict_types=1);

namespace LandingsCore\Http\Controllers;

use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use LandingsCore\Domain\Events\OfferClicked;
use LandingsCore\Http\Requests\TrackOfferRequest;

class TrackOffersController extends Controller
{
    public function track(TrackOfferRequest $request): Response
    {
        event(new OfferClicked(
            $request->getId(),
            $request->session()->getId(),
            $request->session()->get('utm_source'),
            $request->session()->get('utm_campaign'),
            $request->session()->get('utm_medium')
        ));

        return response(null, 204);
    }
}
