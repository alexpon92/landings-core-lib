<?php
declare(strict_types=1);

namespace LandingsCore\Domain\Services\LoanersApi\Validators;

use LandingsCore\Domain\Services\LoanersApi\Validators\Exceptions\DuplicateRuleSetGeoException;
use LandingsCore\Domain\Services\LoanersApi\Validators\Exceptions\InvalidRuleSetTypeException;

class RulesRepository
{
    private $map;

    /**
     * RulesRepository constructor.
     *
     * @param array|IRuleSet[] $set
     *
     * @throws DuplicateRuleSetGeoException
     * @throws InvalidRuleSetTypeException
     */
    public function __construct(array $set)
    {
        $this->map = [];

        foreach ($set as $elem) {
            if (! ($elem instanceof IRuleSet) ) {
                $class = get_class($elem);
                throw new InvalidRuleSetTypeException("Invalid rule set type $class");
            }

            if (isset($this->map[$elem->getGeoKey()])) {
                throw new DuplicateRuleSetGeoException("Duplicate rules set geo {$elem->getGeoKey()}");
            }

            $this->map[$elem->getGeoKey()] = $elem;
        }
    }

    /**
     * @param string $geo
     *
     * @return IRuleSet|null
     */
    public function findByGeo(string $geo): ?IRuleSet
    {
        return $this->map[$geo] ?? null;
    }
}