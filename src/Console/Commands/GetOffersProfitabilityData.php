<?php

namespace LandingsCore\Console\Commands;

use Illuminate\Console\Command;
use LandingsCore\Domain\CorePackage\SynchProfitabilityDataService;

final class GetOffersProfitabilityData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'core:get-offers-profitability-data';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Synchronize offers profitability data';

    /**
     * @param SynchProfitabilityDataService $syncProfitabilityDataService
     */
    public function handle(SynchProfitabilityDataService $syncProfitabilityDataService)
    {
        $syncProfitabilityDataService->synch();
    }
}
