<?php

namespace LandingsCore\Domain\Services;

use Illuminate\Support\Carbon;
use LandingsCore\Domain\CorePackage\Enums\OffersOrderingEnum;
use LandingsCore\Domain\Dto\OfferWithProfitabilityDto;

final class OffersBuilder
{
    /**
     * @var Carbon
     */
    private $borderDate;

    public function __construct(string $youngOffersPeriod)
    {
        $this->borderDate = now()->sub(new \DateInterval($youngOffersPeriod));
    }

    /**
     * Build offers for the showcase
     *
     * @param OfferWithProfitabilityDto[] $allOffers
     *
     * @return OfferWithProfitabilityDto[]
     *
     * @throws \Exception
     */
    public function getOffers(
        array $allOffers
    ): array
    {
        if (config('landings-core-lib.offers_ordering') === OffersOrderingEnum::PROBABILITY) {
            return $allOffers;
        }

        $result = array_merge(
            $this->getTopOffers($allOffers),
            $this->getYoungOffers($allOffers)
        );

        $allOffers = $this->stripUsedOffers($allOffers, $result);
        $result    = array_merge(
            $result,
            $this->getMainOffers($allOffers)
        );

        $allOffers = $this->stripUsedOffers($allOffers, $result);
        $result    = array_merge(
            $result,
            $this->getOtherOffers($allOffers)
        );
        $result = array_slice($result, 0, config('landings-core-lib.total_offers_in_list'));

        return $this->calculateProbability($result);
    }

    /**
     * Build top offers
     *
     * @param OfferWithProfitabilityDto[] $offers
     *
     * @return OfferWithProfitabilityDto[]
     */
    private function getTopOffers(array $offers): array
    {
        return $this->filterAndOrderOffers(
            $offers,
            static function (OfferWithProfitabilityDto $offer) {
                return $offer->getOffer()->is_in_top;
            },
            static function (OfferWithProfitabilityDto $a, OfferWithProfitabilityDto $b) {
                return $b->getOffer()->probability <=> $a->getOffer()->probability;
            }
        );
    }

    /**
     * Build young offers
     *
     * @param OfferWithProfitabilityDto[] $offers
     *
     * @return OfferWithProfitabilityDto[]
     *
     * @throws \Exception
     */
    private function getYoungOffers(array $offers): array
    {
        $borderDate = $this->borderDate;

        return $this->filterAndOrderOffers(
            $offers,
            static function (OfferWithProfitabilityDto $offer) use ($borderDate) {
                return $offer->getOffer()->is_in_top === false && $offer->getOffer()->created_at >= $borderDate;
            },
            static function (OfferWithProfitabilityDto $a, OfferWithProfitabilityDto $b) {
                return $b->getOffer()->probability <=> $a->getOffer()->probability;
            }
        );
    }

    /**
     * Build main offers depending on user data
     *
     * @param OfferWithProfitabilityDto[] $offers
     *
     * @return OfferWithProfitabilityDto[]
     */
    private function getMainOffers(array $offers): array
    {
        return $this->filterAndOrderOffers(
            $offers,
            static function (OfferWithProfitabilityDto $offer) {
                return $offer->getEpc() > 0;
            },
            static function (OfferWithProfitabilityDto $a, OfferWithProfitabilityDto $b) {
                return $b->getEpc() <=> $a->getEpc();
            }
        );
    }

    /**
     * Build rest offers
     *
     * @param OfferWithProfitabilityDto[] $offers
     *
     * @return OfferWithProfitabilityDto[]
     */
    private function getOtherOffers(array $offers): array
    {
        return $this->filterAndOrderOffers(
            $offers,
            static function (OfferWithProfitabilityDto $offer) {
                return $offer->getOffer()->is_in_popup === false;
            },
            static function (OfferWithProfitabilityDto $a, OfferWithProfitabilityDto $b) {
                return $b->getOffer()->probability <=> $a->getOffer()->probability;
            }
        );
    }

    /**
     * Filter and sort offers
     *
     * @param array    $offers
     * @param callable $filterCallback
     * @param callable $sortCallback
     *
     * @return array
     */
    private function filterAndOrderOffers(array $offers, callable $filterCallback, callable $sortCallback): array
    {
        $result = array_filter($offers, $filterCallback);
        usort($result, $sortCallback);

        return $result;
    }

    /**
     * Remove already used offers from the list
     *
     * @param OfferWithProfitabilityDto[] $allOffers
     * @param OfferWithProfitabilityDto[] $usedOffers
     *
     * @return OfferWithProfitabilityDto[]
     */
    private function stripUsedOffers(array $allOffers, array $usedOffers): array
    {
        $usedIds = [];
        array_map(
            static function (OfferWithProfitabilityDto $offer) use (&$usedIds) {
                $usedIds[] = $offer->getOffer()->id;
            },
            $usedOffers
        );

        return array_filter(
            $allOffers,
            static function (OfferWithProfitabilityDto $offer) use ($usedIds) {
                return !in_array($offer->getOffer()->id, $usedIds, true);
            }
        );
    }

    /**
     * Calculate probability based on position
     *
     * @param OfferWithProfitabilityDto[] $offers
     *
     * @return OfferWithProfitabilityDto[]
     *
     * @throws \Exception
     */
    private function calculateProbability(array $offers): array
    {
        $count = \count($offers);

        if ($count === 0) {
            return $offers;
        }

        $maxProbabilityValue = 100;
        $minProbabilityValue = config('landings-core-lib.offers_min_probability_percent');
        $probabilityStep     = ($maxProbabilityValue - $minProbabilityValue) / $count;
        $offers[0]->setDisplayProbability(random_int($maxProbabilityValue - $probabilityStep, $maxProbabilityValue));

        for ($i = 1; $i < $count; $i++) {
            $maxProbabilityValue = $offers[$i - 1]->getDisplayProbability();
            $offers[$i]->setDisplayProbability(
                random_int($maxProbabilityValue - $probabilityStep, $maxProbabilityValue)
            );
        }

        return $offers;
    }
}
