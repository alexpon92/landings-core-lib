<?php
declare(strict_types=1);

namespace LandingsCore\Domain\Services\LoanersApi\ApiProcessor;

use LandingsCore\Domain\Entity\Session;
use LandingsCore\Domain\Events\LoanerUpdated;
use LandingsCore\Domain\Services\LoanersApi\ApiProcessor\Exceptions\MapperNotFoundException;
use LandingsCore\Domain\Services\LoanersApi\LoanerDataMappers\Factories\MappersFactory;
use Psr\Log\LoggerInterface;

class LoanerApiProcessor
{
    /**
     * @var MappersFactory
     */
    private $mappersFactories;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * LoanerApiProcessor constructor.
     *
     * @param MappersFactory  $mappersFactories
     * @param LoggerInterface $logger
     */
    public function __construct(MappersFactory $mappersFactories, LoggerInterface $logger)
    {
        $this->mappersFactories = $mappersFactories;
        $this->logger           = $logger;
    }

    /**
     * @param array  $data
     * @param string $source
     * @param string $web
     * @param string $applicationId
     * @param string $geo
     * @throws MapperNotFoundException
     */
    public function execute(array $data, string $source, string $web, string $applicationId, string $geo): void
    {
        $mapper = $this->mappersFactories->buildByGeo($geo);
        if (!$mapper) {
            $this->logger->error(
                'Mapper is not found for geo',
                [
                    'geo' => $geo
                ]
            );
            throw new MapperNotFoundException("Mapper is not found for geo {$geo}");
        }

        $session = Session::buildForApi();
        $mapper->map($data, $session);

        $session->addToPayload(
            ['utm_source' => $source, 'utm_campaign' => $web, 'utm_medium' => 'api', 'query_arr.___api_id' => $applicationId]
        );

        event(new LoanerUpdated($session->id, $session->getPayloadAsArray()));
    }
}