<?php
declare(strict_types=1);

namespace LandingsCore\Domain\Services\LoanersApi\LoanerDataMappers\Mappers;

use Carbon\Carbon;
use LandingsCore\Domain\Entity\Session;
use LandingsCore\Domain\Services\LoanersApi\LoanerDataMappers\IMapper;

class RuDataMapper implements IMapper
{
    public function map(array $data, Session $session): Session
    {
        $payload = [];
        if (isset($data['referer'])) {
            $payload['query_arr.___lead_info.referer'] = $data['referer'];
        }

        if (isset($data['phone'])) {
            $session->phone = $data['phone'];
            $payload['lead.phone'] = $data['phone'];
        }

        if (isset($data['email'])) {
            $payload['lead.email'] = $data['email'];
        }

        if (isset($data['last_name'])) {
            $payload['lead.last_name'] = $data['last_name'];
        }

        if (isset($data['first_name'])) {
            $payload['lead.first_name'] = $data['first_name'];
        }

        if (isset($data['patronymic'])) {
            $payload['lead.patronymic'] = $data['patronymic'];
        }

        if (isset($data['birthday'])) {
            $payload['lead.birthday'] = $data['birthday'];
        }

        if (isset($data['loan_sum'])) {
            $payload['lead.loan_sum'] = $data['loan_sum'];
        }

        if (isset($data['passport_serial'])) {
            $payload['lead.passport_serial'] = preg_replace("/\s/", '', $data['passport_serial']);
        }

        if (isset($data['passport_number'])) {
            $payload['lead.passport_number'] = preg_replace("/\s/", '', $data['passport_number']);
        }

        if (isset($data['birth_place'])) {
            $payload['lead.birth_place'] = $data['birth_place'];
        }

        if (isset($data['region'])) {
            $payload['lead.region'] = $data['region'];
        }

        if (isset($data['city'])) {
            $payload['lead.city'] = $data['city'];
        }

        if (isset($data['street'])) {
            $payload['lead.street'] = $data['street'];
        }

        if (isset($data['house'])) {
            $payload['lead.house'] = $data['house'];
        }

        if (isset($data['flat'])) {
            $payload['lead.region'] = $data['flat'];
        }

        if (isset($data['address_not_equal'])) {
            $payload['lead.address_not_equal'] = $data['address_not_equal'];
        }

        if (isset($data['city_fact'])) {
            $payload['lead.city_fact'] = $data['city_fact'];
        }

        if (isset($data['street_fact'])) {
            $payload['lead.street_fact'] = $data['street_fact'];
        }

        if (isset($data['house_fact'])) {
            $payload['lead.house_fact'] = $data['house_fact'];
        }

        if (isset($data['flat_fact'])) {
            $payload['lead.flat_fact'] = $data['flat_fact'];
        }

        $session->addToPayload($payload);

        return $session;
    }
}