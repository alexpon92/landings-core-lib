<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class LeadActionsTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lead_card_links', function (Blueprint $table) {
            $table->increments('id');
            $table->string('phone', 20)->index();
            $table->string('token')->index();

            $table->timestamps();
        });

        Schema::create('lead_sign_out_attempts', function (Blueprint $table) {
            $table->increments('id');
            $table->string('phone', 20)->index();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lead_card_links');
        Schema::dropIfExists('lead_sign_out_attempts');
    }
}
