<?php
declare(strict_types=1);

namespace LandingsCore\Domain\CorePackage;

use LandingsCore\Domain\Entity\Activity;
use LandingsCore\Domain\CorePackage\CoreClient\ICoreClient;
use LandingsCore\Domain\Entity\Template;
use LandingsCore\Domain\Entity\Utm;

class SynchLandingSettingsService
{
    /**
     * @var ICoreClient
     */
    private $client;

    /**
     * SynchLandingSettingsService constructor.
     * @param ICoreClient $client
     */
    public function __construct(ICoreClient $client)
    {
        $this->client = $client;
    }

    /**
     * @throws \Exception
     */
    public function synch(): void
    {
        $settings = $this->client->getLandingSettings();
        $this->saveSettings($settings);
        $this->saveActivities($settings['activities']);
        $this->saveUtms($settings['utm']);

    }

    private function saveActivities(array $activities): void
    {
        if (isset($activities['city'], $activities['names'], $activities['amount'])) {
            $activity = Activity::first() ?? new Activity();
            $activity->setCities($activities['city']);
            $activity->setNames($activities['names']);
            $activity->setAmounts($activities['amount']);
            $activity->save();
        }
    }

    private function saveUtms(array $utms): void
    {
        $ids = [];
        foreach ($utms as $utm) {
            $utmEntity            = Utm::findByUtm($utm['utm'], $utm['value']) ?? new Utm();
            $utmEntity->utm       = $utm['utm'];
            $utmEntity->utm_value = $utm['value'];
            $utmEntity->title     = $utm['title'];
            $utmEntity->save();
            $ids[] = $utmEntity->id;
        }

        if (!empty($ids)) {
            Utm::whereNotIn('id', $ids)->delete();
        }
    }

    private function saveSettings(array $settings): void
    {
        $settingsEntity = Template::first() ?? new Template();
        $settingsEntity->setTemplate($settings['template']);
        $settingsEntity->setSettings($settings['settings']);
        $settingsEntity->setLocales($settings['landing']['locales']);
        $settingsEntity->setLocales($settings['landing']['locales']);
        $settingsEntity->setCreditors($settings['creditors']);
        $settingsEntity->setLocalization($this->prepareLocalization($settings['localization']));
        $settingsEntity->country = $settings['landing']['country'];
        $settingsEntity->version = $settings['version'];
        $settingsEntity->save();
    }

    private function prepareLocalization(array $localization): array
    {
        $preparedLocalization = [];
        foreach ($localization as $item) {
            $preparedLocalization[$item['key']] = $item['value'];
        }

        return $preparedLocalization;
    }

}