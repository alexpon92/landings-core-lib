<?php
declare(strict_types=1);

namespace LandingsCore\Domain\Services\OfferVendorsDuplicatesCheck\CheckServices;

use LandingsCore\Domain\Dto\OfferVendorDuplicateCheck;
use LandingsCore\Domain\Entity\Session;
use LandingsCore\Domain\Services\OfferVendorsDuplicatesCheck\CheckServices\Exceptions\BadResponseCodeException;
use LandingsCore\Domain\Services\OfferVendorsDuplicatesCheck\CheckServices\Exceptions\MalformedResponseFormatException;
use phpDocumentor\Reflection\Types\Static_;

abstract class AbstractCheckService
{
    /**
     * @return string
     */
    abstract public static function getServiceKey(): string;

    /**
     * @param string     $phone
     * @param array|null $data
     *
     * @return CheckResponse
     * @throws BadResponseCodeException
     * @throws MalformedResponseFormatException
     */
    abstract protected function check(string $phone, ?array $data = []): CheckResponse;

    /**
     * @param Session $session
     *
     * @throws BadResponseCodeException
     * @throws MalformedResponseFormatException
     */
    public function searchDuplicates(Session $session): void
    {
        if (!$session->phone) {
            return;
        }

        if (!$this->isNeedToCheck($session->findOfferCheck(static::getServiceKey()))){
            return;
        }

        $res = $this->check($session->phone);

        $session->addOfferVendorsCheck(
            new OfferVendorDuplicateCheck(
                $res->isExists() ? OfferVendorDuplicateCheck::STATUS_EXISTS
                    : OfferVendorDuplicateCheck::STATUS_NOT_EXISTS,
                static::getServiceKey(),
                now()
            )
        );
    }

    public function isNeedToCheck(?OfferVendorDuplicateCheck $lastCheck): bool
    {
        return !$lastCheck ||
            now() >
            $lastCheck->getCheckTime()
                ->add(new \DateInterval(config('landings-core-lib.offer_vendors_duplicate_check.check_life_time')));
    }
}