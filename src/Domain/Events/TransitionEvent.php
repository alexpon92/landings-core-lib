<?php
declare(strict_types=1);

namespace LandingsCore\Domain\Events;


use Carbon\Carbon;

class TransitionEvent
{
    private $sessionId;
    private $utmSource;
    private $utmMedium;
    private $utmCampaign;
    private $landingUri;
    private $referer;
    private $ipAddress;
    private $data;
    private $visitedAt;
    private $triggeredAt;

    /**
     * TransitionEvent constructor.
     * @param string      $sessionId
     * @param string      $utmSource
     * @param string|null $utmMedium
     * @param string|null $utmCampaign
     * @param string|null $landingUri
     * @param string|null $referer
     * @param string|null $ipAddress
     * @param array|null  $data
     * @param Carbon      $visitedAt
     */
    public function __construct(
        string $sessionId,
        string $utmSource,
        ?string $utmMedium,
        ?string $utmCampaign,
        ?string $landingUri,
        ?string $referer,
        ?string $ipAddress,
        ?array $data,
        Carbon $visitedAt
    ) {
        $this->sessionId   = $sessionId;
        $this->utmSource   = $utmSource;
        $this->utmMedium   = $utmMedium;
        $this->utmCampaign = $utmCampaign;
        $this->landingUri  = $landingUri;
        $this->referer     = $referer;
        $this->ipAddress   = $ipAddress;
        $this->data        = $data;
        $this->visitedAt   = $visitedAt;
        $this->triggeredAt = Carbon::now();
    }

    /**
     * @return string
     */
    public function getSessionId(): string
    {
        return $this->sessionId;
    }

    /**
     * @return string
     */
    public function getUtmSource(): string
    {
        return $this->utmSource;
    }

    /**
     * @return string|null
     */
    public function getUtmMedium(): ?string
    {
        return $this->utmMedium;
    }

    /**
     * @return string|null
     */
    public function getUtmCampaign(): ?string
    {
        return $this->utmCampaign;
    }

    /**
     * @return string|null
     */
    public function getLandingUri(): ?string
    {
        return $this->landingUri;
    }

    /**
     * @return string|null
     */
    public function getReferer(): ?string
    {
        return $this->referer;
    }

    /**
     * @return string|null
     */
    public function getIpAddress(): ?string
    {
        return $this->ipAddress;
    }

    /**
     * @return array|null
     */
    public function getData(): ?array
    {
        return $this->data;
    }

    /**
     * @return Carbon
     */
    public function getVisitedAt(): Carbon
    {
        return $this->visitedAt;
    }

    /**
     * @return Carbon
     */
    public function getTriggeredAt(): Carbon
    {
        return $this->triggeredAt;
    }
}