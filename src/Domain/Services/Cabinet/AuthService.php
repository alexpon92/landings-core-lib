<?php
declare(strict_types=1);

namespace LandingsCore\Domain\Services\Cabinet;

use Illuminate\Support\Facades\Auth;
use LandingsCore\Domain\Entity\LeadSignOutAttempt;
use LandingsCore\Domain\Entity\PersonalData;
use LandingsCore\Domain\Services\Cabinet\Exceptions\PersonalDataNotFoundException;
use LandingsCore\Domain\Services\Cabinet\Exceptions\SignOutException;

class AuthService
{

    public function execute(string $phone): void
    {
        $personalData = PersonalData::findByPhone($phone);

        if (null === $personalData) {
            throw new PersonalDataNotFoundException("Personal data not found by phone {$phone}");
        }

        $signOutAttempt = LeadSignOutAttempt::getLastSignOutAttempt($phone);
        if ($signOutAttempt) {
            throw new SignOutException("Loaner is signed out");
        }

        Auth::setUser($personalData);
    }

}