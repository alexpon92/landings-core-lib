<?php
declare(strict_types=1);

namespace LandingsCore\Domain\Services\Cabinet\Exceptions;


class SignOutException extends \Exception
{

}