<?php
declare(strict_types=1);

namespace LandingsCore\Domain\Entity;

use Illuminate\Database\Eloquent\Model;

class Template extends Model
{
    protected $table = 'template';
    protected $guarded = ['id'];

    public function getSettings(): array
    {
        return json_decode($this->settings, true);
    }

    public function setSettings(array $settings): void
    {
        $this->settings = json_encode($settings);
    }

    public function getTemplate(): array
    {
        return json_decode($this->content, true);
    }

    public function setTemplate(array $template): void
    {
        $this->content = json_encode($template);
    }

    public function getLocales(): array
    {
        return json_decode($this->locales, true);
    }

    public function setLocales(array $locales): void
    {
        $this->locales = json_encode($locales);
    }

    public function getLocalization(): array
    {
        return null !== $this->localization ? json_decode($this->localization, true): [];
    }

    public function setLocalization(array $locales): void
    {
        $this->localization = json_encode($locales);
    }

    public function getCreditors(): array
    {
        return json_decode($this->creditors, true);
    }

    public function setCreditors(array $creditors): void
    {
        $this->creditors = json_encode($creditors);
    }
}
