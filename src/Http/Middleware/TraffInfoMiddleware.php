<?php
declare(strict_types=1);

namespace LandingsCore\Http\Middleware;

use Illuminate\Support\Carbon;
use Illuminate\Http\Request;
use Closure;
use LandingsCore\Domain\Events\TransitionEvent;
use LandingsCore\Http\Helpers\IdentifyRequestIp;

class TraffInfoMiddleware
{
    /**
     * @var IdentifyRequestIp
     */
    private $ipResolver;

    /**
     * TraffInfoMiddleware constructor.
     *
     * @param IdentifyRequestIp $ipResolver
     */
    public function __construct(IdentifyRequestIp $ipResolver)
    {
        $this->ipResolver = $ipResolver;
    }

    public function handle(Request $request, Closure $next)
    {
        if (!$request->session()->has('query_arr.___referer')) {
            $referer = $request->header('referer');
            if (!empty($referer) && strpos($referer, (string)config('landings-core-lib.domain_name')) === false) {
                $request->session()->put('query_arr.___referer', $referer);
            }
        }

        $ip = $this->ipResolver->getIp($request);
        if (!empty($ip)) {
            $request->session()->put('query_arr.___ip', $ip);
        }

        if (!$request->session()->has('query_arr.___land_uri')) {
            $url = $request->url();
            if (!empty($url)) {
                $request->session()->put('query_arr.___land_uri', $url);
            }
        }

        //traffic sources
        if ($request->has('utm_source')) {
            $source = $request->get('utm_source');
            if ($source) {
                $request->session()->put('utm_source', $source);
            }
        }

        if ($request->has('utm_medium')) {
            $medium = $request->get('utm_medium');
            if ($medium) {
                $request->session()->put('utm_medium', $medium);
            }
        }

        if ($request->has('utm_campaign')) {
            $campaign = $request->get('utm_campaign');
            if ($campaign) {
                $request->session()->put('utm_campaign', $campaign);
            }
        }

        if ($request->has('utm_source')) {
            $queryArr = $request->session()->get('query_arr', []);
            $request->session()->put('query_arr', array_merge($queryArr, $request->all()));
        }

        return $next($request);
    }

    public function terminate(Request $request, $response)
    {
        $parsedReferer = parse_url($request->header('referer') ?? '');
        $host = $parsedReferer['host'] ?? null;

        if ($request->has('utm_source') && $host !== config('landings-core-lib.domain_name')) {
            $event = new TransitionEvent(
                $request->session()->getId(),
                $request->session()->get('utm_source'),
                $request->session()->get('utm_medium'),
                $request->session()->get('utm_campaign'),
                $request->session()->get('query_arr.___land_uri'),
                $request->session()->get('query_arr.___referer'),
                $request->session()->get('query_arr.___ip'),
                $request->session()->get('query_arr'),
                Carbon::now()
            );
            event($event);
        }
    }
}
