<?php
declare(strict_types=1);

namespace LandingsCore;

use LandingsCore\Console\Commands\GetLoanerSellingServices;
use LandingsCore\Console\Commands\GetOffersProfitabilityData;
use LandingsCore\Console\Commands\GetSellingServicesAndReports;
use LandingsCore\Console\Commands\RetryQueue;
use LandingsCore\Console\Commands\ResendSessions;
use LandingsCore\Console\Commands\SynchLandingSettings;
use Illuminate\Support\ServiceProvider;
use LandingsCore\Console\Commands\ClearEmptySessions;
use LandingsCore\Console\Commands\GetOffersAndReasons;
use LandingsCore\Console\Commands\SynchLoanersFeedCommand;
use LandingsCore\Console\Commands\SynchSignedOffLoaners;
use LandingsCore\Console\Commands\SynchTrafficSources;
use LandingsCore\Console\Commands\UpdateAssetVersion;
use LandingsCore\Domain\CorePackage\CoreClient\CoreClient;
use LandingsCore\Domain\CorePackage\CoreClient\ICoreClient;
use GuzzleHttp\Client;
use LandingsCore\Domain\Services\LoanersApi\Validators\RuleSets\UaRuleSet;
use LandingsCore\Domain\Services\LoanersApi\Validators\RuleSets\RuRuleSet;
use LandingsCore\Domain\Services\LoanersApi\Validators\RulesRepository;
use LandingsCore\Domain\Services\OffersBuilder;
use Psr\Log\LoggerInterface;

class LandingsCoreServiceProvider extends ServiceProvider
{
    public function boot()
    {
        if (
            config('landings-core-lib.project-type') === 'broker' &&
            config('landings-core-lib.loaners_api.register_routes')
        ) {
            $this->loadRoutesFrom(__DIR__ . '/routes/broker.php');
        }

        $this->loadViewsFrom(__DIR__ . '/resources/views', 'landings-core-lib');

        $this->publishes([
            __DIR__ . '/resources/views' => resource_path('views/vendor/landings-core-lib'),
        ]);

        $this->publishes(
            [
                __DIR__ . '/resources/js' => public_path('js/vendor'),
            ],
            'public'
        );
    }

    public function register()
    {
        $this->publishes(
            [
                __DIR__ . '/config.php' => config_path('landings-core-lib.php'),
            ],
            'config'
        );

        $this->mergeConfigFrom(
            __DIR__.'/config.php', 'landings-core-lib'
        );

        $this->app->singleton(
            ICoreClient::class,
            static function ($app) {
                return new CoreClient(
                    (string)config('landings-core-lib.core_client.landing_name'),
                    (string)config('landings-core-lib.core_client.auth_token'),
                    (string)config('landings-core-lib.core_client.base_uri'),
                    $app->make(Client::class),
                    $app->make(LoggerInterface::class)
                );
            }
        );

        $this->app->bind(
            OffersBuilder::class,
            static function ($app) {
                return new OffersBuilder(
                    config('landings-core-lib.young_offers_period')
                );
            }
        );

        if (config('landings-core-lib.project-type') === 'broker') {
            if ($this->app->runningInConsole()) {
                $this->commands(
                    [
                        GetOffersAndReasons::class,
                        GetOffersProfitabilityData::class,
                        SynchSignedOffLoaners::class,
                        ClearEmptySessions::class,
                        SynchTrafficSources::class,
                        ResendSessions::class,
                        UpdateAssetVersion::class,
                        RetryQueue::class,
                        SynchLoanersFeedCommand::class,
                        GetLoanerSellingServices::class,
                        GetSellingServicesAndReports::class
                    ]
                );
            }

            $this->publishes(
                [
                    __DIR__ . '/Database/migrations/brokers' => base_path('database/migrations'),
                ],
                'migrations'
            );

            $this->app->bind(
                RulesRepository::class,
                static function ($app) {
                    return new RulesRepository(
                        [
                            new UaRuleSet(),
                            new RuRuleSet(),
                        ]
                    );
                }
            );
        }

        if (config('landings-core-lib.project-type') === 'landing') {
            if ($this->app->runningInConsole()) {
                $this->commands(
                    [
                        GetOffersAndReasons::class,
                        GetOffersProfitabilityData::class,
                        SynchLandingSettings::class,
                        UpdateAssetVersion::class
                    ]
                );
            }

            $this->publishes(
                [
                    __DIR__ . '/Database/migrations/landings' => base_path('database/migrations'),
                ],
                'migrations'
            );
        }
    }
}
