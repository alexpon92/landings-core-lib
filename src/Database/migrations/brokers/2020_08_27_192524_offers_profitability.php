<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class OffersProfitability extends Migration
{
    public function up(): void
    {
        Schema::create('offer_profitabilities', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('external_id')->unique();
            $table->integer('traffic_source_id');
            $table->string('traffic_source_name');
            $table->integer('traffic_web_id');
            $table->string('traffic_web_web_id');
            $table->integer('offer_id');
            $table->integer('external_offer_id');
            $table->double('epc');

            $table->timestamps();
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('offer_profitabilities');
    }
}
