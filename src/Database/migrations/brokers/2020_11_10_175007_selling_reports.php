<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class SellingReports extends Migration
{
    /**
     * {@inheritDoc}
     */
    public function up(): void
    {
        Schema::create('selling_reports', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('external_id')->index();
            $table->integer('loaner_id')->index();
            $table->integer('personal_data_id');
            $table->jsonb('loaner_selling_service_ids');
            $table->string('transaction_amount');
            $table->string('transaction_currency');
            $table->string('transaction_local_amount');
            $table->string('transaction_local_currency');
            $table->timestamp('triggered_at');
            $table->timestamps();
        });
    }

    /**
     * {@inheritDoc}
     */
    public function down(): void
    {
        Schema::dropIfExists('selling_reports');
    }
}
