<?php
declare(strict_types=1);

namespace LandingsCore\Domain\Events;

use Carbon\Carbon;

class LoanerSignOffBadPayments
{
    /**
     * @var string
     */
    private $phone;

    /**
     * @var Carbon
     */
    private $triggeredAt;

    /**
     * LoanerSignOffBadPayments constructor.
     *
     * @param string $phone
     */
    public function __construct(string $phone)
    {
        $this->phone = $phone;
        $this->triggeredAt = now();
    }

    /**
     * @return string
     */
    public function getPhone(): string
    {
        return $this->phone;
    }

    /**
     * @return Carbon
     */
    public function getTriggeredAt(): Carbon
    {
        return $this->triggeredAt;
    }

}