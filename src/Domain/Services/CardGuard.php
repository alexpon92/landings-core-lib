<?php
declare(strict_types=1);

namespace LandingsCore\Domain\Services;

use LandingsCore\Domain\Entity\LeadCardLink;
use LandingsCore\Domain\Entity\LeadSignOutAttempt;

class CardGuard
{
    /**
     * Card step logic skip
     * 1. if card is switched off - hide
     * 2. if card is linked in another acc - hide
     * 3. if signed_of recently - hide
     *
     * @param string|null $phone
     * @param bool|null   $isCardEnabled
     *
     * @return bool
     */
    public function isNeedToHideCardStep(?string $phone, ?bool $isCardEnabled = true): bool
    {
        if (!$isCardEnabled) {
            return true;
        }

        if ($phone) {
            $leadCardLink      = LeadCardLink::getLastCardLink($phone);
            $hasSignOutAttempt = LeadSignOutAttempt::getLastSignOutAttempt($phone);
            if ($leadCardLink || $hasSignOutAttempt) {
                return true;
            }

        }

        return false;
    }
}