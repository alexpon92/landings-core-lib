<?php
declare(strict_types=1);

namespace LandingsCore\Domain\CorePackage\CoreClient;

use Carbon\Carbon;

class FeedDto
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $type;

    /**
     * @var array
     */
    private $payload;

    /**
     * @var Carbon
     */
    private $triggeredAt;

    /**
     * @var int
     */
    private $loanerId;

    /**
     * @var string
     */
    private $phone;

    /**
     * FeedDto constructor.
     *
     * @param int    $id
     * @param string $type
     * @param array  $payload
     * @param Carbon $triggeredAt
     * @param int    $loanerId
     * @param string $phone
     */
    public function __construct(
        int $id,
        string $type,
        array $payload,
        Carbon $triggeredAt,
        int $loanerId,
        string $phone
    ) {
        $this->id          = $id;
        $this->type        = $type;
        $this->payload     = $payload;
        $this->triggeredAt = $triggeredAt;
        $this->loanerId    = $loanerId;
        $this->phone       = $phone;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @return array
     */
    public function getPayload(): array
    {
        return $this->payload;
    }

    /**
     * @return Carbon
     */
    public function getTriggeredAt(): Carbon
    {
        return $this->triggeredAt;
    }

    /**
     * @return int
     */
    public function getLoanerId(): int
    {
        return $this->loanerId;
    }

    /**
     * @return string
     */
    public function getPhone(): string
    {
        return $this->phone;
    }
}