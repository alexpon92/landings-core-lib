<?php

declare(strict_types=1);

namespace LandingsCore\Domain\CorePackage;

use LandingsCore\Domain\CorePackage\CoreClient\ClientExceptions\BadResponseCode;
use LandingsCore\Domain\CorePackage\CoreClient\ClientExceptions\BadResponseFormat;
use LandingsCore\Domain\CorePackage\CoreClient\ICoreClient;
use LandingsCore\Domain\CorePackage\CoreClient\SellingReports\SellingReportDto;
use LandingsCore\Domain\Entity\SellingReport;

class SyncSellingReports
{
    /**
     * @var ICoreClient
     */
    private $client;

    /**
     * @var ImgStorageService
     */
    private $imgStorageService;

    /**
     * SyncLoanersSellingServices constructor.
     *
     * @param ICoreClient       $client
     * @param ImgStorageService $imgStorageService
     */
    public function __construct(ICoreClient $client, ImgStorageService $imgStorageService)
    {
        $this->client            = $client;
        $this->imgStorageService = $imgStorageService;
    }

    /**
     * @throws BadResponseCode
     * @throws BadResponseFormat
     */
    public function execute(): void
    {
        do {
            $reports = $this->client->getSellingReports(SellingReport::getLastExternalId());

            /** @var SellingReportDto $report */
            foreach ($reports as $report) {
                $reportEntity                             = new SellingReport();
                $reportEntity->external_id                = $report->getExternalId();
                $reportEntity->loaner_id                  = $report->getLoanerId();
                $reportEntity->personal_data_id           = $report->getPersonalDataId();
                $reportEntity->loaner_selling_service_ids = json_encode($report->getSellingServiceIds());
                $reportEntity->type                       = $report->getType();
                $reportEntity->transaction_amount         = $report->getTransactionAmount();
                $reportEntity->transaction_currency       = $report->getTransactionCurrency();
                $reportEntity->transaction_local_amount   = $report->getTransactionLocalAmount();
                $reportEntity->transaction_local_currency = $report->getTransactionLocalCurrency();
                $reportEntity->triggered_at               = $report->getReportedAt();
                $reportEntity->save();
            }
        } while (!empty($reports));
    }

}