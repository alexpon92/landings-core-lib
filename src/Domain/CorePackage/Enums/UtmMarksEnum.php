<?php
declare(strict_types=1);

namespace LandingsCore\Domain\CorePackage\Enums;

class UtmMarksEnum
{
    // Specific UTM Sources
    public const FAILED_UTM_SOURCE   = 'failed';
    public const FAILED_UTM_CAMPAIGN = 'failed';
    public const FAILED_UTM_MEDIUM   = 'failed';
}