<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class OffersAndReasons extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('leave_reasons', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('external_id')->unique();
            $table->jsonb('texts');
            $table->text('url_pattern');

            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('offers', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('external_id')->unique();
            $table->string('name');
            $table->text('img_content');
            $table->string('type');
            $table->integer('amount');
            $table->integer('weight');
            $table->string('percent');
            $table->boolean('is_in_popup');
            $table->boolean('is_in_top');

            $table->integer('duration')->nullable();
            $table->string('credit_history')->nullable();
            $table->integer('age_from')->nullable();
            $table->integer('age_to')->nullable();
            $table->string('card_state')->nullable();
            $table->decimal('probability', 4, 2)->nullable();
            $table->jsonb('click_rules')->nullable();
            $table->jsonb('additional_info')->nullable();

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('offers');
        Schema::dropIfExists('leave_reasons');
    }
}
