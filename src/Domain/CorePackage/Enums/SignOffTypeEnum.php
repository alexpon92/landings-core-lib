<?php
declare(strict_types=1);

namespace LandingsCore\Domain\CorePackage\Enums;

class SignOffTypeEnum
{
    public const TYPE_CLIENT_INITIATIVE = 'client_initiative';
    public const TYPE_BAD_PAYMENTS      = 'bad_payments';
    public const TYPE_ALL_PAID          = 'all_paid';
}