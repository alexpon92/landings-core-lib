<?php
declare(strict_types=1);

namespace LandingsCore\Domain\Events;

use Carbon\Carbon;

class LoanerSignOffAllPaid
{
    /**
     * @var
     */
    private $phone;

    /**
     * @var Carbon
     */
    private $triggeredAt;

    /**
     * LoanerSignOffAllPaid constructor.
     *
     * @param $phone
     */
    public function __construct($phone)
    {
        $this->phone       = $phone;
        $this->triggeredAt = now();
    }

    /**
     * @return mixed
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @return Carbon
     */
    public function getTriggeredAt(): Carbon
    {
        return $this->triggeredAt;
    }

}