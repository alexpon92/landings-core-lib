<?php

namespace Tests\Feature\Domain\Services\Cabinet;

use Illuminate\Foundation\Testing\RefreshDatabase;
use LandingsCore\Domain\Services\Cabinet\AuthService;
use Tests\TestCase;

class AuthServiceTest extends TestCase
{
    use RefreshDatabase;

    public function testLoanerAuthentication(): void
    {
        /** @var AuthService $service */
        $service = $this->app->make(AuthService::class);

        $service->execute('+79099991122');

        $this->assertAuthenticated();
    }
}
