<?php
declare(strict_types=1);

namespace LandingsCore\Domain\Services\OfferVendorsDuplicatesCheck\CheckServices\Vendors;

use GuzzleHttp\Client;
use LandingsCore\Domain\Services\OfferVendorsDuplicatesCheck\CheckServices\AbstractCheckService;
use LandingsCore\Domain\Services\OfferVendorsDuplicatesCheck\CheckServices\CheckResponse;
use LandingsCore\Domain\Services\OfferVendorsDuplicatesCheck\CheckServices\Exceptions\BadResponseCodeException;
use LandingsCore\Domain\Services\OfferVendorsDuplicatesCheck\CheckServices\VendorsEnum;
use Psr\Log\LoggerInterface;

class CreditPlus extends AbstractCheckService
{
    /**
     * @var Client
     */
    private $httpClient;

    /**
     * @var string
     */
    private $login;

    /**
     * @var string
     */
    private $secret;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * CreditPlus constructor.
     *
     * @param Client          $httpClient
     * @param string          $baseUri
     * @param string          $login
     * @param string          $secret
     * @param LoggerInterface $logger
     */
    public function __construct(
        Client $httpClient,
        string $baseUri,
        string $login,
        string $secret,
        LoggerInterface $logger
    ) {
        $this->httpClient = new Client(
            array_merge(
                $httpClient->getConfig(),
                [
                    'headers'     => [
                        'content-type' => 'application/json',
                        'accept'       => 'application/json'
                    ],
                    'base_uri'    => $baseUri,
                    'http_errors' => false
                ]
            )
        );
        $this->login      = $login;
        $this->secret     = $secret;
        $this->logger     = $logger;
    }

    public static function getServiceKey(): string
    {
        return strtolower(VendorsEnum::TYPE_RU_CREDIT_PLUS);
    }

    protected function check(string $phone, ?array $data = []): CheckResponse
    {
        $payload = [
            'auth'         => [
                'client' => $this->login,
                'secret' => $this->secret
            ],
            'mobile_phone' => str_replace('+', '', $phone)
        ];

        $this->logger->debug(
            'Check duplicates in creditplus vendor',
            [
                'payload' => $payload
            ]
        );

        $response = $this->httpClient->post(
            '',
            [
                'body' => json_encode($payload)
            ]
        );
        $contents = $response->getBody()->getContents();

        $this->logger->debug(
            'Credit plus response',
            [
                'response_code' => $response->getStatusCode(),
                'response'      => $contents,
                'payload'       => $payload
            ]
        );

        $code = $response->getStatusCode();
        if ($code !== 200 && $code !== 422) {
            $this->logger->error(
                'Bad response code from creditplus vendor',
                [
                    'response_code' => $code,
                    'response'      => $contents,
                    'payload'       => $payload
                ]
            );
            throw new BadResponseCodeException("Bad response code from creditplus vendor {$code}");
        }

        if ($code === 200) {
            return CheckResponse::buildForNotExists(static::getServiceKey());
        }

        return CheckResponse::buildForExists(static::getServiceKey());
    }
}