<?php

declare(strict_types=1);

namespace LandingsCore\Domain\CorePackage\Enums;

class CardStateEnum
{
    public const LINKED     = 'linked';
    public const NOT_LINKED = 'not_linked';
    public const ALL        = 'all';

    /**
     * @return array
     */
    public static function getAll(): array
    {
        return [
            self::ALL,
            self::LINKED,
            self::NOT_LINKED
        ];
    }

}