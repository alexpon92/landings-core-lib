<?php
declare(strict_types=1);

namespace LandingsCore\Domain\Entity;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * LandingsCore\Domain\Entity\OfferProfitability
 *
 * @property int         $id
 * @property int         $external_id
 * @property int         $traffic_source_id
 * @property string      $traffic_source_name
 * @property int         $traffic_web_id
 * @property string      $traffic_web_web_id
 * @property int         $offer_id
 * @property int         $external_offer_id
 * @property float       $epc
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property Carbon|null $deleted_at
 * @method static whereNotIn($column, $values, $boolean = 'and')
 * @method static \Illuminate\Database\Eloquent\Builder|OfferProfitability newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|OfferProfitability newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|OfferProfitability query()
 * @method static \Illuminate\Database\Eloquent\Builder|OfferProfitability whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OfferProfitability whereEpc($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OfferProfitability whereExternalId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OfferProfitability whereExternalOfferId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OfferProfitability whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OfferProfitability whereOfferId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OfferProfitability whereTrafficSourceId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OfferProfitability whereTrafficSourceName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OfferProfitability whereTrafficWebId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OfferProfitability whereTrafficWebWebId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OfferProfitability whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class OfferProfitability extends Model
{
}
