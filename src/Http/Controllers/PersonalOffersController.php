<?php
declare(strict_types=1);

namespace LandingsCore\Http\Controllers;

use Illuminate\Auth\SessionGuard;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use LandingsCore\Domain\Entity\PersonalData;
use LandingsCore\Domain\Services\Cabinet\AuthService;
use Psr\Log\LoggerInterface;

class PersonalOffersController extends Controller
{
    private LoggerInterface $logger;

    /**
     * PersonalOffersController constructor.
     *
     * @param LoggerInterface $logger
     */
    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    public function redirectWithData(AuthService $authService, Request $request, $hash)
    {
        $personalData = PersonalData::whereCoreHash($hash)->first();

        if ($personalData === null) {
            return redirect()->route(config('landings-core-lib.general_offers_route'));
        }

        try {
            $authService->execute($personalData->phone);
        } catch (\Throwable $e) {
            $this->logger->error(
                'Cannot auto sign in loaner',
                [
                    'error' => $e->getMessage()
                ]
            );
            return redirect()->route(config('landings-core-lib.general_offers_route'));
        }

        $keyParts = [
            'login',
            config('auth.defaults.guard'),
            sha1(SessionGuard::class)
        ];
        $key      = implode('_', $keyParts);

        $request->session()->put($key, $request->user()->id);

        return view('landings-core-lib::web.redirect', [
            'route' => route(config('landings-core-lib.personal_offers_route'))
        ]);
    }
}
