<?php
declare(strict_types=1);

namespace LandingsCore\Domain\CorePackage\Enums;

class ActivityTypeEnum
{
    public const NO_EMPLOYMENT    = 'no';
    public const OFFICIAL         = 'official';
    public const PRIVATE          = 'private';
    public const UNOFFICIAL       = 'unofficial';
    public const MATERNITY_LEAVE  = 'maternity_leave';
    public const PENSIONER        = 'pensioner';
    public const STUDENT          = 'student';

    public static function getAll(): array
    {
        return [
            self::NO_EMPLOYMENT,
            self::OFFICIAL,
            self::PRIVATE,
            self::UNOFFICIAL,
            self::MATERNITY_LEAVE,
            self::PENSIONER,
            self::STUDENT
        ];
    }
}