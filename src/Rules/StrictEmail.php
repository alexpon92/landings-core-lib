<?php

declare(strict_types=1);

namespace LandingsCore\Rules;


use Illuminate\Contracts\Validation\Rule;
use LandingsCore\Domain\CorePackage\CoreClient\ICoreClient;

final class StrictEmail implements Rule
{
    public function passes($attribute, $value): bool
    {
        /** @var ICoreClient $client */
        $client = app(ICoreClient::class);

        return $client->validateEmail($value);
    }

    public function message(): string
    {
        return 'The :attribute should be a valid email.';
    }
}
