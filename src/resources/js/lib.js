function trackOfferClick(route, id) {
    $.post({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
        },
        data: {
            id: id,
        },
        url: route
    });
}

function showMoreOffers(offerContainersSelector, buttonSelector) {
    let containers = $(offerContainersSelector);
    for (let i = 0; i < containers.length; i++) {
        if ($(containers[i]).is(':hidden')) {
            $(containers[i]).slideDown();

            if (i === containers.length - 1) {
                $(buttonSelector).hide();
            }
            return;
        }
    }
}

