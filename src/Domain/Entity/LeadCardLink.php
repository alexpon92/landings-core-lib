<?php
declare(strict_types=1);

namespace LandingsCore\Domain\Entity;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * App\Domain\Entity\LeadCardLink
 *
 * @property string                          $phone
 * @property string                          $token
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|LeadCardLink newModelQuery()
 * @method static Builder|LeadCardLink newQuery()
 * @method static Builder|LeadCardLink query()
 * @method static Builder|LeadCardLink whereCreatedAt($value)
 * @method static Builder|LeadCardLink wherePhone($value)
 * @method static Builder|LeadCardLink whereToken($value)
 * @method static Builder|LeadCardLink whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property int                             $id
 * @method static Builder|LeadCardLink whereId($value)
 */
class LeadCardLink extends Model
{
    public static function getLastCardLink(string $phone): ?self
    {
        /** @var LeadCardLink|null $leadCardLink */
        $leadCardLink = self::query()
                            ->where('phone', $phone)
                            ->orderBy('created_at', 'DESC')
                            ->first();

        return $leadCardLink;
    }

    public static function findByPhone(string $phone): ?self
    {
        /** @var LeadCardLink|null $leadCardLink */
        $leadCardLink = self::query()
            ->where('phone', $phone)
            ->first();

        return $leadCardLink;
    }
}