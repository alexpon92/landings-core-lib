<?php

Route::middleware('api')->post(
    'api/loaner',
    \LandingsCore\Http\Controllers\Api\Loaners\LoanerApiController::class . '@storeLoaner'
);

Route::middleware('web')->get(
    'form/{id}',
    \LandingsCore\Http\Controllers\PersonalFormController::class . '@redirectWithData'
);

Route::middleware('web')->get(
    'personal-offers/{hash}',
    \LandingsCore\Http\Controllers\PersonalOffersController::class . '@redirectWithData'
);

Route::middleware('web')->post('/offers/track', \LandingsCore\Http\Controllers\TrackOffersController::class . '@track')
    ->name('broker.offers_track');
