<?php
declare(strict_types=1);

namespace LandingsCore\Domain\CorePackage\CoreClient;

class LoanerDto implements \JsonSerializable
{
    /**
     * @var string
     */
    private $id;

    /**
     * @var null|string
     */
    private $utmSource;

    /**
     * @var null|string
     */
    private $utmCampaign;

    /**
     * @var null|string
     */
    private $utmMedium;

    /**
     * @var null|string
     */
    private $phone;

    /**
     * @var null|string
     */
    private $email;

    /**
     * @var null|string
     */
    private $agreementAcceptedAt;
    /**
     * @var string|null
     */
    private $firstName;
    /**
     * @var string|null
     */
    private $lastName;
    /**
     * @var string|null
     */
    private $patronymic;
    /**
     * @var string|null
     */
    private $birthday;
    /**
     * @var string|null
     */
    private $gender;
    /**
     * @var int|null
     */
    private $loanSum;
    /**
     * @var string|null
     */
    private $loanAim;
    /**
     * @var string|null
     */
    private $passportSerial;
    /**
     * @var string|null
     */
    private $passportNumber;
    /**
     * @var string|null
     */
    private $passportDepCode;
    /**
     * @var string|null
     */
    private $passportIssuedBy;
    /**
     * @var string|null
     */
    private $birthPlace;
    /**
     * @var string|null
     */
    private $idCardNumber;
    /**
     * @var string|null
     */
    private $idCardExpireDate;
    /**
     * @var string|null
     */
    private $inn;
    /**
     * @var string|null
     */
    private $region;
    /**
     * @var string|null
     */
    private $city;
    /**
     * @var string|null
     */
    private $street;
    /**
     * @var string|null
     */
    private $house;
    /**
     * @var string|null
     */
    private $flat;
    /**
     * @var string|null
     */
    private $regionFact;
    /**
     * @var string|null
     */
    private $cityFact;
    /**
     * @var string|null
     */
    private $streetFact;
    /**
     * @var string|null
     */
    private $houseFact;
    /**
     * @var string|null
     */
    private $flatFact;
    /**
     * @var string|null
     */
    private $creditHistory;
    /**
     * @var string|null
     */
    private $employment;
    /**
     * @var string|null
     */
    private $bankCardToken;
    /**
     * @var string|null
     */
    private $bankCardFirstSix;
    /**
     * @var string|null
     */
    private $bankCardLastFour;

    /**
     * @var string|null
     */
    private $tokenCreatedAt;

    /**
     * @var array|null
     */
    private $queryArr;

    /**
     * @var int|null
     */
    private $formStep;

    /**
     * LoanerDto constructor.
     *
     * @param string      $id
     * @param string|null $utmSource
     * @param string|null $utmCampaign
     * @param string|null $utmMedium
     * @param string|null $phone
     * @param string|null $email
     * @param string|null $agreementAcceptedAt
     * @param string|null $firstName
     * @param string|null $lastName
     * @param string|null $patronymic
     * @param string|null $birthday
     * @param string|null $gender
     * @param int|null    $loanSum
     * @param string|null $loanAim
     * @param string|null $passportSerial
     * @param string|null $passportNumber
     * @param string|null $passportDepCode
     * @param string|null $passportIssuedBy
     * @param string|null $birthPlace
     * @param string|null $idCardNumber
     * @param string|null $idCardExpireDate
     * @param string|null $inn
     * @param string|null $region
     * @param string|null $city
     * @param string|null $street
     * @param string|null $house
     * @param string|null $flat
     * @param string|null $regionFact
     * @param string|null $cityFact
     * @param string|null $streetFact
     * @param string|null $houseFact
     * @param string|null $flatFact
     * @param string|null $creditHistory
     * @param string|null $employment
     * @param string|null $bankCardToken
     * @param string|null $bankCardFirstSix
     * @param string|null $bankCardLastFour
     * @param string|null $tokenCreatedAt
     * @param array|null  $queryArr
     * @param int|null    $formStep
     */
    public function __construct(
        string $id,
        ?string $utmSource,
        ?string $utmCampaign,
        ?string $utmMedium,
        ?string $phone,
        ?string $email,
        ?string $agreementAcceptedAt,
        ?string $firstName,
        ?string $lastName,
        ?string $patronymic,
        ?string $birthday,
        ?string $gender,
        ?int $loanSum,
        ?string $loanAim,
        ?string $passportSerial,
        ?string $passportNumber,
        ?string $passportDepCode,
        ?string $passportIssuedBy,
        ?string $birthPlace,
        ?string $idCardNumber,
        ?string $idCardExpireDate,
        ?string $inn,
        ?string $region,
        ?string $city,
        ?string $street,
        ?string $house,
        ?string $flat,
        ?string $regionFact,
        ?string $cityFact,
        ?string $streetFact,
        ?string $houseFact,
        ?string $flatFact,
        ?string $creditHistory,
        ?string $employment,
        ?string $bankCardToken,
        ?string $bankCardFirstSix,
        ?string $bankCardLastFour,
        ?string $tokenCreatedAt,
        ?array $queryArr,
        ?int $formStep
    ) {
        $this->id                  = $id;
        $this->utmSource           = $utmSource;
        $this->utmCampaign         = $utmCampaign;
        $this->utmMedium           = $utmMedium;
        $this->phone               = $phone;
        $this->email               = $email;
        $this->agreementAcceptedAt = $agreementAcceptedAt;
        $this->firstName           = $firstName;
        $this->lastName            = $lastName;
        $this->patronymic          = $patronymic;
        $this->birthday            = $birthday;
        $this->gender              = $gender;
        $this->loanSum             = $loanSum;
        $this->loanAim             = $loanAim;
        $this->passportSerial      = $passportSerial;
        $this->passportNumber      = $passportNumber;
        $this->passportDepCode     = $passportDepCode;
        $this->passportIssuedBy    = $passportIssuedBy;
        $this->birthPlace          = $birthPlace;
        $this->idCardNumber        = $idCardNumber;
        $this->idCardExpireDate    = $idCardExpireDate;
        $this->inn                 = $inn;
        $this->region              = $region;
        $this->city                = $city;
        $this->street              = $street;
        $this->house               = $house;
        $this->flat                = $flat;
        $this->regionFact          = $regionFact;
        $this->cityFact            = $cityFact;
        $this->streetFact          = $streetFact;
        $this->houseFact           = $houseFact;
        $this->flatFact            = $flatFact;
        $this->creditHistory       = $creditHistory;
        $this->employment          = $employment;
        $this->bankCardToken       = $bankCardToken;
        $this->bankCardFirstSix    = $bankCardFirstSix;
        $this->bankCardLastFour    = $bankCardLastFour;
        $this->tokenCreatedAt      = $tokenCreatedAt;
        $this->queryArr            = $queryArr;
        $this->formStep            = $formStep;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getUtmSource(): ?string
    {
        return $this->utmSource;
    }

    /**
     * @return string|null
     */
    public function getUtmCampaign(): ?string
    {
        return $this->utmCampaign;
    }

    /**
     * @return string|null
     */
    public function getUtmMedium(): ?string
    {
        return $this->utmMedium;
    }

    /**
     * @return string|null
     */
    public function getPhone(): ?string
    {
        return $this->phone;
    }

    /**
     * @return string|null
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * @return string|null
     */
    public function getAgreementAcceptedAt(): ?string
    {
        return $this->agreementAcceptedAt;
    }

    /**
     * @return string|null
     */
    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    /**
     * @return string|null
     */
    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    /**
     * @return string|null
     */
    public function getPatronymic(): ?string
    {
        return $this->patronymic;
    }

    /**
     * @return string|null
     */
    public function getBirthday(): ?string
    {
        return $this->birthday;
    }

    /**
     * @return string|null
     */
    public function getGender(): ?string
    {
        return $this->gender;
    }

    /**
     * @return int|null
     */
    public function getLoanSum(): ?int
    {
        return $this->loanSum;
    }

    /**
     * @return string|null
     */
    public function getLoanAim(): ?string
    {
        return $this->loanAim;
    }

    /**
     * @return string|null
     */
    public function getPassportSerial(): ?string
    {
        return $this->passportSerial;
    }

    /**
     * @return string|null
     */
    public function getPassportNumber(): ?string
    {
        return $this->passportNumber;
    }

    /**
     * @return string|null
     */
    public function getPassportDepCode(): ?string
    {
        return $this->passportDepCode;
    }

    /**
     * @return string|null
     */
    public function getPassportIssuedBy(): ?string
    {
        return $this->passportIssuedBy;
    }

    /**
     * @return string|null
     */
    public function getBirthPlace(): ?string
    {
        return $this->birthPlace;
    }

    /**
     * @return string|null
     */
    public function getIdCardNumber(): ?string
    {
        return $this->idCardNumber;
    }

    /**
     * @return string|null
     */
    public function getIdCardExpireDate(): ?string
    {
        return $this->idCardExpireDate;
    }

    /**
     * @return string|null
     */
    public function getInn(): ?string
    {
        return $this->inn;
    }

    /**
     * @return string|null
     */
    public function getRegion(): ?string
    {
        return $this->region;
    }

    /**
     * @return string|null
     */
    public function getCity(): ?string
    {
        return $this->city;
    }

    /**
     * @return string|null
     */
    public function getStreet(): ?string
    {
        return $this->street;
    }

    /**
     * @return string|null
     */
    public function getHouse(): ?string
    {
        return $this->house;
    }

    /**
     * @return string|null
     */
    public function getFlat(): ?string
    {
        return $this->flat;
    }

    /**
     * @return string|null
     */
    public function getRegionFact(): ?string
    {
        return $this->regionFact;
    }

    /**
     * @return string|null
     */
    public function getCityFact(): ?string
    {
        return $this->cityFact;
    }

    /**
     * @return string|null
     */
    public function getStreetFact(): ?string
    {
        return $this->streetFact;
    }

    /**
     * @return string|null
     */
    public function getHouseFact(): ?string
    {
        return $this->houseFact;
    }

    /**
     * @return string|null
     */
    public function getFlatFact(): ?string
    {
        return $this->flatFact;
    }

    /**
     * @return string|null
     */
    public function getCreditHistory(): ?string
    {
        return $this->creditHistory;
    }

    /**
     * @return string|null
     */
    public function getEmployment(): ?string
    {
        return $this->employment;
    }

    /**
     * @return string|null
     */
    public function getBankCardToken(): ?string
    {
        return $this->bankCardToken;
    }

    /**
     * @return string|null
     */
    public function getBankCardFirstSix(): ?string
    {
        return $this->bankCardFirstSix;
    }

    /**
     * @return string|null
     */
    public function getBankCardLastFour(): ?string
    {
        return $this->bankCardLastFour;
    }

    /**
     * @return string|null
     */
    public function getTokenCreatedAt(): ?string
    {
        return $this->tokenCreatedAt;
    }

    /**
     * @return array|null
     */
    public function getQueryArr(): ?array
    {
        return $this->queryArr;
    }

    /**
     * @return int|null
     */
    public function getFormStep(): ?int
    {
        return $this->formStep;
    }


    public function jsonSerialize()
    {
        return [
            'id'    => $this->id,
            'phone' => $this->phone,

            'first_name' => $this->firstName,
            'last_name'  => $this->lastName,
            'patronymic' => $this->patronymic,
            'email'      => $this->email,
            'birthday'   => $this->birthday,
            'gender'     => $this->gender,
            'loan_sum'   => $this->loanSum,
            'loan_aim'   => $this->loanAim,

            'passport_number'     => $this->passportNumber,
            'passport_serial'     => $this->passportSerial,
            'passport_dep_code'   => $this->passportDepCode,
            'passport_issued_by'  => $this->passportIssuedBy,
            'birth_place'         => $this->birthPlace,
            'id_card_number'      => $this->idCardNumber,
            'id_card_expire_date' => $this->idCardExpireDate,
            'inn'                 => $this->inn,

            'region' => $this->region,
            'city'   => $this->city,
            'street' => $this->street,
            'house'  => $this->house,
            'flat'   => $this->flat,

            'region_fact' => $this->regionFact,
            'city_fact'   => $this->cityFact,
            'street_fact' => $this->streetFact,
            'house_fact'  => $this->houseFact,
            'flat_fact'   => $this->flatFact,

            'credit_history'             => $this->creditHistory,
            'employment'                 => $this->employment,
            'utm_source'                 => $this->utmSource,
            'utm_campaign'               => $this->utmCampaign,
            'utm_medium'                 => $this->utmMedium,
            'bank_card_token'            => $this->bankCardToken,
            'bank_card_first_six'        => $this->bankCardFirstSix,
            'bank_card_last_four'        => $this->bankCardLastFour,
            'agreement_accepted_at'      => $this->agreementAcceptedAt,
            'bank_card_token_created_at' => $this->tokenCreatedAt,
            'query_arr'                  => $this->queryArr,

            'form_step' => $this->formStep
        ];
    }
}