<?php
declare(strict_types=1);

namespace LandingsCore\Listeners;

use LandingsCore\Domain\CorePackage\CoreClient\ClientExceptions\BadResponseCode;
use LandingsCore\Domain\CorePackage\CoreClient\ClientExceptions\BadResponseFormat;
use LandingsCore\Domain\CorePackage\CoreClient\ICoreClient;
use LandingsCore\Domain\CorePackage\CoreClient\LoanerDto;
use LandingsCore\Domain\Services\Cabinet\ProcessPersonalData;
use LandingsCore\Domain\Entity\PersonalData;
use LandingsCore\Domain\Entity\Session;
use LandingsCore\Domain\Events\LoanerUpdated;
use Psr\Log\LoggerInterface;

class LoanerSubscriber extends AbstractListener
{
    /**
     * @var ICoreClient
     */
    private $coreClient;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var ProcessPersonalData
     */
    private $processPersonalData;

    /**
     * LoanerSubscriber constructor.
     * @param ICoreClient         $coreClient
     * @param LoggerInterface     $logger
     * @param ProcessPersonalData $processPersonalData
     */
    public function __construct(
        ICoreClient $coreClient,
        LoggerInterface $logger,
        ProcessPersonalData $processPersonalData
    ) {
        $this->coreClient          = $coreClient;
        $this->logger              = $logger;
        $this->processPersonalData = $processPersonalData;
    }

    /**
     * @param LoanerUpdated $event
     *
     * @throws BadResponseCode
     * @throws BadResponseFormat
     */
    public function onLoanerUpdated(LoanerUpdated $event): void
    {
        if ($event->getPhone()) {
            $response = $this->coreClient->storeLoaner(
                new LoanerDto(
                    $event->getSessionId(),
                    $event->getUtmSource(),
                    $event->getUtmCampaign(),
                    $event->getUtmMedium(),
                    $event->getPhone(),
                    $event->getEmail(),
                    $event->getAgreementAcceptedAt(),
                    $event->getFirstName(),
                    $event->getLastName(),
                    $event->getPatronymic(),
                    $event->getBirthday(),
                    $event->getGender(),
                    $event->getLoanSum(),
                    $event->getLoanAim(),

                    $event->getPassportSerial(),
                    $event->getPassportNumber(),
                    $event->getPassportDepCode(),
                    $event->getPassportIssuedBy(),
                    $event->getBirthPlace(),
                    $event->getIdCardNumber(),
                    $event->getIdCardExpireDate(),
                    $event->getInn(),

                    $event->getRegion(),
                    $event->getCity(),
                    $event->getStreet(),
                    $event->getHouse(),
                    $event->getFlat(),

                    $event->getRegionFact(),
                    $event->getCityFact(),
                    $event->getStreetFact(),
                    $event->getHouseFact(),
                    $event->getFlatFact(),

                    $event->getCreditHistory(),
                    $event->getEmployment(),
                    $event->getBankCardToken(),
                    $event->getBankCardFirstSix(),
                    $event->getBankCardLastFour(),
                    $event->getTokenCreatedAt(),
                    $event->getQueryArr(),

                    $event->getFormStep()
                )
            );

            if ($response) {
                $session = Session::find($event->getSessionId());
                if (!$session) {
                    $this->logger->error(
                        'Session is not found after storing loaner in core',
                        [
                            'session_id' => $event->getSessionId(),
                        ]
                    );
                    return;
                }

                if (!$session->core_loaner_id) {
                    $session->setLoanerDataFromCoreResponse($response);
                }


                if (config('landings-core-lib.enable_cabinet')) {
                    $personalData = $this->processPersonalData->execute(
                        $event->getPhone(),
                        $event->getSessionId(),
                        $event->getRawPayload()
                    );

                    if ($personalData->session_id === $event->getSessionId()) {
                        $personalData->setLoanerDataFromCoreResponse($response);
                    }
                }
            }
        }
    }

    public function getConnection(): string
    {
        return (string)config('landings-core-lib.loaners_send_queue.connection');
    }

    public function getQueue(): string
    {
        return (string)config('landings-core-lib.loaners_send_queue.name');
    }

    /**
     * Register the listeners for the subscriber.
     *
     * @param \Illuminate\Events\Dispatcher $events
     */
    public function subscribe($events)
    {
        $events->listen(
            LoanerUpdated::class,
            self::class . '@onLoanerUpdated'
        );

    }
}