<?php
declare(strict_types=1);

namespace LandingsCore\Console\Commands;

use Illuminate\Console\Command;
use LandingsCore\Domain\Services\AssetManager;

class UpdateAssetVersion extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'core:update-asset-version';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update asset version on deploy';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        AssetManager::changeVersion();
    }
}