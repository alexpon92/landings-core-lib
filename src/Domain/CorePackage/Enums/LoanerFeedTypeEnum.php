<?php
declare(strict_types=1);

namespace LandingsCore\Domain\CorePackage\Enums;


class LoanerFeedTypeEnum
{
    public const SMS                = 'sms';
    public const SMS_SIGN_OFF       = 'sms_sign_off';
    public const TRANSACTION        = 'transaction';
    public const FAILED_TRANSACTION = 'failed_transaction';

    public static function getAll(): array
    {
        return [
            self::SMS,
            self::SMS_SIGN_OFF,
            self::TRANSACTION,
            self::FAILED_TRANSACTION
        ];
    }

}
