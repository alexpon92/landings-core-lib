<?php
declare(strict_types=1);

namespace LandingsCore\Domain\CorePackage;

use LandingsCore\Domain\Entity\Offer;
use LandingsCore\Domain\CorePackage\CoreClient\ICoreClient;
use LandingsCore\Domain\CorePackage\CoreClient\OfferDto;

class SynchOffersService
{
    /**
     * @var ICoreClient
     */
    private $client;

    /**
     * @var ImgStorageService
     */
    private $imgStorageService;

    /**
     * SynchOffersService constructor.
     *
     * @param ICoreClient       $client
     * @param ImgStorageService $imgStorageService
     */
    public function __construct(ICoreClient $client, ImgStorageService $imgStorageService)
    {
        $this->client            = $client;
        $this->imgStorageService = $imgStorageService;
    }

    /**
     * @throws \Exception
     */
    public function synch(): void
    {
        $offers   = $this->client->getOffers();
        $offerIds = array_map(
            static function (OfferDto $elem) {
                return $elem->getId();
            },
            $offers
        );

        $time = time();

        foreach ($offers as $offer) {
            $imgToDelete = null;

            $offerEntity = Offer::findByExternalId($offer->getId());
            if (!$offerEntity) {
                $offerEntity = new Offer();
            }

            $offerEntity->external_id     = $offer->getId();
            $offerEntity->name            = $offer->getName();
            $offerEntity->type            = $offer->getType();
            $offerEntity->amount          = $offer->getAmount();
            $offerEntity->weight          = $offer->getWeight();
            $offerEntity->percent         = $offer->getPercent();
            $offerEntity->is_in_top       = $offer->isInTop();
            $offerEntity->is_in_popup     = $offer->isInPopup();
            $offerEntity->duration        = $offer->getDuration();
            $offerEntity->credit_history  = $offer->getCreditHistory();

            $imgUrl = $this->imgStorageService->storeOfferImage($offer->getImgContent(), $offer->getId());

            $offerEntity->img_content     = $imgUrl . '?t=' . $time;
            $offerEntity->age_from        = $offer->getAgeFrom();
            $offerEntity->age_to          = $offer->getAgeTo();
            $offerEntity->card_state      = $offer->getCardState();
            $offerEntity->probability     = $offer->getProbability();
            $offerEntity->click_rules     = json_encode($offer->getClickRules());
            $offerEntity->setAdditionalInfo($offer->getAdditionalInfo());
            $offerEntity->setSettings($offer->getSettings());
            $offerEntity->deleted_at  = null;
            $offerEntity->vendor_name = mb_strtolower($offer->getName());
            $offerEntity->epc         = $offer->getEpc();
            $offerEntity->save();
        }

        $offersToDeleteQuery = Offer::whereNotIn('external_id', $offerIds);
        $offersToDelete = $offersToDeleteQuery->get();

        /** @var Offer $item */
        foreach ($offersToDelete as $item) {
            [$path, $stamp] = explode('?', $item->img_content);
            $this->imgStorageService->deleteImage($path);
        }

        $offersToDeleteQuery->delete();

    }

}
