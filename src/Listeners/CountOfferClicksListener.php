<?php
declare(strict_types=1);

namespace LandingsCore\Listeners;

use Illuminate\Events\Dispatcher;
use LandingsCore\Domain\CorePackage\CoreClient\ICoreClient;
use LandingsCore\Domain\Events\OfferClicked;

final class CountOfferClicksListener extends AbstractListener
{
    /**
     * @var ICoreClient
     */
    private $client;

    /**
     * CountOfferClicksListener constructor.
     *
     * @param ICoreClient $client
     */
    public function __construct(ICoreClient $client) {
        $this->client = $client;
    }

    /**
     * @param OfferClicked $event
     */
    public function onOfferClicked(OfferClicked $event): void
    {
        $this->client->trackOfferClick(
            $event->getOfferId(),
            $event->getTriggeredAt(),
            $event->getSessionId(),
            $event->getUtmSource(),
            $event->getUtmCampaign(),
            $event->getUtmMedium()
        );
    }

    public function getConnection(): string
    {
        return (string)config('landings-core-lib.count_offer_clicks.connection');
    }

    public function getQueue(): string
    {
        return (string)config('landings-core-lib.count_offer_clicks.name');
    }

    /**
     * Register the listeners for the subscriber.
     *
     * @param Dispatcher $events
     */
    public function subscribe(Dispatcher $events): void
    {
        $events->listen(
            OfferClicked::class,
            self::class . '@onOfferClicked'
        );
    }
}
