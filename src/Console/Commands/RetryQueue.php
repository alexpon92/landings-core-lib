<?php
declare(strict_types=1);

namespace LandingsCore\Console\Commands;

use Illuminate\Console\Command;

class RetryQueue extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'broker:retry-queue {--names=} {--limit=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Dispatch some failed jobs from specific queue';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(): void
    {
        $queueNames = $this->option('names');
        if (!$queueNames) {
            $this->warn('No queue names specified');
            return;
        }

        $limit = $this->option('limit');
        if (!$limit) {
            $this->warn('Limit is empty or not specified');
            return;
        }

        $queues = explode(',', $queueNames);


        $jobs = \DB::table((string)config('queue.failed.table'))
                   ->whereIn('queue', $queues)
                   ->orderBy('failed_at')
                   ->limit($limit)
                   ->get();

        $this->info("Get {$jobs->count()} from queues: {$queueNames}");
        if ($jobs->isNotEmpty()) {
            foreach ($jobs as $job) {
                \Artisan::call("queue:retry {$job->id}");
            }

            $this->info('Jobs dispatched again!');
        }
    }
}