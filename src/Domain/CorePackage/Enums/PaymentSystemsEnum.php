<?php
declare(strict_types=1);

namespace LandingsCore\Domain\CorePackage\Enums;

class PaymentSystemsEnum
{
    public const CLOUD_PAYMENTS = 'cloud_payments';
    public const PLATON         = 'platon';
    public const YKASSA         = 'ykassa';
    public const PROCESSING_KZ  = 'processing_kz';
}