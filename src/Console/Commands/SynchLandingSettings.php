<?php

namespace LandingsCore\Console\Commands;

use Illuminate\Console\Command;
use LandingsCore\Domain\CorePackage\SynchLandingSettingsService;

class SynchLandingSettings extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'core:synch-landing-settings';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Synchronize content, settings, utm from core';

    /**
     * @param SynchLandingSettingsService $service
     * @throws \Exception
     */
    public function handle(SynchLandingSettingsService $service)
    {
        $service->synch();
    }
}
