<?php
declare(strict_types=1);

namespace LandingsCore\Domain\Services\Cabinet;

use LandingsCore\Domain\Entity\LeadSignOutAttempt;
use LandingsCore\Domain\Entity\PersonalData;
use LandingsCore\Domain\Services\Cabinet\Exceptions\PersonalDataNotFoundException;
use LandingsCore\Domain\Services\Cabinet\Exceptions\SignOutException;
use PhoneVerifier\Domain\Entity\VerificationLog;
use PhoneVerifier\Domain\Services\Ip\IpResolver;
use PhoneVerifier\Domain\Services\Verification\SendVerificationSms;

class VerifyService
{
    private $sendVerificationSms;

    private $ipResolver;

    public function __construct(
        SendVerificationSms $sendVerificationSms,
        IpResolver $ipResolver
    ) {
        $this->sendVerificationSms = $sendVerificationSms;
        $this->ipResolver          = $ipResolver;
    }

    public function execute(string $phone, string $ip, string $sessionId): VerificationLog
    {
        if (null !== LeadSignOutAttempt::getLastSignOutAttempt($phone)) {
            throw new SignOutException("Loaner signed out phone: {$phone}");
        }

        if (null === PersonalData::findByPhone($phone)) {
            throw new PersonalDataNotFoundException("Personal data not found by phone {$phone}");
        }

        return $this->sendVerificationSms->execute($phone, $ip);
    }

}