<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class RuRegionCodes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ru_region_codes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('region_name');
            $table->string('gibdd_code')->unique();
            $table->string('okato_code');
            $table->string('oktmo_code')->nullable();
            $table->index('okato_code');
        });

        DB::table('ru_region_codes')->insert(
            [
                [
                    'region_name' => 'Республика Адыгея',
                    'gibdd_code'  => '01',
                    'okato_code'  => '79',
                    'oktmo_code'  => null,
                ],
                [
                    'region_name' => 'Республика Алтай',
                    'gibdd_code'  => '04',
                    'okato_code'  => '84',
                    'oktmo_code'  => null,
                ],
                [
                    'region_name' => 'Республика Башкортостан',
                    'gibdd_code'  => '02',
                    'okato_code'  => '80',
                    'oktmo_code'  => null,
                ],
                [
                    'region_name' => 'Республика Башкортостан',
                    'gibdd_code'  => '102',
                    'okato_code'  => '80',
                    'oktmo_code'  => null,
                ],
                [
                    'region_name' => 'Республика Башкортостан',
                    'gibdd_code'  => '702',
                    'okato_code'  => '80',
                    'oktmo_code'  => null,
                ],
                [
                    'region_name' => 'Республика Бурятия',
                    'gibdd_code'  => '03',
                    'okato_code'  => '81',
                    'oktmo_code'  => null,
                ],
                [
                    'region_name' => 'Республика Дагестан',
                    'gibdd_code'  => '05',
                    'okato_code'  => '82',
                    'oktmo_code'  => null,
                ],
                [
                    'region_name' => 'Республика Ингушетия',
                    'gibdd_code'  => '06',
                    'okato_code'  => '26',
                    'oktmo_code'  => null,
                ],
                [
                    'region_name' => 'Кабардино-Балкарская Республика',
                    'gibdd_code'  => '07',
                    'okato_code'  => '83',
                    'oktmo_code'  => null,
                ],
                [
                    'region_name' => 'Республика Калмыкия',
                    'gibdd_code'  => '08',
                    'okato_code'  => '85',
                    'oktmo_code'  => null,
                ],
                [
                    'region_name' => 'Карачаево-Черкесская Республика',
                    'gibdd_code'  => '09',
                    'okato_code'  => '91',
                    'oktmo_code'  => null,
                ],
                [
                    'region_name' => 'Республика Карелия',
                    'gibdd_code'  => '10',
                    'okato_code'  => '86',
                    'oktmo_code'  => null,
                ],
                [
                    'region_name' => 'Республика Коми',
                    'gibdd_code'  => '11',
                    'okato_code'  => '87',
                    'oktmo_code'  => null,
                ],
                [
                    'region_name' => 'Республика Крым',
                    'gibdd_code'  => '82',
                    'okato_code'  => '35',
                    'oktmo_code'  => null,
                ],
                [
                    'region_name' => 'Республика Марий Эл',
                    'gibdd_code'  => '12',
                    'okato_code'  => '88',
                    'oktmo_code'  => null,
                ],
                [
                    'region_name' => 'Республика Мордовия',
                    'gibdd_code'  => '13',
                    'okato_code'  => '89',
                    'oktmo_code'  => null,
                ],
                [
                    'region_name' => 'Республика Мордовия',
                    'gibdd_code'  => '113',
                    'okato_code'  => '89',
                    'oktmo_code'  => null,
                ],
                [
                    'region_name' => 'Республика Саха (Якутия)',
                    'gibdd_code'  => '14',
                    'okato_code'  => '98',
                    'oktmo_code'  => null,
                ],
                [
                    'region_name' => 'Республика Северная Осетия — Алания',
                    'gibdd_code'  => '15',
                    'okato_code'  => '90',
                    'oktmo_code'  => null,
                ],
                [
                    'region_name' => 'Республика Татарстан',
                    'gibdd_code'  => '16',
                    'okato_code'  => '92',
                    'oktmo_code'  => null,
                ],
                [
                    'region_name' => 'Республика Татарстан',
                    'gibdd_code'  => '116',
                    'okato_code'  => '92',
                    'oktmo_code'  => null,
                ],
                [
                    'region_name' => 'Республика Татарстан',
                    'gibdd_code'  => '716',
                    'okato_code'  => '92',
                    'oktmo_code'  => null,
                ],
                [
                    'region_name' => 'Республика Тыва',
                    'gibdd_code'  => '17',
                    'okato_code'  => '93',
                    'oktmo_code'  => null,
                ],
                [
                    'region_name' => 'Удмуртская Республика',
                    'gibdd_code'  => '18',
                    'okato_code'  => '94',
                    'oktmo_code'  => null,
                ],
                [
                    'region_name' => 'Республика Хакасия',
                    'gibdd_code'  => '19',
                    'okato_code'  => '95',
                    'oktmo_code'  => null,
                ],
                [
                    'region_name' => 'Чеченская Республика',
                    'gibdd_code'  => '95',
                    'okato_code'  => '96',
                    'oktmo_code'  => null,
                ],
                [
                    'region_name' => 'Чувашская Республика',
                    'gibdd_code'  => '21',
                    'okato_code'  => '97',
                    'oktmo_code'  => null,
                ],
                [
                    'region_name' => 'Чувашская Республика',
                    'gibdd_code'  => '121',
                    'okato_code'  => '97',
                    'oktmo_code'  => null,
                ],
                [
                    'region_name' => 'Алтайский край',
                    'gibdd_code'  => '22',
                    'okato_code'  => '01',
                    'oktmo_code'  => null,
                ],
                [
                    'region_name' => 'Алтайский край',
                    'gibdd_code'  => '122',
                    'okato_code'  => '01',
                    'oktmo_code'  => null,
                ],
                [
                    'region_name' => 'Забайкальский край',
                    'gibdd_code'  => '75',
                    'okato_code'  => '76',
                    'oktmo_code'  => null,
                ],
                [
                    'region_name' => 'Забайкальский край',
                    'gibdd_code'  => '80',
                    'okato_code'  => '76',
                    'oktmo_code'  => null,
                ],
                [
                    'region_name' => 'Камчатский край',
                    'gibdd_code'  => '41',
                    'okato_code'  => '30',
                    'oktmo_code'  => null,
                ],
                [
                    'region_name' => 'Краснодарский край',
                    'gibdd_code'  => '23',
                    'okato_code'  => '03',
                    'oktmo_code'  => null,
                ],
                [
                    'region_name' => 'Краснодарский край',
                    'gibdd_code'  => '93',
                    'okato_code'  => '03',
                    'oktmo_code'  => null,
                ],
                [
                    'region_name' => 'Краснодарский край',
                    'gibdd_code'  => '123',
                    'okato_code'  => '03',
                    'oktmo_code'  => null,
                ],
                [
                    'region_name' => 'Краснодарский край',
                    'gibdd_code'  => '193',
                    'okato_code'  => '03',
                    'oktmo_code'  => null,
                ],
                [
                    'region_name' => 'Красноярский край',
                    'gibdd_code'  => '24',
                    'okato_code'  => '04',
                    'oktmo_code'  => null,
                ],
                [
                    'region_name' => 'Красноярский край',
                    'gibdd_code'  => '84',
                    'okato_code'  => '04',
                    'oktmo_code'  => null,
                ],
                [
                    'region_name' => 'Красноярский край',
                    'gibdd_code'  => '88',
                    'okato_code'  => '04',
                    'oktmo_code'  => null,
                ],
                [
                    'region_name' => 'Красноярский край',
                    'gibdd_code'  => '124',
                    'okato_code'  => '04',
                    'oktmo_code'  => null,
                ],
                [
                    'region_name' => 'Пермский край',
                    'gibdd_code'  => '59',
                    'okato_code'  => '57',
                    'oktmo_code'  => null,
                ],
                [
                    'region_name' => 'Пермский край',
                    'gibdd_code'  => '81',
                    'okato_code'  => '57',
                    'oktmo_code'  => null,
                ],
                [
                    'region_name' => 'Пермский край',
                    'gibdd_code'  => '159',
                    'okato_code'  => '57',
                    'oktmo_code'  => null,
                ],
                [
                    'region_name' => 'Приморский край',
                    'gibdd_code'  => '25',
                    'okato_code'  => '05',
                    'oktmo_code'  => null,
                ],
                [
                    'region_name' => 'Приморский край',
                    'gibdd_code'  => '125',
                    'okato_code'  => '05',
                    'oktmo_code'  => null,
                ],
                [
                    'region_name' => 'Ставропольский край',
                    'gibdd_code'  => '26',
                    'okato_code'  => '07',
                    'oktmo_code'  => null,
                ],
                [
                    'region_name' => 'Ставропольский край',
                    'gibdd_code'  => '126',
                    'okato_code'  => '07',
                    'oktmo_code'  => null,
                ],
                [
                    'region_name' => 'Хабаровский край',
                    'gibdd_code'  => '27',
                    'okato_code'  => '08',
                    'oktmo_code'  => null,
                ],
                [
                    'region_name' => 'Амурская область',
                    'gibdd_code'  => '28',
                    'okato_code'  => '10',
                    'oktmo_code'  => null,
                ],
                [
                    'region_name' => 'Архангельская область',
                    'gibdd_code'  => '29',
                    'okato_code'  => '11',
                    'oktmo_code'  => null,
                ],
                [
                    'region_name' => 'Астраханская область',
                    'gibdd_code'  => '30',
                    'okato_code'  => '12',
                    'oktmo_code'  => null,
                ],
                [
                    'region_name' => 'Белгородская область',
                    'gibdd_code'  => '31',
                    'okato_code'  => '14',
                    'oktmo_code'  => null,
                ],
                [
                    'region_name' => 'Брянская область',
                    'gibdd_code'  => '32',
                    'okato_code'  => '15',
                    'oktmo_code'  => null,
                ],
                [
                    'region_name' => 'Владимирская область',
                    'gibdd_code'  => '33',
                    'okato_code'  => '17',
                    'oktmo_code'  => null,
                ],
                [
                    'region_name' => 'Волгоградская область',
                    'gibdd_code'  => '34',
                    'okato_code'  => '18',
                    'oktmo_code'  => null,
                ],
                [
                    'region_name' => 'Волгоградская область',
                    'gibdd_code'  => '134',
                    'okato_code'  => '18',
                    'oktmo_code'  => null,
                ],
                [
                    'region_name' => 'Вологодская область',
                    'gibdd_code'  => '35',
                    'okato_code'  => '19',
                    'oktmo_code'  => null,
                ],
                [
                    'region_name' => 'Воронежская область',
                    'gibdd_code'  => '36',
                    'okato_code'  => '20',
                    'oktmo_code'  => null,
                ],
                [
                    'region_name' => 'Воронежская область',
                    'gibdd_code'  => '136',
                    'okato_code'  => '20',
                    'oktmo_code'  => null,
                ],
                [
                    'region_name' => 'Ивановская область',
                    'gibdd_code'  => '37',
                    'okato_code'  => '24',
                    'oktmo_code'  => null,
                ],
                [
                    'region_name' => 'Иркутская область',
                    'gibdd_code'  => '38',
                    'okato_code'  => '25',
                    'oktmo_code'  => null,
                ],
                [
                    'region_name' => 'Иркутская область',
                    'gibdd_code'  => '85',
                    'okato_code'  => '25',
                    'oktmo_code'  => null,
                ],
                [
                    'region_name' => 'Иркутская область',
                    'gibdd_code'  => '138',
                    'okato_code'  => '25',
                    'oktmo_code'  => null,
                ],
                [
                    'region_name' => 'Калининградская область',
                    'gibdd_code'  => '39',
                    'okato_code'  => '27',
                    'oktmo_code'  => null,
                ],
                [
                    'region_name' => 'Калининградская область',
                    'gibdd_code'  => '91',
                    'okato_code'  => '27',
                    'oktmo_code'  => null,
                ],
                [
                    'region_name' => 'Калужская область',
                    'gibdd_code'  => '40',
                    'okato_code'  => '29',
                    'oktmo_code'  => null,
                ],
                [
                    'region_name' => 'Кемеровская область',
                    'gibdd_code'  => '42',
                    'okato_code'  => '32',
                    'oktmo_code'  => null,
                ],
                [
                    'region_name' => 'Кемеровская область',
                    'gibdd_code'  => '142',
                    'okato_code'  => '32',
                    'oktmo_code'  => null,
                ],
                [
                    'region_name' => 'Кировская область',
                    'gibdd_code'  => '43',
                    'okato_code'  => '33',
                    'oktmo_code'  => null,
                ],
                [
                    'region_name' => 'Костромская область',
                    'gibdd_code'  => '44',
                    'okato_code'  => '34',
                    'oktmo_code'  => null,
                ],
                [
                    'region_name' => 'Курганская область',
                    'gibdd_code'  => '45',
                    'okato_code'  => '37',
                    'oktmo_code'  => null,
                ],
                [
                    'region_name' => 'Курская область',
                    'gibdd_code'  => '46',
                    'okato_code'  => '38',
                    'oktmo_code'  => null,
                ],
                [
                    'region_name' => 'Ленинградская область',
                    'gibdd_code'  => '47',
                    'okato_code'  => '41',
                    'oktmo_code'  => null,
                ],
                [
                    'region_name' => 'Ленинградская область',
                    'gibdd_code'  => '147',
                    'okato_code'  => '41',
                    'oktmo_code'  => null,
                ],
                [
                    'region_name' => 'Липецкая область',
                    'gibdd_code'  => '48',
                    'okato_code'  => '42',
                    'oktmo_code'  => null,
                ],
                [
                    'region_name' => 'Магаданская область',
                    'gibdd_code'  => '49',
                    'okato_code'  => '44',
                    'oktmo_code'  => null,
                ],
                [
                    'region_name' => 'Московская область',
                    'gibdd_code'  => '50',
                    'okato_code'  => '46',
                    'oktmo_code'  => null,
                ],
                [
                    'region_name' => 'Московская область',
                    'gibdd_code'  => '90',
                    'okato_code'  => '46',
                    'oktmo_code'  => null,
                ],
                [
                    'region_name' => 'Московская область',
                    'gibdd_code'  => '150',
                    'okato_code'  => '46',
                    'oktmo_code'  => null,
                ],
                [
                    'region_name' => 'Московская область',
                    'gibdd_code'  => '190',
                    'okato_code'  => '46',
                    'oktmo_code'  => null,
                ],
                [
                    'region_name' => 'Московская область',
                    'gibdd_code'  => '750',
                    'okato_code'  => '46',
                    'oktmo_code'  => null,
                ],
                [
                    'region_name' => 'Московская область',
                    'gibdd_code'  => '790',
                    'okato_code'  => '46',
                    'oktmo_code'  => null,
                ],
                [
                    'region_name' => 'Мурманская область',
                    'gibdd_code'  => '51',
                    'okato_code'  => '47',
                    'oktmo_code'  => null,
                ],
                [
                    'region_name' => 'Нижегородская область',
                    'gibdd_code'  => '52',
                    'okato_code'  => '22',
                    'oktmo_code'  => null,
                ],
                [
                    'region_name' => 'Нижегородская область',
                    'gibdd_code'  => '152',
                    'okato_code'  => '22',
                    'oktmo_code'  => null,
                ],
                [
                    'region_name' => 'Новгородская область',
                    'gibdd_code'  => '53',
                    'okato_code'  => '49',
                    'oktmo_code'  => null,
                ],
                [
                    'region_name' => 'Новосибирская область',
                    'gibdd_code'  => '54',
                    'okato_code'  => '50',
                    'oktmo_code'  => null,
                ],
                [
                    'region_name' => 'Новосибирская область',
                    'gibdd_code'  => '154',
                    'okato_code'  => '50',
                    'oktmo_code'  => null,
                ],
                [
                    'region_name' => 'Омская область',
                    'gibdd_code'  => '55',
                    'okato_code'  => '52',
                    'oktmo_code'  => null,
                ],
                [
                    'region_name' => 'Оренбургская область',
                    'gibdd_code'  => '56',
                    'okato_code'  => '53',
                    'oktmo_code'  => null,
                ],
                [
                    'region_name' => 'Оренбургская область',
                    'gibdd_code'  => '156',
                    'okato_code'  => '53',
                    'oktmo_code'  => null,
                ],
                [
                    'region_name' => 'Орловская область',
                    'gibdd_code'  => '57',
                    'okato_code'  => '54',
                    'oktmo_code'  => null,
                ],
                [
                    'region_name' => 'Пензенская область',
                    'gibdd_code'  => '58',
                    'okato_code'  => '56',
                    'oktmo_code'  => null,
                ],
                [
                    'region_name' => 'Псковская область',
                    'gibdd_code'  => '60',
                    'okato_code'  => '58',
                    'oktmo_code'  => null,
                ],
                [
                    'region_name' => 'Ростовская область',
                    'gibdd_code'  => '61',
                    'okato_code'  => '60',
                    'oktmo_code'  => null,
                ],
                [
                    'region_name' => 'Ростовская область',
                    'gibdd_code'  => '161',
                    'okato_code'  => '60',
                    'oktmo_code'  => null,
                ],
                [
                    'region_name' => 'Ростовская область',
                    'gibdd_code'  => '761',
                    'okato_code'  => '60',
                    'oktmo_code'  => null,
                ],
                [
                    'region_name' => 'Рязанская область',
                    'gibdd_code'  => '62',
                    'okato_code'  => '61',
                    'oktmo_code'  => null,
                ],
                [
                    'region_name' => 'Самарская область',
                    'gibdd_code'  => '63',
                    'okato_code'  => '36',
                    'oktmo_code'  => null,
                ],
                [
                    'region_name' => 'Самарская область',
                    'gibdd_code'  => '163',
                    'okato_code'  => '36',
                    'oktmo_code'  => null,
                ],
                [
                    'region_name' => 'Самарская область',
                    'gibdd_code'  => '763',
                    'okato_code'  => '36',
                    'oktmo_code'  => null,
                ],
                [
                    'region_name' => 'Саратовская область',
                    'gibdd_code'  => '64',
                    'okato_code'  => '63',
                    'oktmo_code'  => null,
                ],
                [
                    'region_name' => 'Саратовская область',
                    'gibdd_code'  => '164',
                    'okato_code'  => '63',
                    'oktmo_code'  => null,
                ],
                [
                    'region_name' => 'Сахалинская область',
                    'gibdd_code'  => '65',
                    'okato_code'  => '64',
                    'oktmo_code'  => null,
                ],
                [
                    'region_name' => 'Свердловская область',
                    'gibdd_code'  => '66',
                    'okato_code'  => '65',
                    'oktmo_code'  => null,
                ],
                [
                    'region_name' => 'Свердловская область',
                    'gibdd_code'  => '96',
                    'okato_code'  => '65',
                    'oktmo_code'  => null,
                ],
                [
                    'region_name' => 'Свердловская область',
                    'gibdd_code'  => '196',
                    'okato_code'  => '65',
                    'oktmo_code'  => null,
                ],
                [
                    'region_name' => 'Смоленская область',
                    'gibdd_code'  => '67',
                    'okato_code'  => '66',
                    'oktmo_code'  => null,
                ],
                [
                    'region_name' => 'Тамбовская область',
                    'gibdd_code'  => '68',
                    'okato_code'  => '68',
                    'oktmo_code'  => null,
                ],
                [
                    'region_name' => 'Тверская область',
                    'gibdd_code'  => '69',
                    'okato_code'  => '28',
                    'oktmo_code'  => null,
                ],
                [
                    'region_name' => 'Томская область',
                    'gibdd_code'  => '70',
                    'okato_code'  => '69',
                    'oktmo_code'  => null,
                ],
                [
                    'region_name' => 'Тульская область',
                    'gibdd_code'  => '71',
                    'okato_code'  => '70',
                    'oktmo_code'  => null,
                ],
                [
                    'region_name' => 'Тюменская область',
                    'gibdd_code'  => '72',
                    'okato_code'  => '71',
                    'oktmo_code'  => null,
                ],
                [
                    'region_name' => 'Ульяновская область',
                    'gibdd_code'  => '73',
                    'okato_code'  => '73',
                    'oktmo_code'  => null,
                ],
                [
                    'region_name' => 'Ульяновская область',
                    'gibdd_code'  => '173',
                    'okato_code'  => '73',
                    'oktmo_code'  => null,
                ],
                [
                    'region_name' => 'Челябинская область',
                    'gibdd_code'  => '74',
                    'okato_code'  => '75',
                    'oktmo_code'  => null,
                ],
                [
                    'region_name' => 'Челябинская область',
                    'gibdd_code'  => '174',
                    'okato_code'  => '75',
                    'oktmo_code'  => null,
                ],
                [
                    'region_name' => 'Челябинская область',
                    'gibdd_code'  => '774',
                    'okato_code'  => '75',
                    'oktmo_code'  => null,
                ],
                [
                    'region_name' => 'Ярославская область',
                    'gibdd_code'  => '76',
                    'okato_code'  => '78',
                    'oktmo_code'  => null,
                ],
                [
                    'region_name' => 'Москва',
                    'gibdd_code'  => '77',
                    'okato_code'  => '45',
                    'oktmo_code'  => null,
                ],
                [
                    'region_name' => 'Москва',
                    'gibdd_code'  => '97',
                    'okato_code'  => '45',
                    'oktmo_code'  => null,
                ],
                [
                    'region_name' => 'Москва',
                    'gibdd_code'  => '99',
                    'okato_code'  => '45',
                    'oktmo_code'  => null,
                ],
                [
                    'region_name' => 'Москва',
                    'gibdd_code'  => '177',
                    'okato_code'  => '45',
                    'oktmo_code'  => null,
                ],
                [
                    'region_name' => 'Москва',
                    'gibdd_code'  => '197',
                    'okato_code'  => '45',
                    'oktmo_code'  => null,
                ],
                [
                    'region_name' => 'Москва',
                    'gibdd_code'  => '199',
                    'okato_code'  => '45',
                    'oktmo_code'  => null,
                ],
                [
                    'region_name' => 'Москва',
                    'gibdd_code'  => '777',
                    'okato_code'  => '45',
                    'oktmo_code'  => null,
                ],
                [
                    'region_name' => 'Москва',
                    'gibdd_code'  => '797',
                    'okato_code'  => '45',
                    'oktmo_code'  => null,
                ],
                [
                    'region_name' => 'Москва',
                    'gibdd_code'  => '799',
                    'okato_code'  => '45',
                    'oktmo_code'  => null,
                ],
                [
                    'region_name' => 'Санкт-Петербург',
                    'gibdd_code'  => '78',
                    'okato_code'  => '40',
                    'oktmo_code'  => null,
                ],
                [
                    'region_name' => 'Санкт-Петербург',
                    'gibdd_code'  => '98',
                    'okato_code'  => '40',
                    'oktmo_code'  => null,
                ],
                [
                    'region_name' => 'Санкт-Петербург',
                    'gibdd_code'  => '178',
                    'okato_code'  => '40',
                    'oktmo_code'  => null,
                ],
                [
                    'region_name' => 'Санкт-Петербург',
                    'gibdd_code'  => '198',
                    'okato_code'  => '40',
                    'oktmo_code'  => null,
                ],
                [
                    'region_name' => 'Севастополь',
                    'gibdd_code'  => '92',
                    'okato_code'  => '67',
                    'oktmo_code'  => null,
                ],
                [
                    'region_name' => 'Еврейская автономная область',
                    'gibdd_code'  => '79',
                    'okato_code'  => '99',
                    'oktmo_code'  => null,
                ],
                [
                    'region_name' => 'Ненецкий автономный округ',
                    'gibdd_code'  => '83',
                    'okato_code'  => '111',
                    'oktmo_code'  => '118',
                ],
                [
                    'region_name' => 'Ханты-Мансийский автономный округ - Югра',
                    'gibdd_code'  => '86',
                    'okato_code'  => '71100',
                    'oktmo_code'  => '718',
                ],
                [
                    'region_name' => 'Ханты-Мансийский автономный округ - Югра',
                    'gibdd_code'  => '186',
                    'okato_code'  => '71100',
                    'oktmo_code'  => '718',
                ],
                [
                    'region_name' => 'Чукотский автономный округ',
                    'gibdd_code'  => '87',
                    'okato_code'  => '77',
                    'oktmo_code'  => null,
                ],
                [
                    'region_name' => 'Ямало-Ненецкий автономный округ',
                    'gibdd_code'  => '89',
                    'okato_code'  => '71140',
                    'oktmo_code'  => '719',
                ],
                [
                    'region_name' => 'Территории, находящиеся за пределами РФ и обслуживаемые Управлением режимных объектов МВД России, Байконур',
                    'gibdd_code'  => '94',
                    'okato_code'  => '55',
                    'oktmo_code'  => null,
                ],
            ]
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ru_region_codes');
    }
}
