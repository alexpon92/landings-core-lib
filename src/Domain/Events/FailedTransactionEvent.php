<?php
declare(strict_types=1);

namespace LandingsCore\Domain\Events;

use Carbon\Carbon;

class FailedTransactionEvent
{
    /**
     * @var string
     */
    private $phone;

    /**
     * @var string
     */
    private $amount;

    /**
     * @var string
     */
    private $currency;

    /**
     * @var string
     */
    private $vendorId;

    /**
     * @var array
     */
    private $data;

    /**
     * @var string
     */
    private $type;

    /**
     * @var string
     */
    private $failReason;

    /**
     * @var string|null
     */
    private $cardFirstSix;

    /**
     * @var string|null
     */
    private $cardLastFour;

    /**
     * @var string
     */
    private $paymentSystem;

    /**
     * @var Carbon
     */
    private $processedAt;

    /**
     * FailedTransactionEvent constructor.
     *
     * @param string      $phone
     * @param string      $amount
     * @param string      $currency
     * @param string      $vendorId
     * @param array       $data
     * @param string      $type
     * @param string      $failReason
     * @param string|null $cardFirstSix
     * @param string|null $cardLastFour
     * @param string      $paymentSystem
     * @param Carbon      $processedAt
     */
    public function __construct(
        string $phone,
        string $amount,
        string $currency,
        string $vendorId,
        array $data,
        string $type,
        string $failReason,
        ?string $cardFirstSix,
        ?string $cardLastFour,
        string $paymentSystem,
        Carbon $processedAt
    ) {
        $this->phone         = $phone;
        $this->amount        = $amount;
        $this->currency      = $currency;
        $this->vendorId      = $vendorId;
        $this->data          = $data;
        $this->type          = $type;
        $this->failReason    = $failReason;
        $this->cardFirstSix  = $cardFirstSix;
        $this->cardLastFour  = $cardLastFour;
        $this->paymentSystem = $paymentSystem;
        $this->processedAt   = $processedAt;
    }

    /**
     * @return string
     */
    public function getPhone(): string
    {
        return $this->phone;
    }

    /**
     * @return string
     */
    public function getAmount(): string
    {
        return $this->amount;
    }

    /**
     * @return string
     */
    public function getCurrency(): string
    {
        return $this->currency;
    }

    /**
     * @return string
     */
    public function getVendorId(): string
    {
        return $this->vendorId;
    }

    /**
     * @return array
     */
    public function getData(): array
    {
        return $this->data;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @return string
     */
    public function getFailReason(): string
    {
        return $this->failReason;
    }

    /**
     * @return string|null
     */
    public function getCardFirstSix(): ?string
    {
        return $this->cardFirstSix;
    }

    /**
     * @return string|null
     */
    public function getCardLastFour(): ?string
    {
        return $this->cardLastFour;
    }

    /**
     * @return string
     */
    public function getPaymentSystem(): string
    {
        return $this->paymentSystem;
    }

    /**
     * @return Carbon
     */
    public function getProcessedAt(): Carbon
    {
        return $this->processedAt;
    }
}