<?php
declare(strict_types=1);

namespace LandingsCore\Domain\Services\OfferVendorsDuplicatesCheck\CheckServices\Exceptions;

class MalformedResponseFormatException extends \Exception
{

}