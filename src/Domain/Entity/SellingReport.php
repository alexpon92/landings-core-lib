<?php

declare(strict_types=1);

namespace LandingsCore\Domain\Entity;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * LandingsCore\Domain\Entity\SellingReport
 *
 * @property int         $id
 * @property int         $external_id
 * @property int         $loaner_id
 * @property int         $personal_data_id
 * @property string      $loaner_selling_service_ids
 * @property string      $type
 * @property null|float  $transaction_amount
 * @property null|string $transaction_currency
 * @property null|float  $transaction_local_amount
 * @property null|string $transaction_local_currency
 * @property Carbon      $triggered_at
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @mixin \Eloquent
 */
class SellingReport extends Model
{
    public const REGISTRATION = 'registration';
    public const WRITE_OFF    = 'write_off';
    public const CARD_LINK    = 'card_link';

    /**
     * @return int|null
     */
    public static function getLastExternalId(): ?int
    {
        /** @var SellingReport $report */
        $report = self::orderBy('external_id', 'desc')->first();

        return $report->external_id ?? null;
    }

    /**
     * @param int $loanerId
     *
     * @return array
     */
    public static function getByLoanerId(int $loanerId): array
    {
        return self::where('loaner_id', $loanerId)
            ->orderBy('external_id', 'desc')
            ->get()
            ->all();
    }

    public function getLoanerSellingServiceIds(): array
    {
        return json_decode($this->loaner_selling_service_ids, true);
    }

}
