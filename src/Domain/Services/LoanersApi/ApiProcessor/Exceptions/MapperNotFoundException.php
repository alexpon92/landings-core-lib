<?php
declare(strict_types=1);

namespace LandingsCore\Domain\Services\LoanersApi\ApiProcessor\Exceptions;

class MapperNotFoundException extends \Exception
{

}