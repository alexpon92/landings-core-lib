<?php
declare(strict_types=1);

namespace LandingsCore\Listeners;

use LandingsCore\Domain\CorePackage\CoreClient\ICoreClient;
use LandingsCore\Domain\Events\FailedTransactionEvent;
use Psr\Log\LoggerInterface;

class FailedTransactionsSubscriber extends AbstractListener
{
    /**
     * @var ICoreClient
     */
    private $coreClient;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * FailedTransactionsSubscriber constructor.
     *
     * @param ICoreClient     $coreClient
     * @param LoggerInterface $logger
     */
    public function __construct(ICoreClient $coreClient, LoggerInterface $logger)
    {
        $this->coreClient = $coreClient;
        $this->logger     = $logger;
    }

    public function onFailedTransaction(FailedTransactionEvent $event): void
    {
        $this->coreClient->storeFailedTransaction(
            $event->getPhone(),
            $event->getAmount(),
            $event->getCurrency(),
            $event->getVendorId(),
            $event->getType(),
            $event->getFailReason(),
            $event->getCardFirstSix(),
            $event->getCardLastFour(),
            $event->getPaymentSystem(),
            $event->getData(),
            $event->getProcessedAt()
        );
    }

    public function getConnection(): string
    {
        return (string)config('landings-core-lib.failed_transactions_queue.connection');
    }

    public function getQueue(): string
    {
        return (string)config('landings-core-lib.failed_transactions_queue.name');
    }

    /**
     * Register the listeners for the subscriber.
     *
     * @param \Illuminate\Events\Dispatcher $events
     */
    public function subscribe($events)
    {
        $events->listen(
            FailedTransactionEvent::class,
            self::class . '@onFailedTransaction'
        );

    }
}