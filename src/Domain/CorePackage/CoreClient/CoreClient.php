<?php

declare(strict_types=1);

namespace LandingsCore\Domain\CorePackage\CoreClient;

use Carbon\Carbon;
use GuzzleHttp\Client;
use GuzzleHttp\ClientInterface;
use Illuminate\Http\Response;
use LandingsCore\Domain\CorePackage\CoreClient\ClientExceptions\BadResponseCode;
use LandingsCore\Domain\CorePackage\CoreClient\ClientExceptions\BadResponseFormat;
use LandingsCore\Domain\CorePackage\CoreClient\SellingReports\SellingReportDto;
use LandingsCore\Domain\CorePackage\CoreClient\SellingServices\SellingServiceDto;
use LandingsCore\Domain\CorePackage\CoreClient\StoreLoaner\LoanerDto as LoanerResponse;
use LandingsCore\Domain\CorePackage\CoreClient\StoreLoaner\PersonalDataDto;
use Psr\Log\LoggerInterface;

class CoreClient implements ICoreClient
{
    /**
     * @var string
     */
    private $landingName;

    /**
     * @var string
     */
    private $authToken;

    /**
     * @var string
     */
    private $baseUri;

    /**
     * @var ClientInterface
     */
    private $httpClient;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * CoreClient constructor.
     *
     * @param string          $landingName
     * @param string          $authToken
     * @param string          $baseUri
     * @param ClientInterface $httpClient
     * @param LoggerInterface $logger
     */
    public function __construct(
        string $landingName,
        string $authToken,
        string $baseUri,
        ClientInterface $httpClient,
        LoggerInterface $logger
    ) {
        $config     = $httpClient->getConfig();
        $config     = array_merge(
            $config,
            [
                'base_uri'    => $baseUri,
                'http_errors' => false,
                'headers'     => [
                    'Content-type' => 'application/json',
                    'Accept'       => 'application/json',
                    'Landing'      => $landingName,
                    'Auth-Token'   => $authToken,
                ],
            ]
        );
        $httpClient = new Client($config);

        $this->landingName = $landingName;
        $this->authToken   = $authToken;
        $this->baseUri     = $baseUri;
        $this->httpClient  = $httpClient;
        $this->logger      = $logger;
    }

    public function getLeaveReasons(): array
    {
        $response = $this->httpClient->get('api/landings/leave-reasons');

        if ($response->getStatusCode() !== Response::HTTP_OK) {
            $this->logger->error(
                'Bad http code while getting leave reasons',
                [
                    'code'    => $response->getStatusCode(),
                    'content' => $response->getBody()->getContents(),
                ]
            );
            throw new BadResponseCode("Bad http code while getting leave reasons {$response->getStatusCode()}");
        }

        $content = $response->getBody()->getContents();
        $data    = json_decode($content, true);
        if (!$data || !is_array($data) || !isset($data['leave_reasons'])) {
            $this->logger->error(
                'Bad response format while getting leave reasons',
                [
                    'content' => $content,
                ]
            );
            throw new BadResponseFormat('Bad response format while getting leave reasons');
        }

        $result = [];
        foreach ($data['leave_reasons'] as $reason) {
            if (!isset($reason['id'], $reason['texts'], $reason['url_pattern'])) {
                $this->logger->error(
                    'Bad response format while getting leave reasons',
                    [
                        'content' => $content,
                    ]
                );
                throw new BadResponseFormat('Bad response format while getting leave reasons');
            }

            $result[] = new LeaveReasonDto(
                $reason['id'],
                $reason['texts'],
                $reason['url_pattern']
            );
        }

        return $result;
    }

    public function getOffers(): array
    {
        $response = $this->httpClient->get('api/landings/offers');

        if ($response->getStatusCode() !== Response::HTTP_OK) {
            $this->logger->error(
                'Bad http code while getting offers',
                [
                    'code'    => $response->getStatusCode(),
                    'content' => $response->getBody()->getContents(),
                ]
            );
            throw new BadResponseCode("Bad http code while getting offers {$response->getStatusCode()}");
        }

        $content = $response->getBody()->getContents();
        $data    = json_decode($content, true);
        if (!$data || !is_array($data) || !isset($data['offers'])) {
            $this->logger->error(
                'Bad response format while getting offers',
                [
                    'content' => $content,
                ]
            );
            throw new BadResponseFormat('Bad response format while getting offers');
        }

        $result = [];
        foreach ($data['offers'] as $offer) {
            if (!$this->validateOffer($offer)) {
                $this->logger->error(
                    'Bad response offer format while getting offers',
                    [
                        'content'  => $content,
                        'offer_id' => $offer['id'] ?? null,
                    ]
                );
                throw new BadResponseFormat('Bad response format while getting offers');
            }

            $result[] = new OfferDto(
                (int) $offer['id'],
                $offer['name'],
                $offer['img_content'],
                $offer['type'],
                (int) $offer['amount'],
                (int) $offer['weight'],
                $offer['duration'],
                $offer['credit_history'],
                $offer['age_from'],
                $offer['age_to'],
                $offer['landing_name'],
                (string) $offer['probability'],
                $offer['percent'],
                $offer['card_state'],
                (bool) $offer['is_in_top'],
                (bool) $offer['is_in_popup'],
                $offer['click_rules'],
                $offer['additional_info'],
                (float) $offer['epc'],
                $offer['settings']
            );
        }

        return $result;
    }

    private function validateOffer(array $offer): bool
    {
        if (
        !isset(
            $offer['id'],
            $offer['name'],
            $offer['img_content'],
            $offer['type'],
            $offer['amount'],
            $offer['weight'],
            $offer['is_in_top'],
            $offer['is_in_popup'],
            $offer['percent'],
            $offer['card_state'],
            $offer['landing_name'],
            $offer['click_rules']
        )
        ) {
            return false;
        }
        if (
            !array_key_exists('age_from', $offer)
            && !array_key_exists('age_to', $offer)
            && !array_key_exists('probability', $offer)
            && !array_key_exists('duration', $offer)
            && !array_key_exists('credit_history', $offer)
        ) {
            return false;
        }

        return true;
    }

    public function getOffersProfitability(): array
    {
        $response = $this->httpClient->get('api/landings/offers-profitability');

        if ($response->getStatusCode() !== Response::HTTP_OK) {
            $this->logger->error(
                'Bad http code while getting offers profitability',
                [
                    'code'    => $response->getStatusCode(),
                    'content' => $response->getBody()->getContents(),
                ]
            );
            throw new BadResponseCode("Bad http code while getting offers profitability {$response->getStatusCode()}");
        }

        $content = $response->getBody()->getContents();
        $data    = json_decode($content, true);

        if (!$data || !is_array($data) || !isset($data['profitability'])) {
            $this->logger->error(
                'Bad response format while getting offers profitability',
                [
                    'content' => $content,
                ]
            );
            throw new BadResponseFormat('Bad response format while getting offers profitability');
        }

        $result = [];
        foreach ($data['profitability'] as $offerProfitability) {
            if (
            !isset(
                $offerProfitability['id'],
                $offerProfitability['landing_name'],
                $offerProfitability['traffic_source_id'],
                $offerProfitability['traffic_source_name'],
                $offerProfitability['traffic_web_id'],
                $offerProfitability['traffic_web_web_id'],
                $offerProfitability['offer_id'],
                $offerProfitability['epc']
            )
            ) {
                $this->logger->error(
                    'Bad response offer format while getting offers profitability',
                    [
                        'content'                => $content,
                        'offer_profitability_id' => $offerProfitability['id'] ?? null,
                    ]
                );
                throw new BadResponseFormat('Bad response format while getting offers');
            }

            $result[] = new OfferProfitabilityDataDto(
                (int) $offerProfitability['id'],
                $offerProfitability['landing_name'],
                (int) $offerProfitability['traffic_source_id'],
                $offerProfitability['traffic_source_name'],
                (int) $offerProfitability['traffic_web_id'],
                $offerProfitability['traffic_web_web_id'],
                (int) $offerProfitability['offer_id'],
                (float) $offerProfitability['epc']
            );
        }

        return $result;
    }

    public function storeLoaner(LoanerDto $loanerDto): ?LoanerResponse
    {
        $response = $this->httpClient->post(
            'api/landings/loaner',
            [
                'body'    => json_encode($loanerDto),
                'headers' => [
                    'api-version' => 'v1.1',
                ],
            ]
        );

        if ($response->getStatusCode() === Response::HTTP_NOT_ACCEPTABLE) {
            $this->logger->warning(
                'Loaner is locked for update',
                [
                    'code'        => $response->getStatusCode(),
                    'content'     => $response->getBody()->getContents(),
                    'loaner_data' => $loanerDto,
                ]
            );

            return null;
        }

        if ($response->getStatusCode() !== Response::HTTP_OK) {
            $this->logger->error(
                'Bad http code while storing loaner',
                [
                    'code'        => $response->getStatusCode(),
                    'content'     => $response->getBody()->getContents(),
                    'loaner_data' => $loanerDto,
                ]
            );
            throw new BadResponseCode("Bad http code while storing loaner {$response->getStatusCode()}");
        }

        $this->logger->debug('Loaner successfully saved', ['id' => $loanerDto->getId()]);

        $data = json_decode($response->getBody()->getContents(), true);

        return new LoanerResponse(
            (int) $data['loaner']['id'],
            new PersonalDataDto(
                (int) $data['loaner']['personal_data']['id'],
                (string) $data['loaner']['personal_data']['hash']
            )
        );
    }

    public function getSignedOffLoaners(?int $limit = 500): array
    {
        $response = $this->httpClient->get(
            'api/landings/signed-off-loaners',
            [
                'query' => [
                    'limit' => $limit,
                ],
            ]
        );

        if ($response->getStatusCode() !== Response::HTTP_OK) {
            $this->logger->error(
                'Bad http code while getting signed off loaners',
                [
                    'code'    => $response->getStatusCode(),
                    'content' => $response->getBody()->getContents(),
                ]
            );
            throw new BadResponseCode("Bad http code while getting signed off loaners {$response->getStatusCode()}");
        }

        $content = $response->getBody()->getContents();
        $data    = json_decode($content, true);
        if (!$data || !is_array($data) || !isset($data['phones']) || !is_array($data['phones'])) {
            $this->logger->error(
                'Bad response format while getting signed off loaners',
                [
                    'content' => $content,
                ]
            );
            throw new BadResponseFormat('Bad response format while getting signed off loaners');
        }

        return $data['phones'];
    }

    public function signOffLoaner(string $phone, string $type): void
    {
        $response = $this->httpClient->post(
            'api/landings/loaner/sign-off',
            [
                'body' => json_encode(['phone' => $phone, 'type' => $type]),
            ]
        );

        if ($response->getStatusCode() !== Response::HTTP_OK) {
            $this->logger->error(
                'Bad http code while sign off loaner',
                [
                    'phone'   => $phone,
                    'code'    => $response->getStatusCode(),
                    'content' => $response->getBody()->getContents(),
                ]
            );
            throw new BadResponseCode("Bad http code while sign off loaner {$response->getStatusCode()}");
        }
    }

    public function validateEmail(string $email): bool
    {
        $response = $this->httpClient->get(
            'api/landings/check-email',
            [
                'query' => ['email' => $email],
            ]
        );

        switch ($response->getStatusCode()) {
            case Response::HTTP_ACCEPTED:
                return true;
            case Response::HTTP_UNPROCESSABLE_ENTITY:
                return false;
        }

        $this->logger->error(
            'Bad http code while validating loaner email',
            [
                'email'   => $email,
                'code'    => $response->getStatusCode(),
                'content' => $response->getBody()->getContents(),
            ]
        );

        throw new BadResponseCode("Bad http code while validating loaner {$response->getStatusCode()}");
    }

    public function storeTransaction(
        string $phone,
        string $vendorId,
        string $token,
        string $amount,
        string $commissionAmount,
        string $currency,
        string $paymentSystem,
        ?array $data,
        Carbon $dateTime
    ): void {
        $response = $this->httpClient->post(
            'api/landings/transaction',
            [
                'body' => json_encode(
                    [
                        'phone'             => $phone,
                        'token'             => $token,
                        'amount'            => $amount,
                        'commission_amount' => $commissionAmount,
                        'currency'          => $currency,
                        'vendor_id'         => $vendorId,
                        'payment_system'    => $paymentSystem,
                        'data'              => $data ?? [],
                        'processed_at'      => $dateTime->format('Y-m-d H:i:sO'),
                    ]
                ),
            ]
        );

        if ($response->getStatusCode() !== Response::HTTP_OK) {
            $this->logger->error(
                'Bad http code while store transaction',
                [
                    'phone'   => $phone,
                    'code'    => $response->getStatusCode(),
                    'content' => $response->getBody()->getContents(),
                ]
            );
            throw new BadResponseCode("Bad http code while store transaction loaner {$response->getStatusCode()}");
        }
    }

    public function storeFailedTransaction(
        string $phone,
        string $amount,
        string $currency,
        string $vendorId,
        string $type,
        string $failReason,
        ?string $cardFirstSix,
        ?string $cardLastFour,
        string $paymentSystem,
        ?array $data,
        Carbon $dateTime
    ): void {
        $response = $this->httpClient->post(
            'api/landings/failed-transaction',
            [
                'body' => json_encode(
                    [
                        'phone'          => $phone,
                        'amount'         => $amount,
                        'currency'       => $currency,
                        'vendor_id'      => $vendorId,
                        'data'           => $data,
                        'type'           => $type,
                        'fail_reason'    => $failReason,
                        'card_first_six' => $cardFirstSix,
                        'card_last_four' => $cardLastFour,
                        'payment_system' => $paymentSystem,
                        'processed_at'   => $dateTime->format('Y-m-d H:i:sO'),
                    ]
                ),
            ]
        );

        if ($response->getStatusCode() !== Response::HTTP_OK) {
            $this->logger->error(
                'Bad http code while store failed transaction',
                [
                    'phone'   => $phone,
                    'code'    => $response->getStatusCode(),
                    'content' => $response->getBody()->getContents(),
                ]
            );
            throw new BadResponseCode("Bad http code while store failed transaction {$response->getStatusCode()}");
        }
    }


    public function storeTransition(
        string $sessionId,
        string $utmSource,
        ?string $utmMedium,
        ?string $utmCampaign,
        ?string $landingUri,
        ?string $referer,
        ?string $ipAddress,
        ?array $data,
        Carbon $visitedAt
    ): void {
        $response = $this->httpClient->post(
            'api/landings/landing-transitions',
            [
                'body' => json_encode(
                    [
                        'session_id'   => $sessionId,
                        'utm_source'   => $utmSource,
                        'utm_medium'   => $utmMedium,
                        'utm_campaign' => $utmCampaign,
                        'landing_uri'  => $landingUri,
                        'referer'      => $referer,
                        'ip_address'   => $ipAddress,
                        'data'         => $data,
                        'visited_at'   => $visitedAt->format('Y-m-d H:i:sO'),
                    ]
                ),
            ]
        );

        if ($response->getStatusCode() !== Response::HTTP_OK) {
            $this->logger->error(
                'Bad http code while store transition',
                [
                    'session_id' => $sessionId,
                    'code'       => $response->getStatusCode(),
                    'content'    => $response->getBody()->getContents(),
                ]
            );
            throw new BadResponseCode("Bad http code while store transition {$response->getStatusCode()}");
        }
    }

    public function getLandingSettings(): array
    {
        $response = $this->httpClient->get('api/landings/showcase-settings');

        if ($response->getStatusCode() !== Response::HTTP_OK) {
            $this->logger->error(
                'Bad http code while getting showcase settings',
                [
                    'code'    => $response->getStatusCode(),
                    'content' => $response->getBody()->getContents(),
                ]
            );
            throw new BadResponseCode("Bad http code while getting showcase settings {$response->getStatusCode()}");
        }

        $content = $response->getBody()->getContents();
        $data    = json_decode($content, true);
        if (!$data || !is_array($data) || !$this->validateLandingSettings($data)) {
            $this->logger->error(
                'Bad response format while getting showcase settings',
                [
                    'content' => $content,
                ]
            );
            throw new BadResponseFormat('Bad response format while getting showcase settings');
        }

        return $data;
    }

    public function signOffLoanerSmsSubscription(string $phone): void
    {
        $response = $this->httpClient->post(
            'api/landings/loaner/sign-off-sms',
            [
                'body' => json_encode(
                    [
                        'phone' => $phone,
                    ]
                ),
            ]
        );

        if ($response->getStatusCode() === Response::HTTP_NOT_FOUND) {
            $this->logger->info(
                'Loaner not found in sign off sms',
                [
                    'phone'   => $phone,
                    'code'    => $response->getStatusCode(),
                    'content' => $response->getBody()->getContents(),
                ]
            );

            return;
        }

        if ($response->getStatusCode() !== Response::HTTP_OK) {
            $this->logger->error(
                'Bad http code while sign off loaner sms subscription',
                [
                    'phone'   => $phone,
                    'code'    => $response->getStatusCode(),
                    'content' => $response->getBody()->getContents(),
                ]
            );
            throw new BadResponseCode(
                "Bad http code while sign off loaner sms subscription {$response->getStatusCode()}"
            );
        }
    }

    private function validateLandingSettings(array $settings): bool
    {
        $version  = $settings['version'] ?? null;
        $template = $settings['template'];

        if (null === $version || null === $template) {
            return false;
        }

        switch ($version) {
            case 'v1':
                if (
                !isset(
                    $template['link'],
                    $template['phone'],
                    $template['address'],
                    $template['license'],
                    $template['seo_text'],
                    $template['agreement'],
                    $template['content_blocks'],
                    $template['block_with_time']
                )
                ) {
                    return false;
                }
                break;
        }

        return true;
    }

    public function getTrafficSources(): array
    {
        $response = $this->httpClient->get(
            'api/landings/traffic-sources'
        );

        if ($response->getStatusCode() !== Response::HTTP_OK) {
            $this->logger->error(
                'Bad http code while getting traffic sources',
                [
                    'code'    => $response->getStatusCode(),
                    'content' => $response->getBody()->getContents(),
                ]
            );
            throw new BadResponseCode("Bad http code while getting traffic sources {$response->getStatusCode()}");
        }

        $content = $response->getBody()->getContents();
        $data    = json_decode($content, true);

        if (!isset($data['traffic_sources']) || !is_array($data['traffic_sources'])) {
            $this->logger->error(
                'Bad response format while getting traffic sources',
                [
                    'content' => $content,
                ]
            );
            throw new BadResponseFormat('Bad response format while getting traffic sources');
        }

        $res = [];
        foreach ($data['traffic_sources'] as $trafficSource) {
            $res[] = new TrafficSourceDto(
                $trafficSource['id'],
                $trafficSource['name'],
                $trafficSource['description'],
                $trafficSource['type'],
                $trafficSource['access_type'],
                Carbon::createFromFormat('Y-m-d H:i:s', $trafficSource['created_at'])
            );
        }

        return $res;
    }

    public function getLoanersFeed(?int $lastId): array
    {
        $response = $this->httpClient->get(
            'api/landings/loaners-feed',
            [
                'query' => [
                    'last_id' => $lastId ?? 0,
                ],
            ]
        );

        if ($response->getStatusCode() !== Response::HTTP_OK) {
            $this->logger->error(
                'Bad http code while getting loaners feed',
                [
                    'code'    => $response->getStatusCode(),
                    'content' => $response->getBody()->getContents(),
                ]
            );
            throw new BadResponseCode("Bad http code while getting loaners feed {$response->getStatusCode()}");
        }

        $content = $response->getBody()->getContents();
        $data    = json_decode($content, true);

        if (!isset($data['feed']) || !is_array($data['feed'])) {
            $this->logger->error(
                'Bad response format while getting loaners feed',
                [
                    'content' => $content,
                ]
            );
            throw new BadResponseFormat('Bad response format while getting loaners feed');
        }

        $res = [];
        foreach ($data['feed'] as $feedItem) {
            $res[] = new FeedDto(
                $feedItem['id'],
                $feedItem['type'],
                $feedItem['payload'],
                Carbon::createFromFormat('Y-m-d H:i:s', $feedItem['triggered_at']),
                $feedItem['loaner_id'],
                $feedItem['phone']
            );
        }

        return $res;
    }

    public function trackOfferClick(
        int $offerId,
        Carbon $triggeredAt,
        ?string $sessionId = null,
        ?string $utmSource = null,
        ?string $utmCampaign = null,
        ?string $utmMedium = null
    ): void {
        $response = $this->httpClient->post(
            'api/landings/track-offer',
            [
                'body' => json_encode(
                    [
                        'offer_id'     => $offerId,
                        'triggered_at' => $triggeredAt->format('Y-m-d H:i:sO'),
                        'external_id'  => $sessionId,
                        'utm_source'   => $utmSource,
                        'utm_campaign' => $utmCampaign,
                        'utm_medium'   => $utmMedium,
                    ]
                ),
            ]
        );

        if ($response->getStatusCode() !== Response::HTTP_NO_CONTENT) {
            $this->logger->error(
                'Bad http code while tracking offer click',
                [
                    'code'    => $response->getStatusCode(),
                    'content' => $response->getBody()->getContents(),
                    'id'      => $offerId,
                ]
            );

            throw new BadResponseCode("Bad http code while storing offer click {$response->getStatusCode()}");
        }
    }

    public function getLoanerSellingServices(): array
    {
        $response = $this->httpClient->get(
            'api/landings/loaner-selling-services'
        );

        if ($response->getStatusCode() !== Response::HTTP_OK) {
            $this->logger->error(
                'Bad http code while getting loaners selling services',
                [
                    'code'    => $response->getStatusCode(),
                    'content' => $response->getBody()->getContents(),
                ]
            );
            throw new BadResponseCode(
                "Bad http code while getting loaners selling services {$response->getStatusCode()}"
            );
        }

        $content = $response->getBody()->getContents();
        $data    = json_decode($content, true);

        if (!isset($data['loaner_selling_services']) || !is_array($data['loaner_selling_services'])) {
            $this->logger->error(
                'Bad response format while getting loaners selling services',
                [
                    'content' => $content,
                ]
            );
            throw new BadResponseFormat('Bad response format while getting loaners selling services');
        }

        $result = [];
        foreach ($data['loaner_selling_services'] as $service) {
            $result[] = new SellingServiceDto(
                $service['id'],
                $service['type'],
                $service['name'] ?? null,
                $service['description'] ?? null,
                $service['logo'] ?? null
            );
        }

        return $result;
    }

    public function getSellingReports(?int $lastId): array
    {
        $options = null === $lastId
            ? []
            : [
                'query' => [
                    'last_id' => $lastId,
                ],
            ];

        $response = $this->httpClient->get(
            'api/landings/selling-reports',
            $options
        );

        if ($response->getStatusCode() !== Response::HTTP_OK) {
            $this->logger->error(
                'Bad http code while getting selling reports',
                [
                    'code'    => $response->getStatusCode(),
                    'content' => $response->getBody()->getContents(),
                ]
            );
            throw new BadResponseCode("Bad http code while getting selling reports {$response->getStatusCode()}");
        }

        $content = $response->getBody()->getContents();
        $data    = json_decode($content, true);

        if (!isset($data['selling_reports']) || !is_array($data['selling_reports'])) {
            $this->logger->error(
                'Bad response format while getting  selling reports',
                [
                    'content' => $content,
                ]
            );
            throw new BadResponseFormat('Bad response format while getting selling reports');
        }

        $result = [];
        foreach ($data['selling_reports'] as $report) {
            $result[] = new SellingReportDto(
                $report['id'],
                $report['loaner_id'],
                $report['personal_data_id'],
                $report['loaner_selling_service_ids'],
                $report['type'],
                $report['transaction_amount'] ?? null,
                $report['transaction_currency'] ?? null,
                $report['transaction_local_amount'] ?? null,
                $report['transaction_local_currency'] ?? null,
                Carbon::parse($report['created_at'])
            );
        }

        return $result;
    }

    public function checkIp(string $ip): IPCheckResponseDto
    {
        $response = $this->httpClient->get(
            'api/landings/check-ip',
            [
                'query' => [
                    'address' => $ip,
                ],
            ]
        );

        if ($response->getStatusCode() !== Response::HTTP_OK) {
            $this->logger->error(
                'Bad http code while checking ip',
                [
                    'code'    => $response->getStatusCode(),
                    'content' => $response->getBody()->getContents(),
                ]
            );
            throw new BadResponseCode("Bad http code while checking ip {$response->getStatusCode()}");
        }

        $content = $response->getBody()->getContents();
        $data    = json_decode($content, true);
        if (
            !isset(
                $data['ip_address'],
                $data['region'],
                $data['region_name'],
                $data['city'],
                $data['country']
            )
        ) {
            $this->logger->error(
                'Bad response format while checking ips',
                [
                    'content' => $content,
                    'ip'      => $ip,
                ]
            );
            throw new BadResponseFormat('Bad response format while checking ips');
        }

        return new IPCheckResponseDto(
            $data['ip_address'],
            $data['city'],
            $data['country'],
            $data['country_code'] ?? null,
            $data['lat'] ?? null,
            $data['lon'] ?? null,
            $data['region'],
            $data['region_name'],
            $data['timezone'] ?? null
        );
    }


    /**
     * @return string
     */
    public function getLandingName(): string
    {
        return $this->landingName;
    }

    /**
     * @return string
     */
    public function getAuthToken(): string
    {
        return $this->authToken;
    }

    /**
     * @return string
     */
    public function getBaseUri(): string
    {
        return $this->baseUri;
    }

    /**
     * @return ClientInterface
     */
    public function getHttpClient(): ClientInterface
    {
        return $this->httpClient;
    }

    /**
     * @return LoggerInterface
     */
    public function getLogger(): LoggerInterface
    {
        return $this->logger;
    }
}
