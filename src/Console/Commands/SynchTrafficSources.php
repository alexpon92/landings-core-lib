<?php
declare(strict_types=1);

namespace LandingsCore\Console\Commands;

use Illuminate\Console\Command;

class SynchTrafficSources extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'core:synch-traffic-sources';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Synch traffic sources from core';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function handle(\LandingsCore\Domain\CorePackage\SynchTrafficSources $service)
    {
        $service->execute();
    }
}