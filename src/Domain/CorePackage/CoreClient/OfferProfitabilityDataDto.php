<?php

namespace LandingsCore\Domain\CorePackage\CoreClient;

final class OfferProfitabilityDataDto
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $landingName;

    /**
     * @var int
     */
    private $trafficSourceId;

    /**
     * @var string
     */
    private $trafficSourceName;

    /**
     * @var int
     */
    private $trafficWebId;

    /**
     * @var string
     */
    private $trafficWebWebId;

    /**
     * @var int
     */
    private $offer_id;

    /**
     * @var float
     */
    private $epc;

    public function __construct(
        int $id,
        string $landingName,
        int $trafficSourceId,
        string $trafficSourceName,
        int $trafficWebId,
        string $trafficWebWebId,
        int $offer_id,
        float $epc
    )
    {
        $this->id = $id;
        $this->landingName = $landingName;
        $this->trafficSourceId = $trafficSourceId;
        $this->trafficSourceName = $trafficSourceName;
        $this->trafficWebId = $trafficWebId;
        $this->trafficWebWebId = $trafficWebWebId;
        $this->offer_id = $offer_id;
        $this->epc = $epc;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getLandingName(): string
    {
        return $this->landingName;
    }

    public function getTrafficSourceId(): int
    {
        return $this->trafficSourceId;
    }

    public function getTrafficSourceName(): string
    {
        return $this->trafficSourceName;
    }

    public function getTrafficWebId(): int
    {
        return $this->trafficWebId;
    }

    public function getTrafficWebWebId(): string
    {
        return $this->trafficWebWebId;
    }

    public function getOfferId(): int
    {
        return $this->offer_id;
    }

    public function getEpc(): float
    {
        return $this->epc;
    }
}
