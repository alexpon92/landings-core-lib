<?php
declare(strict_types=1);

namespace LandingsCore\Domain\Entity;

use Illuminate\Database\Query\JoinClause;
use LandingsCore\Domain\CorePackage\Enums\CardStateEnum;
use LandingsCore\Domain\CorePackage\Enums\CreditHistoryEnum;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Carbon;
use LandingsCore\Domain\CorePackage\Enums\OffersOrderingEnum;
use LandingsCore\Domain\Dto\OfferWithProfitabilityDto;
use Log;

/**
 * App\Domain\Entity\Offer
 *
 * @property int         $id
 * @property int         $external_id
 * @property string      $name
 * @property string      $type
 * @property int         $amount
 * @property int         $weight
 * @property string      $percent
 * @property bool        $is_in_popup
 * @property bool        $is_in_top
 * @property int|null    $duration
 * @property string|null $credit_history
 * @property int|null    $age_from
 * @property int|null    $age_to
 * @property string|null $card_state
 * @property float|null  $probability
 * @property string|null $click_rules
 * @property string|null $additional_info
 * @property string|null $settings
 * @property float|null  $epc
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property Carbon|null $deleted_at
 * @method static bool|null forceDelete()
 * @method static Builder|Offer newModelQuery()
 * @method static Builder|Offer newQuery()
 * @method static \Illuminate\Database\Query\Builder|Offer onlyTrashed()
 * @method static Builder|Offer query()
 * @method static bool|null restore()
 * @method static Builder|Offer whereAgeFrom($value)
 * @method static Builder|Offer whereAgeTo($value)
 * @method static Builder|Offer whereAmount($value)
 * @method static Builder|Offer whereCardState($value)
 * @method static Builder|Offer whereClickRules($value)
 * @method static Builder|Offer whereCreatedAt($value)
 * @method static Builder|Offer whereCreditHistory($value)
 * @method static Builder|Offer whereDeletedAt($value)
 * @method static Builder|Offer whereDuration($value)
 * @method static Builder|Offer whereExternalId($value)
 * @method static Builder|Offer whereId($value)
 * @method static Builder|Offer whereIsInPopup($value)
 * @method static Builder|Offer whereIsInTop($value)
 * @method static Builder|Offer whereName($value)
 * @method static Builder|Offer wherePercent($value)
 * @method static Builder|Offer whereProbability($value)
 * @method static Builder|Offer whereType($value)
 * @method static Builder|Offer whereUpdatedAt($value)
 * @method static Builder|Offer whereWeight($value)
 * @method static \Illuminate\Database\Query\Builder|Offer withTrashed()
 * @method static \Illuminate\Database\Query\Builder|Offer withoutTrashed()
 * @mixin \Eloquent
 * @property string      $img_content
 * @property string|null $vendor_name
 * @method static Builder|Offer whereImgContent($value)
 */
class Offer extends Model
{
    use SoftDeletes;

    public const DEFAULT_SOURCE_KEY = 'default';

    /**
     * @param string $source
     *
     * @return array
     */
    public function getClickRuleBySource(string $source): array
    {
        if ($this->click_rules) {
            $clickRules = json_decode($this->click_rules, true);
            if (is_array($clickRules)) {
                foreach ($clickRules as $clickRule) {
                    if (
                        isset($clickRule['traffic_source_name']) &&
                        mb_strtolower($clickRule['traffic_source_name']) === mb_strtolower($source)
                    ) {
                        return $clickRule;
                    }
                }
            }
        }

        return [];
    }

    /**
     * @param string|null $source
     * @param string|null $campaign
     * @param string|null $loanerHash
     * @param string|null $offerId
     *
     * @return string
     */
    public function getUrl(?string $source, ?string $campaign, ?string $loanerHash): string
    {
        if ($source) {
            $clickRule = $this->getClickRuleBySource($source);
            if ($clickRule && !empty($clickRule['url_pattern'])) {
                return $this->renderUrlPattern(
                    $clickRule['url_pattern'],
                    $source,
                    $campaign ?? '',
                    $loanerHash ?? ''
                );
            }
        }

        $clickRule = $this->getClickRuleBySource(self::DEFAULT_SOURCE_KEY);
        if ($clickRule && !empty($clickRule['url_pattern'])) {
            return $this->renderUrlPattern(
                $clickRule['url_pattern'],
                $source ?? '',
                $campaign ?? '',
                $loanerHash ?? ''
            );
        }

        Log::error(
            'Click rules has no default entry',
            ['offer_id' => $this->external_id, 'click_rules' => $this->click_rules]
        );
        return '';
    }

    /**
     * @param string $url_pattern
     * @param string $source
     * @param string $campaign
     * @param string $loanerHash
     *
     * @return string
     */
    public function renderUrlPattern(
        string $url_pattern,
        string $source,
        string $campaign,
        string $loanerHash
    ): string {
        $wrapperUrlPattern = config('landings-core-lib.wrapper_url_pattern');

        $url = preg_replace(
            ['/\{source\}/i', '/\{wm_id\}/i', '/\{loaner_hash\}/i', '/\{offer_id\}/i'],
            [$source, $campaign, $loanerHash, $this->external_id],
            $url_pattern
        );

        if ($wrapperUrlPattern === null) {
            return $url;
        }

        return preg_replace('/\{url\}/i', urlencode($url), (string)$wrapperUrlPattern);
    }

    /**
     * @param int $id
     *
     * @return Offer|null
     */
    public static function findByExternalId(int $id): ?Offer
    {
        /** @var Offer $offer */
        $offer = self::withTrashed()->where('external_id', $id)->first();
        return $offer;
    }

    /**
     * @param string      $creditHistory
     * @param string      $cardState
     * @param int         $age
     * @param array|null  $excludeVendors
     * @param string|null $sourceName
     * @param string|null $webId
     *
     * @return OfferWithProfitabilityDto[]
     */
    public static function getOffers(
        string $creditHistory,
        string $cardState,
        int $age,
        ?array $excludeVendors = [],
        ?string $sourceName = null,
        ?string $webId = null
    ): array {
        $query = self::query()->select('offers.*')
                     ->from('offers')
                     ->whereIn('offers.credit_history', [$creditHistory, CreditHistoryEnum::NA_CREDIT_HISTORY])
                     ->where('offers.age_from', '<=', $age)
                     ->where('offers.age_to', '>=', $age)
                     ->whereIn('offers.card_state', [$cardState, CardStateEnum::ALL]);

        if (!empty($excludeVendors)) {
            $query->whereNotIn('offers.vendor_name', $excludeVendors);
        }

        $result = [];

        switch (config('landings-core-lib.offers_ordering')) {
            case OffersOrderingEnum::EPC:
                $allOffers = $query->addSelect('op.epc as calculated_epc')
                                   ->leftJoin(
                                       'offer_profitabilities as op',
                                       static function (JoinClause $join) use ($sourceName, $webId) {
                                           $join->on('offers.id', '=', 'op.offer_id')
                                                ->where('op.traffic_source_name', '=', $sourceName)
                                                ->where('op.traffic_web_web_id', '=', $webId);
                                       }
                                   )
                                   ->get();

                foreach ($allOffers as $offer) {
                    $result[] = new OfferWithProfitabilityDto(
                        $offer,
                        $offer->calculated_epc ? (float)$offer->calculated_epc : null
                    );
                }

                break;
            case OffersOrderingEnum::PROBABILITY:
                $offers = $query->orderBy('probability', 'DESC')
                                ->limit(config('landings-core-lib.total_offers_in_list'))
                                ->get();

                // Get other offers to fill empty slots
                if ($offers->count() < config('landings-core-lib.total_offers_in_list')) {
                    $otherOffers = self::query()->where('is_in_popup', false)
                                       ->whereNotIn('id', $offers->pluck('id')->all())
                                       ->where('card_state', CardStateEnum::ALL)
                                       ->orderBy('probability', 'DESC')
                                       ->limit(config('landings-core-lib.total_offers_in_list') - $offers->count())
                                       ->get();

                    /** @var Offer[] $offers */
                    $offers = $offers->concat($otherOffers);
                }

                foreach ($offers as $offer) {
                    $result[] = (new OfferWithProfitabilityDto($offer, null))
                        ->setDisplayProbability((int)$offer->probability);
                }

                break;
        }

        return $result;
    }

    /**
     * @param array|null $excludeVendors
     *
     * @return Offer|null
     */
    public static function getOffersForPopup(
        ?array $excludeVendors = []
    ): ?Offer {
        $query = self::query()
                     ->where('is_in_popup', true);

        if (!empty($excludeVendors)) {
            $query->whereNotIn('vendor_name', $excludeVendors);
        }

        /** @var Offer $offer */
        $offer = $query->orderBy('probability', 'DESC')
                       ->first();

        return $offer;
    }

    public function getAdditionalInfo(): array
    {
        return null !== $this->additional_info ? json_decode($this->additional_info, true) : [];
    }

    public function setAdditionalInfo(?array $additionalInfo): void
    {
        $this->additional_info = null !== $additionalInfo ? json_encode($additionalInfo) : null;
    }

    public function getSettings(): array
    {
        return null !== $this->settings ? json_decode($this->settings, true) : [];
    }

    public function setSettings(?array $settings): void
    {
        $this->settings = null !== $settings ? json_encode($settings) : null;
    }

    public function isForRefferal(): bool
    {
        $settings = $this->getSettings();
        if (isset($settings['only_for_referrals'])) {
            return (bool)$settings['only_for_referrals'];
        }

        return false;
    }
}
