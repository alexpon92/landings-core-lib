<?php
declare(strict_types=1);

namespace LandingsCore\Listeners;

use LandingsCore\Domain\Entity\Session;
use LandingsCore\Domain\Events\LoanerUpdated;
use LandingsCore\Domain\Services\OfferVendorsDuplicatesCheck\Factories\OfferVendorsCheckFactory;
use Psr\Log\LoggerInterface;

class OfferVendorsDuplicatesCheck extends AbstractListener
{
    /**
     * @var OfferVendorsCheckFactory
     */
    private $vendorsFactory;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * OfferVendorsDuplicatesCheck constructor.
     *
     * @param OfferVendorsCheckFactory $vendorsFactory
     * @param LoggerInterface          $logger
     */
    public function __construct(OfferVendorsCheckFactory $vendorsFactory, LoggerInterface $logger)
    {
        $this->vendorsFactory = $vendorsFactory;
        $this->logger         = $logger;
    }

    public function onLoanerUpdated(LoanerUpdated $event): void
    {
        /** @var Session $session */
        $session = Session::find($event->getSessionId());

        if (!$session || !$session->phone) {
            $this->logger->error(
                'Session is not found in offer duplicates check',
                [
                    'session_id' => $event->getSessionId()
                ]
            );
            return;
        }

        $keys = $this->vendorsFactory->getAllServiceKeys();
        foreach ($keys as $key) {
            $service = $this->vendorsFactory->buildByKey($key);
            $service->searchDuplicates($session);
        }
    }

    public function getConnection(): string
    {
        return (string)config('landings-core-lib.offer_vendors_duplicate_check.queue.connection');
    }

    public function getQueue(): string
    {
        return (string)config('landings-core-lib.offer_vendors_duplicate_check.queue.name');
    }

    /**
     * Register the listeners for the subscriber.
     *
     * @param \Illuminate\Events\Dispatcher $events
     */
    public function subscribe($events)
    {
        $events->listen(
            LoanerUpdated::class,
            self::class . '@onLoanerUpdated'
        );

    }
}