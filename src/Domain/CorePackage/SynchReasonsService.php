<?php
declare(strict_types=1);

namespace LandingsCore\Domain\CorePackage;

use LandingsCore\Domain\Entity\LeaveReason;
use LandingsCore\Domain\CorePackage\CoreClient\ICoreClient;
use LandingsCore\Domain\CorePackage\CoreClient\LeaveReasonDto;

class SynchReasonsService
{
    /**
     * @var ICoreClient
     */
    private $client;

    /**
     * SynchReasonsService constructor.
     *
     * @param ICoreClient $client
     */
    public function __construct(ICoreClient $client)
    {
        $this->client = $client;
    }


    /**
     * @throws \Exception
     */
    public function synch(): void
    {
        $reasons   = $this->client->getLeaveReasons();
        $reasonIds = array_map(
            static function (LeaveReasonDto $elem) {
                return $elem->getId();
            },
            $reasons
        );

        foreach ($reasons as $reason) {
            $leaveReason = LeaveReason::findByExternalId($reason->getId());
            if (!$leaveReason) {
                $leaveReason = new LeaveReason();
            }

            $leaveReason->external_id = $reason->getId();
            $leaveReason->texts       = json_encode($reason->getTexts());
            $leaveReason->url_pattern = $reason->getUrlPattern();
            $leaveReason->deleted_at  = null;
            $leaveReason->save();
        }
        LeaveReason::whereNotIn('external_id', $reasonIds)->delete();
    }
}