<?php

namespace LandingsCore\Console\Commands;


use LandingsCore\Domain\CorePackage\CoreClient\ClientExceptions\BadResponseCode;
use LandingsCore\Domain\CorePackage\CoreClient\ClientExceptions\BadResponseFormat;
use Illuminate\Console\Command;
use LandingsCore\Domain\CorePackage\SyncLoanersSellingServices;
use LandingsCore\Domain\CorePackage\SyncSellingReports;

class GetSellingServicesAndReports extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'core:sync-selling-services-and-reports';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Synchronize selling services and reports from core';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @param SyncLoanersSellingServices $syncLoanersSellingServices
     * @param SyncSellingReports         $sellingReports
     * @throws BadResponseCode
     * @throws BadResponseFormat
     */
    public function handle(SyncLoanersSellingServices $syncLoanersSellingServices, SyncSellingReports $sellingReports)
    {
        $syncLoanersSellingServices->execute();
        $sellingReports->execute();
    }
}
