<?php

declare(strict_types=1);

namespace LandingsCore\Domain\Entity;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * LandingsCore\Domain\Entity\LoanerSellingService
 *
 * @property int         $id
 * @property int         $external_id
 * @property string      $type
 * @property string|null $name
 * @property string|null $description
 * @property string|null $logo
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|LoanerSellingService newModelQuery()
 * @method static Builder|LoanerSellingService newQuery()
 * @method static Builder|LoanerSellingService query()
 * @method static Builder|LoanerSellingService whereCreatedAt($value)
 * @method static Builder|LoanerSellingService whereDescription($value)
 * @method static Builder|LoanerSellingService whereId($value)
 * @method static Builder|LoanerSellingService whereExternalId($value)
 * @method static Builder|LoanerSellingService whereLogo($value)
 * @method static Builder|LoanerSellingService whereName($value)
 * @method static Builder|LoanerSellingService whereType($value)
 * @method static Builder|LoanerSellingService whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class LoanerSellingService extends Model
{

    /**
     * @param int $id
     * @return static|null
     */
    public static function findByExternalId(int $id): ?self
    {
        /** @var LoanerSellingService $sellingService */
        $sellingService = self::where('external_id', $id)->first();
        return $sellingService;
    }

}
