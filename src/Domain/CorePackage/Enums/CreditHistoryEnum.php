<?php

declare(strict_types=1);

namespace LandingsCore\Domain\CorePackage\Enums;

class CreditHistoryEnum
{
    public const WITH_CREDIT_HISTORY    = 'with';
    public const WITHOUT_CREDIT_HISTORY = 'without';
    public const NA_CREDIT_HISTORY      = 'na';

    public static function getAll(): array
    {
        return [
            self::WITH_CREDIT_HISTORY,
            self::WITHOUT_CREDIT_HISTORY,
            self::NA_CREDIT_HISTORY
        ];
    }
}