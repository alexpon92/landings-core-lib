<?php

declare(strict_types=1);

namespace LandingsCore\Domain\CorePackage;

use LandingsCore\Domain\CorePackage\CoreClient\ICoreClient;
use LandingsCore\Domain\Entity\TrafficSource;

class SynchTrafficSources
{
    /**
     * @var ICoreClient
     */
    private $client;

    /**
     * SynchTrafficSources constructor.
     *
     * @param ICoreClient $client
     */
    public function __construct(ICoreClient $client)
    {
        $this->client = $client;
    }

    public function execute(): void
    {
        /** @var TrafficSource[] $localSources */
        $localSources       = TrafficSource::getAll()->keyBy('external_id')->all();
        $existedIds         = [];
        $coreTrafficSources = $this->client->getTrafficSources();

        foreach ($coreTrafficSources as $trafficSource) {
            if (isset($localSources[$trafficSource->getId()])) {
                $changed = false;
                $existedIds[$trafficSource->getId()] = $trafficSource->getId();

                if ($trafficSource->getName() !== $localSources[$trafficSource->getId()]->name) {
                    $localSources[$trafficSource->getId()]->name = $trafficSource->getName();
                }

                if ($trafficSource->getType() !== $localSources[$trafficSource->getId()]->type) {
                    $changed = true;
                    $localSources[$trafficSource->getId()]->type = $trafficSource->getType();
                }

                if ($trafficSource->getAccessType() !== $localSources[$trafficSource->getId()]->access_type) {
                    $changed = true;
                    $localSources[$trafficSource->getId()]->access_type = $trafficSource->getAccessType();
                }

                if ($changed) {
                    $localSources[$trafficSource->getId()]->save();
                }
            } else {
                $newTraffSource              = new TrafficSource();
                $newTraffSource->external_id = $trafficSource->getId();
                $newTraffSource->name        = $trafficSource->getName();
                $newTraffSource->description = $trafficSource->getDescription();
                $newTraffSource->type        = $trafficSource->getType();
                $newTraffSource->access_type = $trafficSource->getAccessType();
                $newTraffSource->save();
            }
        }

        foreach ($localSources as $trafficSource) {
            if (!isset($existedIds[$trafficSource->external_id])) {
                $trafficSource->delete();
            }
        }
    }

}