<?php

declare(strict_types=1);

namespace LandingsCore\Domain\CorePackage\CoreClient\SellingReports;

use Carbon\Carbon;

class SellingReportDto
{
    private int     $externalId;
    private int     $loanerId;
    private int     $personalDataId;
    private array   $sellingServiceIds;
    private string  $type;
    private ?string $transactionAmount;
    private ?string $transactionCurrency;
    private ?string $transactionLocalAmount;
    private ?string $transactionLocalCurrency;
    private Carbon  $reportedAt;

    /**
     * SellingReportDto constructor.
     *
     * @param int         $externalId
     * @param int         $loanerId
     * @param int         $personalDataId
     * @param array       $sellingServiceIds
     * @param string      $type
     * @param string|null $transactionAmount
     * @param string|null $transactionCurrency
     * @param string|null $transactionLocalAmount
     * @param string|null $transactionLocalCurrency
     * @param Carbon      $reportedAt
     */
    public function __construct(
        int $externalId,
        int $loanerId,
        int $personalDataId,
        array $sellingServiceIds,
        string $type,
        ?string $transactionAmount,
        ?string $transactionCurrency,
        ?string $transactionLocalAmount,
        ?string $transactionLocalCurrency,
        Carbon $reportedAt
    ) {
        $this->externalId               = $externalId;
        $this->loanerId                 = $loanerId;
        $this->personalDataId           = $personalDataId;
        $this->sellingServiceIds        = $sellingServiceIds;
        $this->type                     = $type;
        $this->transactionAmount        = $transactionAmount;
        $this->transactionCurrency      = $transactionCurrency;
        $this->transactionLocalAmount   = $transactionLocalAmount;
        $this->transactionLocalCurrency = $transactionLocalCurrency;
        $this->reportedAt               = $reportedAt;
    }

    /**
     * @return int
     */
    public function getExternalId(): int
    {
        return $this->externalId;
    }

    /**
     * @return int
     */
    public function getLoanerId(): int
    {
        return $this->loanerId;
    }

    /**
     * @return int
     */
    public function getPersonalDataId(): int
    {
        return $this->personalDataId;
    }

    /**
     * @return array
     */
    public function getSellingServiceIds(): array
    {
        return $this->sellingServiceIds;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @return string|null
     */
    public function getTransactionAmount(): ?string
    {
        return $this->transactionAmount;
    }

    /**
     * @return string|null
     */
    public function getTransactionCurrency(): ?string
    {
        return $this->transactionCurrency;
    }

    /**
     * @return string|null
     */
    public function getTransactionLocalAmount(): ?string
    {
        return $this->transactionLocalAmount;
    }

    /**
     * @return string|null
     */
    public function getTransactionLocalCurrency(): ?string
    {
        return $this->transactionLocalCurrency;
    }

    /**
     * @return Carbon
     */
    public function getReportedAt(): Carbon
    {
        return $this->reportedAt;
    }
}