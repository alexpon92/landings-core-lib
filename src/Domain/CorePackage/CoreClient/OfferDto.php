<?php
declare(strict_types=1);

namespace LandingsCore\Domain\CorePackage\CoreClient;

final class OfferDto
{
    private int     $id;
    private string  $name;
    private string  $imgContent;
    private string  $type;
    private int     $amount;
    private int     $weight;
    private ?int    $duration;
    private ?string $creditHistory;
    private ?int    $ageFrom;
    private ?int    $ageTo;
    private string  $landingName;
    private ?string $probability;
    private string  $percent;
    private string  $cardState;
    private bool    $isInTop;
    private bool    $isInPopup;
    private array   $clickRules;
    private ?array  $additionalInfo;
    private ?float  $epc;
    private ?array  $settings;

    /**
     * OfferDto constructor.
     *
     * @param int         $id
     * @param string      $name
     * @param string      $imgContent
     * @param string      $type
     * @param int         $amount
     * @param int         $weight
     * @param int|null    $duration
     * @param string|null $creditHistory
     * @param int|null    $ageFrom
     * @param int|null    $ageTo
     * @param string      $landingName
     * @param string|null $probability
     * @param string      $percent
     * @param string      $cardState
     * @param bool        $isInTop
     * @param bool        $isInPopup
     * @param array       $clickRules
     * @param array|null  $additionalInfo
     * @param float|null  $epc
     * @param array|null  $settings
     */
    public function __construct(
        int $id,
        string $name,
        string $imgContent,
        string $type,
        int $amount,
        int $weight,
        ?int $duration,
        ?string $creditHistory,
        ?int $ageFrom,
        ?int $ageTo,
        string $landingName,
        ?string $probability,
        string $percent,
        string $cardState,
        bool $isInTop,
        bool $isInPopup,
        array $clickRules,
        ?array $additionalInfo,
        ?float $epc = null,
        ?array $settings
    ) {
        $this->id             = $id;
        $this->name           = $name;
        $this->imgContent     = $imgContent;
        $this->type           = $type;
        $this->amount         = $amount;
        $this->weight         = $weight;
        $this->duration       = $duration;
        $this->creditHistory  = $creditHistory;
        $this->ageFrom        = $ageFrom;
        $this->ageTo          = $ageTo;
        $this->landingName    = $landingName;
        $this->probability    = $probability;
        $this->percent        = $percent;
        $this->cardState      = $cardState;
        $this->isInTop        = $isInTop;
        $this->isInPopup      = $isInPopup;
        $this->clickRules     = $clickRules;
        $this->additionalInfo = $additionalInfo;
        $this->epc            = $epc;
        $this->settings       = $settings;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getImgContent(): string
    {
        return $this->imgContent;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @return int
     */
    public function getAmount(): int
    {
        return $this->amount;
    }

    /**
     * @return int
     */
    public function getWeight(): int
    {
        return $this->weight;
    }

    /**
     * @return int|null
     */
    public function getDuration(): ?int
    {
        return $this->duration;
    }

    /**
     * @return string|null
     */
    public function getCreditHistory(): ?string
    {
        return $this->creditHistory;
    }

    /**
     * @return int|null
     */
    public function getAgeFrom(): ?int
    {
        return $this->ageFrom;
    }

    /**
     * @return int|null
     */
    public function getAgeTo(): ?int
    {
        return $this->ageTo;
    }

    /**
     * @return string
     */
    public function getLandingName(): string
    {
        return $this->landingName;
    }

    /**
     * @return string|null
     */
    public function getProbability(): ?string
    {
        return $this->probability;
    }

    /**
     * @return string
     */
    public function getPercent(): string
    {
        return $this->percent;
    }

    /**
     * @return string
     */
    public function getCardState(): string
    {
        return $this->cardState;
    }

    /**
     * @return bool
     */
    public function isInTop(): bool
    {
        return $this->isInTop;
    }

    /**
     * @return bool
     */
    public function isInPopup(): bool
    {
        return $this->isInPopup;
    }

    /**
     * @return array
     */
    public function getClickRules(): array
    {
        return $this->clickRules;
    }

    /**
     * @return array|null
     */
    public function getAdditionalInfo(): ?array
    {
        return $this->additionalInfo;
    }

    /**
     * @return float|null
     */
    public function getEpc(): ?float
    {
        return $this->epc;
    }

    /**
     * @return array|null
     */
    public function getSettings(): ?array
    {
        return $this->settings;
    }
}
