To install package require it through composer.

1. Register "LandingsCore\LandingsCoreServiceProvider"
2. Publish config ```php artisan vendor:publish --provider="LandingsCore\LandingsCoreServiceProvider" --tag=config```
3. Set project type in config
4. Publish migrations  ```php artisan vendor:publish --provider="LandingsCore\LandingsCoreServiceProvider" --tag=migrations```
5. Add listeners and etc.

Don't forget to add symlinks for your project:
```bash
cd {{ project }}

#for offers
[ -d storage/app/public/offers-img ] || mkdir storage/app/public/offers-img

ln -nfs {{ project }}/storage/app/public/offers-img {{ release }}/public

#for loaner selling services
[ -d storage/app/public/selling-services-img ] || mkdir storage/app/public/selling-services-img

ln -nfs {{ project }}/storage/app/public/selling-services-img {{ release }}/public
```


#### Cabinet Auth config

setup your auth provider in config/auth.php like this:
```php
[
    'driver' => 'eloquent',
    'model'  => \LandingsCore\Domain\Entity\PersonalData::class,
]
```