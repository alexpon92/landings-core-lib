<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePersonalDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('personal_data', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('session_id');
            $table->string('phone')->unique();
            $table->integer('core_loaner_id')->nullable();
            $table->integer('core_personal_data_id')->nullable();
            $table->string('core_hash')->nullable();
            $table->jsonb('payload')->nullable();
            $table->boolean('conversion')->default(false);
            $table->timestamp('conversion_at')->nullable();
            $table->tinyInteger('form_step')->nullable();
            $table->string('remember')->nullable();
            $table->timestamps();
            $table->index('session_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('personal_data');
    }
}
