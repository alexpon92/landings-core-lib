<?php
declare(strict_types=1);

namespace LandingsCore\Domain\Services\OfferVendorsDuplicatesCheck\Factories;

use GuzzleHttp\Client;
use LandingsCore\Domain\Services\OfferVendorsDuplicatesCheck\CheckServices\AbstractCheckService;
use LandingsCore\Domain\Services\OfferVendorsDuplicatesCheck\CheckServices\Vendors\CreditPlus;
use LandingsCore\Domain\Services\OfferVendorsDuplicatesCheck\CheckServices\VendorsEnum;
use LandingsCore\Domain\Services\OfferVendorsDuplicatesCheck\Factories\Exceptions\UndefinedVendorTypeException;
use Psr\Log\LoggerInterface;

class OfferVendorsCheckFactory
{
    /**
     * @var string[]
     */
    private $map;

    /**
     * OfferVendorsCheckFactory constructor.
     */
    public function __construct()
    {
        $this->map = [];
    }

    public function setVendor(string $key): self
    {
        $this->map[] = $key;
        return $this;
    }

    public function buildByKey(string $key): AbstractCheckService
    {
        switch ($key) {
            case VendorsEnum::TYPE_RU_CREDIT_PLUS:
                return new CreditPlus(
                    app(Client::class),
                    config('landings-core-lib.offer_vendors_duplicate_check.creditplus.base_uri'),
                    config('landings-core-lib.offer_vendors_duplicate_check.creditplus.login'),
                    config('landings-core-lib.offer_vendors_duplicate_check.creditplus.secret'),
                    app(LoggerInterface::class)
                );
        }

        throw new UndefinedVendorTypeException("Undefined vendor type {$key}");
    }

    public function getAllServiceKeys(): array
    {
        return $this->map;
    }
}