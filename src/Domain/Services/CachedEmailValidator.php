<?php

namespace LandingsCore\Domain\Services;

use Illuminate\Support\Facades\Cache;
use LandingsCore\Domain\CorePackage\CoreClient\ICoreClient;

class CachedEmailValidator
{
    private const CACHE_KEY_PREFIX = 'core_client_validated_email_';

    /**
     * @var ICoreClient
     */
    private $coreClient;

    public function __construct(ICoreClient $coreClient)
    {
        $this->coreClient = $coreClient;
    }

    public function isValidEmail(string $email): bool
    {
        $key = self::CACHE_KEY_PREFIX . $email;

        if (Cache::has($key)) {
            return Cache::get($key);
        }

        $result = $this->coreClient->validateEmail($email);


        if ($result) {
            Cache::add($key, true, new \DateInterval('P7D'));
        }

        return $result;
    }

}
