<?php
declare(strict_types=1);

namespace LandingsCore\Domain\CorePackage;

use LandingsCore\Domain\CorePackage\CoreClient\ICoreClient;
use LandingsCore\Domain\CorePackage\CoreClient\OfferProfitabilityDataDto;
use LandingsCore\Domain\Entity\Offer;
use LandingsCore\Domain\Entity\OfferProfitability;

final class SynchProfitabilityDataService
{
    /**
     * @var ICoreClient
     */
    private $client;

    /**
     * SyncProfitabilityDataService constructor.
     *
     * @param ICoreClient $client
     */
    public function __construct(ICoreClient $client)
    {
        $this->client = $client;
    }

    public function synch(): void
    {
        $offersProfitability    = $this->client->getOffersProfitability();
        $offersProfitabilityIds = array_map(
            static function (OfferProfitabilityDataDto $elem) {
                return $elem->getId();
            },
            $offersProfitability
        );

        foreach ($offersProfitability as $offerProfitability) {
            $offerEntity = Offer::findByExternalId($offerProfitability->getOfferId());

            if ($offerEntity === null) {
                continue;
            }

            $profitabilityEntity = OfferProfitability::whereExternalId($offerProfitability->getId())->first();

            if (!$profitabilityEntity) {
                $profitabilityEntity = new OfferProfitability();
            }

            $profitabilityEntity->external_id         = $offerProfitability->getId();
            $profitabilityEntity->traffic_source_id   = $offerProfitability->getTrafficSourceId();
            $profitabilityEntity->traffic_source_name = $offerProfitability->getTrafficSourceName();
            $profitabilityEntity->traffic_web_id      = $offerProfitability->getTrafficWebId();
            $profitabilityEntity->traffic_web_web_id  = $offerProfitability->getTrafficWebWebId();
            $profitabilityEntity->offer_id            = $offerEntity->id;
            $profitabilityEntity->external_offer_id   = $offerProfitability->getOfferId();
            $profitabilityEntity->epc                 = $offerProfitability->getEpc();
            $profitabilityEntity->save();
        }

        OfferProfitability::whereNotIn('external_id', $offersProfitabilityIds)->delete();
    }
}
