<?php
declare(strict_types=1);

namespace LandingsCore\Domain\Services;

class OffersGuard
{
    /**
     * @param string|null $token
     * @param bool|null   $isAllStepsBeforeCardPassed
     *
     * @return bool
     */
    public function isOffersVisitAllowed(?string $token, ?bool $isAllStepsBeforeCardPassed): bool
    {
        return $token || $isAllStepsBeforeCardPassed;
    }
}