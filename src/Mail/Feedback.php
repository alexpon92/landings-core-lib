<?php

namespace LandingsCore\Mail;

use Illuminate\Mail\Mailable;

class Feedback extends Mailable
{
    /**
     * @var array
     */
    private $data;

    /**
     * Feedback constructor.
     * @param array $data
     */
    public function __construct(array $data)
    {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $subject = config('landings-core-lib.feedback_mail_subject') . $this->data['phone'];

        return $this->subject($subject)
                    ->view('vendor.landings-core-lib.mail.feedback', ['data' => $this->data]);
    }
}
