<?php
declare(strict_types=1);

namespace LandingsCore\Domain\Services\OfferVendorsDuplicatesCheck\CheckServices;

class CheckResponse
{
    public const EXISTS     = 'exists';
    public const NOT_EXISTS = 'not_exists';

    /**
     * @var string
     */
    private $serviceKey;
    /**
     * @var string
     */
    private $status;

    /**
     * CheckResponse constructor.
     *
     * @param string $serviceKey
     * @param string $status
     */
    public function __construct(string $serviceKey, string $status)
    {
        $this->serviceKey = $serviceKey;
        $this->status     = $status;
    }

    public static function buildForExists(string $serviceKey): self
    {
        return new self(
            $serviceKey,
            self::EXISTS
        );
    }

    public static function buildForNotExists(string $serviceKey): self
    {
        return new self(
            $serviceKey,
            self::NOT_EXISTS
        );
    }

    /**
     * @return string
     */
    public function getServiceKey(): string
    {
        return $this->serviceKey;
    }

    /**
     * @return string
     */
    public function getStatus(): string
    {
        return $this->status;
    }

    public function isExists(): bool
    {
        return $this->status === self::EXISTS;
    }
}