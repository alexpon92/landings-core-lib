<?php
declare(strict_types=1);

namespace LandingsCore\Http\Middleware;

use Illuminate\Http\Request;
use LandingsCore\Domain\Entity\Session;
use Closure;

class RegenerateSignedOffSession
{
    public function handle(Request $request, Closure $next)
    {
        $session = Session::find($request->session()->getId());
        if ($session && $session->signed_off) {
            $request->session()->regenerate();
        }
        return $next($request);
    }
}