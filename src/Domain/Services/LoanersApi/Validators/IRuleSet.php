<?php
declare(strict_types=1);

namespace LandingsCore\Domain\Services\LoanersApi\Validators;

interface IRuleSet
{
    public function getRules(): array;

    public function getGeoKey(): string;
}