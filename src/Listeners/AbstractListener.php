<?php

declare(strict_types=1);

namespace LandingsCore\Listeners;

use Illuminate\Contracts\Queue\ShouldQueue;

abstract class AbstractListener implements ShouldQueue
{
    abstract public function getConnection(): string;
    abstract public function getQueue(): string;

    public function __get($name)
    {
        if ($name === 'connection') {
            return $this->getConnection();
        }

        if ($name === 'queue') {
            return $this->getQueue();
        }

        return null;
    }
}
