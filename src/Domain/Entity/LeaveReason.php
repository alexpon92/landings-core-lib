<?php
declare(strict_types=1);

namespace LandingsCore\Domain\Entity;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Carbon;
use Log;

/**
 * App\Domain\Entity\LeaveReason
 *
 * @property int                             $id
 * @property int                             $external_id
 * @property mixed                           $texts
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property Carbon|null $deleted_at
 * @method static bool|null forceDelete()
 * @method static Builder|LeaveReason newModelQuery()
 * @method static Builder|LeaveReason newQuery()
 * @method static \Illuminate\Database\Query\Builder|LeaveReason onlyTrashed()
 * @method static Builder|LeaveReason query()
 * @method static bool|null restore()
 * @method static Builder|LeaveReason whereCreatedAt($value)
 * @method static Builder|LeaveReason whereDeletedAt($value)
 * @method static Builder|LeaveReason whereExternalId($value)
 * @method static Builder|LeaveReason whereId($value)
 * @method static Builder|LeaveReason whereTexts($value)
 * @method static Builder|LeaveReason whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|LeaveReason withTrashed()
 * @method static \Illuminate\Database\Query\Builder|LeaveReason withoutTrashed()
 * @mixin \Eloquent
 * @property string $url_pattern
 * @method static Builder|LeaveReason whereUrlPattern($value)
 */
class LeaveReason extends Model
{
    use SoftDeletes;

    public function getTexts(): array
    {
        if (null !== $this->texts) {
            return json_decode($this->texts, true);
        }
        return [];
    }

    public function getTextByLocale(string $locale): string
    {
        $texts = $this->getTexts();
        if (!empty($texts) && isset($texts[$locale])) {
            return $texts[$locale];
        }

        Log::error(
            'Leave reason has no texts for locale!',
            ['leave_reason_id' => $this->id, 'texts' => $this->texts, 'locale' => $locale]
        );
        return collect($texts)->first() ?? '';
    }

    /**
     * @param string|null $source
     * @param string|null $campaign
     * @param string|null $loanerHash
     * @param string|null $offerId
     *
     * @return string
     */
    public function renderUrlPattern(?string $source, ?string $campaign, ?string $loanerHash, ?string $offerId): string
    {
        $source     = $source ?? '';
        $campaign   = $campaign ?? '';
        $loanerHash = $loanerHash ?? '';
        $offerId    = $offerId ?? '';
        return preg_replace(
            ['/\{source\}/i', '/\{wm_id\}/i', '/\{loaner_hash\}/i', '/\{offer_id\}/i'],
            [$source, $campaign, $loanerHash, $offerId],
            $this->url_pattern
        );
    }

    public static function findByExternalId(int $id): ?LeaveReason
    {
        /** @var LeaveReason $reason */
        $reason = self::withTrashed()->where('external_id', $id)->first();
        return $reason;
    }
}
