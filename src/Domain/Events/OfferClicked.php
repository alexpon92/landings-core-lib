<?php

namespace LandingsCore\Domain\Events;

use Carbon\Carbon;

final class OfferClicked
{
    /**
     * @var int
     */
    private $offerId;

    /**
     * @var string|null
     */
    private $sessionId;

    /**
     * @var Carbon
     */
    private $triggeredAt;

    /**
     * @var string|null
     */
    private $utmSource;

    /**
     * @var string|null
     */
    private $utmCampaign;

    /**
     * @var string|null
     */
    private $utmMedium;

    /**
     * OfferClicked constructor.
     *
     * @param int         $offerId
     * @param string|null $sessionId
     * @param string|null $utmSource
     * @param string|null $utmCampaign
     * @param string|null $utmMedium
     */
    public function __construct(
        int $offerId,
        ?string $sessionId = null,
        ?string $utmSource = null,
        ?string $utmCampaign = null,
        ?string $utmMedium = null
    )
    {
        $this->offerId   = $offerId;
        $this->sessionId = $sessionId;
        $this->triggeredAt = now();
        $this->utmSource = $utmSource;
        $this->utmCampaign = $utmCampaign;
        $this->utmMedium = $utmMedium;
    }

    public function getOfferId(): int
    {
        return $this->offerId;
    }

    public function getTriggeredAt(): Carbon
    {
        return $this->triggeredAt;
    }

    public function getSessionId(): ?string
    {
        return $this->sessionId;
    }

    public function getUtmSource(): ?string
    {
        return $this->utmSource;
    }

    public function getUtmCampaign(): ?string
    {
        return $this->utmCampaign;
    }

    public function getUtmMedium(): ?string
    {
        return $this->utmMedium;
    }
}
