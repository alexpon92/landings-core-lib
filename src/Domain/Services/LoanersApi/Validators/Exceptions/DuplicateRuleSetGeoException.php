<?php
declare(strict_types=1);

namespace LandingsCore\Domain\Services\LoanersApi\Validators\Exceptions;

class DuplicateRuleSetGeoException extends \Exception
{

}