<?php

namespace LandingsCore\Console\Commands;

use LandingsCore\Domain\CorePackage\CoreClient\ClientExceptions\BadResponseCode;
use LandingsCore\Domain\CorePackage\CoreClient\ClientExceptions\BadResponseFormat;
use LandingsCore\Domain\CorePackage\SynchSignedOffLoanersService;
use Illuminate\Console\Command;

class SynchSignedOffLoaners extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'core:synch-signed-off-loaners';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Synch signed off loaners from core';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @param SynchSignedOffLoanersService $service
     *
     * @throws BadResponseCode
     * @throws BadResponseFormat
     */
    public function handle(SynchSignedOffLoanersService $service)
    {
        $service->execute();
    }
}
