<?php
declare(strict_types=1);

namespace LandingsCore\Domain\Services\Cabinet;


use LandingsCore\Domain\CorePackage\CoreClient\ClientExceptions\BadResponseCode;
use LandingsCore\Domain\CorePackage\CoreClient\ClientExceptions\BadResponseFormat;
use LandingsCore\Domain\CorePackage\CoreClient\FeedDto;
use LandingsCore\Domain\CorePackage\CoreClient\ICoreClient;
use LandingsCore\Domain\Entity\LoanerFeed;

class SyncLoanersFeed
{
    /**
     * @var ICoreClient
     */
    private $client;

    /**
     * SynchTrafficSources constructor.
     *
     * @param ICoreClient $client
     */
    public function __construct(ICoreClient $client)
    {
        $this->client = $client;
    }

    /**
     * @throws BadResponseCode
     * @throws BadResponseFormat
     */
    public function execute(): void
    {

        $feed = $this->client->getLoanersFeed(LoanerFeed::getMaxId());

        /** @var FeedDto $feedItem */
        foreach ($feed as $feedItem) {
            $feedEntity                 = new LoanerFeed();
            $feedEntity->external_id    = $feedItem->getId();
            $feedEntity->phone          = $feedItem->getPhone();
            $feedEntity->type           = $feedItem->getType();
            $feedEntity->core_loaner_id = $feedItem->getLoanerId();
            $feedEntity->triggered_at   = $feedItem->getTriggeredAt();
            $feedEntity->setPayload($feedItem->getPayload());
            $feedEntity->save();
        }

    }

}