<?php

declare(strict_types=1);

namespace LandingsCore\Domain\Entity;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class TrafficSource
 *
 * @package LandingsCore\Domain\Entity
 * @property int         $id
 * @property string      $name
 * @property string|null $description
 * @property int         $external_id
 * @property Carbon      $created_at
 * @property Carbon      $updated_at
 * @property string|null $type
 * @property string|null $access_type
 * @mixin \Eloquent
 */
class TrafficSource extends Model
{
    public const ACCESS_TYPE_PUBLIC          = 'public';
    public const ACCESS_TYPE_PRIVATE_PAYABLE = 'private_payable';

    public static function getAll()
    {
        return TrafficSource::all();
    }

    public function isAccessTypePrivatePayable(): bool
    {
        return $this->access_type === self::ACCESS_TYPE_PRIVATE_PAYABLE;
    }

    public function isAccessTypePublic(): bool
    {
        return $this->access_type === self::ACCESS_TYPE_PUBLIC;
    }

    public static function findByTrafficSourceName(string $trafficSourceName): ?self
    {
        return self::where('name', $trafficSourceName)->first();
    }
}