<?php
declare(strict_types=1);

namespace LandingsCore\Domain\Services\OfferVendorsDuplicatesCheck;

use LandingsCore\Domain\Dto\OfferVendorDuplicateCheck;
use LandingsCore\Domain\Entity\Session;
use LandingsCore\Domain\Services\OfferVendorsDuplicatesCheck\Factories\OfferVendorsCheckFactory;

class VendorsGuard
{
    /**
     * @var OfferVendorsCheckFactory
     */
    private $factory;

    /**
     * VendorsGuard constructor.
     *
     * @param OfferVendorsCheckFactory $factory
     */
    public function __construct(OfferVendorsCheckFactory $factory)
    {
        $this->factory = $factory;
    }

    public function getExcludedVendors(?Session $session): array
    {
        $keys = $this->factory->getAllServiceKeys();

        if (!$session) {
            return $keys;
        }

        $res = [];
        foreach ($keys as $key) {
            $check = $session->findOfferCheck($key);
            if (!$check || $check->getStatus() === OfferVendorDuplicateCheck::STATUS_EXISTS) {
                $res[] = $key;
            }
        }

        return $res;
    }

}