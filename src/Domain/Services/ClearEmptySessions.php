<?php
declare(strict_types=1);

namespace LandingsCore\Domain\Services;

use LandingsCore\Domain\Entity\Session;

class ClearEmptySessions
{
    /**
     * @param string $lifeTimeDuration
     *
     * @throws \Exception
     */
    public function execute(string $lifeTimeDuration, int $bulkSize)
    {
        $borderDate = now()->sub(new \DateInterval($lifeTimeDuration));

        do {
            $ids = Session::getEmptySessionsIds($borderDate, $bulkSize);
            if (!empty($ids)) {
                Session::deleteByIds($ids);
            }
        } while (!empty($ids));
    }
}