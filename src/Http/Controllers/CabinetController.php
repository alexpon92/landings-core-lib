<?php
declare(strict_types=1);

namespace LandingsCore\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Auth\SessionGuard;
use Illuminate\Contracts\View\View;
use LandingsCore\Domain\CorePackage\Enums\CardStateEnum;
use LandingsCore\Domain\CorePackage\Enums\CreditHistoryEnum;
use LandingsCore\Domain\Entity\LeaveReason;
use LandingsCore\Domain\Entity\Offer;
use LandingsCore\Domain\Entity\PersonalData;
use LandingsCore\Domain\Entity\Session;
use LandingsCore\Domain\Services\Cabinet\Exceptions\PersonalDataNotFoundException;
use LandingsCore\Domain\Services\Cabinet\Exceptions\SignOutException;
use LandingsCore\Domain\Services\OffersBuilder;
use LandingsCore\Domain\Services\OfferVendorsDuplicatesCheck\VendorsGuard;
use LandingsCore\Services\GoogleReCaptcha;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use LandingsCore\Domain\Services\Cabinet\AuthService;
use LandingsCore\Domain\Services\Cabinet\VerifyService;
use PhoneVerifier\Domain\Services\Ip\IpResolver;
use PhoneVerifier\Domain\Services\Verification\SendVerificationSms;
use PhoneVerifier\Domain\Services\Verification\VerifyPhoneByCode;
use Psr\Log\LoggerInterface;
use Validator;

class CabinetController extends Controller
{
    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * LandingController constructor.
     *
     * @param LoggerInterface $logger
     */
    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    public function getConfirmationSms(
        Request $request,
        VerifyService $sendVerificationSms,
        IpResolver $ipResolver,
        GoogleReCaptcha $reCaptcha
    ) {
        $validator = \Illuminate\Support\Facades\Validator::make(
            $request->all(),
            [
                'phone' => [
                    'required',
                    'regex:'.config('landing.loaner_phone_regex'),
                ],
                'token' => 'required|string'
            ]
        );

        if ($validator->fails()) {
            $this->logger->debug(
                'Sms confirmation errors',
                ['request' => $request->all(), 'errors' => $validator->errors()]
            );
            return $this->returnValidationError($validator->errors()->keys());
        }

        $reCaptchaCode = $reCaptcha->execute($request->get('token'), $ipResolver->getIp($request));

        if (GoogleReCaptcha::RECAPTCHA_SUCCESS_CODE !== $reCaptchaCode) {
            $this->logger->error('Code ' . $reCaptchaCode);
            return response('', 403);
        }

        if (config('landing.test_mode')) {
            return response(
                json_encode(
                    [
                        'token'               => 'test',
                        'next_delivery_delay' => 60,
                    ]
                ),
                200,
                ['Content-type' => 'application/json']
            );
        }

        try {
            $log = $sendVerificationSms->execute(
                $request->get('phone'),
                $ipResolver->getIp($request),
                $request->session()->getId()
            );
        } catch ( SignOutException $signOutException) {
            return response('', 404);
        } catch ( PersonalDataNotFoundException $personalDataNotFoundException) {
            return response('', 404);
        } catch (\Exception $exception) {
            return $this->returnValidationError(['phone']);
        }

        return response(
            json_encode(
                [
                    'token'               => $log->token,
                    'next_delivery_delay' => now()->diffInSeconds(
                        $log->created_at->add(new \DateInterval(config('phone-verifier.limits.resend_interval')))
                    ),
                ]
            ),
            200,
            ['Content-type' => 'application/json']
        );
    }

    public function login(
        Request $request,
        VerifyPhoneByCode $verifyPhoneByCodeService,
        AuthService $authService
    ) {
        $validator = Validator::make(
            $request->all(),
            [
                'phone'     => [
                    'required',
                    'regex:'.config('landing.loaner_phone_regex'),
                ],
                'sms_token' => [
                    'nullable',
                    'string',
                    'max:100',
                ],
                'sms_code'  => [
                    'nullable',
                    'string',
                    'size:4',
                ],
            ]
        );

        if ($validator->fails()) {
            $this->logger->debug(
                'Sms confirmation errors',
                ['request' => $request->all(), 'errors' => $validator->errors()]
            );
            return $this->returnValidationError($validator->errors()->keys());
        }



        if (!config('landing.test_mode')) {
            if (empty($request->get('sms_token'))) {
                return $this->returnValidationError(['sms_token']);
            }

            if (empty($request->get('sms_code'))) {
                return $this->returnValidationError(['sms_code']);
            }

            $checkResult = $verifyPhoneByCodeService->execute(
                $request->get('phone'),
                $request->get('sms_code'),
                $request->get('sms_token')
            );

            if (!$checkResult) {
                return $this->returnValidationError(['sms_code']);
            }
        }

        try {
            $authService->execute($request->get('phone'));
        } catch (PersonalDataNotFoundException|SignOutException $e) {
            return response('', 404);
        } catch (\Exception $exception) {
            return $this->returnValidationError(['sms_code']);
        }

        /** @var PersonalData $user */
        $user = $request->user();

        $keyParts = [
            'login',
            config('auth.defaults.guard'),
            sha1(SessionGuard::class)
        ];

        $key = implode('_', $keyParts);

        $request->session()->put($key, $user->id);

        return response()->json([
            'redirect_to' => route(config('landings-core-lib.redirect_cabinet_route')),
        ]);
    }

    public function offers(Request $request, VendorsGuard $guard, OffersBuilder $offersBuilder): View
    {
        $session         = null;
        $payload         = [];
        $excludedVendors = null;

        /** @var PersonalData $personalData */
        $personalData = $request->user();
        $session      = Session::find($personalData->session_id);

        if ($session && $session->signed_off) {
            $session = null;
        }

        if ($session) {
            $payload         = $session->getPayloadAsDotArray();
            $excludedVendors = $guard->getExcludedVendors($session);
        }

        $age = (int)config('landing.default_age');

        if (isset($payload['lead.birthday'])) {
            $age = Carbon::createFromFormat('d.m.Y', $payload['lead.birthday'])
                ->diffInYears(now());
        }
        if ($age < 18) {
            $age = (int)config('landing.default_age');
        }


        $cardState = $personalData->conversion ? CardStateEnum::LINKED : CardStateEnum::NOT_LINKED;

        $creditHistory = $payload['lead.credit_history'] ?? CreditHistoryEnum::NA_CREDIT_HISTORY;

        $listOffers = $offersBuilder->getOffers(
            Offer::getOffers(
                $creditHistory,
                $cardState,
                $age,
                $excludedVendors,
                $payload['utm_source'] ?? $request->session()->get('utm_source'),
                $payload['utm_campaign'] ?? $request->session()->get('utm_campaign')
            )
        );
        $popupOffer = Offer::getOffersForPopup($excludedVendors);
        $reasons    = LeaveReason::all();

        if (!isset($payload['approve_percent'])) {
            $request->session()->put('approve_percent', config('landing.offers.credit_percent'));
        }

        $chunkedOffers = array_chunk($listOffers, config('landings-core-lib.offers_on_first_page'));

        return view(
            config('landings-core-lib.offers_cabinet_view'),
            [
                'title'         => __('offers_page.title'),
                'description'   => __('offers_page.description'),
                'keywords'      => __('offers_page.keywords'),
                'enableFooter'  => true,
                'creditPercent' => $request->session()->get('approve_percent'),
                'name'          => $payload['lead.first_name'] ?? '',
                'listOffers'    => $chunkedOffers,
                'popupOffer'    => $popupOffer,
                'isCardLink'    => false,
                'reasons'       => $reasons,
                'utmSource'     => $payload['utm_source'] ?? '',
                'utmCampaign'   => $payload['utm_campaign'] ?? '',
                'loanerHash'    => null !== $session ? $session->core_hash : ''
            ]
        );
    }

    protected function returnValidationError(array $keys)
    {
        return response(json_encode($keys), 422, ['Content-type' => 'application/json']);
    }
}
