<?php
declare(strict_types=1);

namespace LandingsCore\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TrackOfferRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'id' => ['required', 'numeric'],
        ];
    }

    public function getId(): int
    {
        return (int) $this->get('id');
    }
}
