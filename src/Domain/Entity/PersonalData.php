<?php
declare(strict_types=1);

namespace LandingsCore\Domain\Entity;

use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;
use LandingsCore\Domain\CorePackage\CoreClient\StoreLoaner\LoanerDto;

/**
 * LandingsCore\Domain\Entity\PersonalData
 *
 * @property int         $id
 * @property string      $session_id
 * @property string      $phone
 * @property int|null    $core_loaner_id
 * @property int|null    $core_personal_data_id
 * @property string|null $core_hash
 * @property mixed|null  $payload
 * @property bool        $conversion
 * @property Carbon|null $conversion_at
 * @property int|null    $form_step
 * @property string|null $remember
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|PersonalData newModelQuery()
 * @method static Builder|PersonalData newQuery()
 * @method static Builder|PersonalData query()
 * @method static Builder|PersonalData whereId($value)
 * @method static Builder|PersonalData whereSessionId($value)
 * @method static Builder|PersonalData wherePhone($value)
 * @method static Builder|PersonalData whereCoreLoanerId($value)
 * @method static Builder|PersonalData whereCorePersonalDataId($value)
 * @method static Builder|PersonalData whereCoreHash($value)
 * @method static Builder|PersonalData wherePayload($value)
 * @method static Builder|PersonalData whereConversion($value)
 * @method static Builder|PersonalData whereConversionAt($value)
 * @method static Builder|PersonalData whereFormStep($value)
 * @method static Builder|PersonalData whereRemember($value)
 * @method static Builder|PersonalData whereCreatedAt($value)
 * @method static Builder|PersonalData whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class PersonalData extends Model implements Authenticatable
{
    public function getAuthIdentifierName()
    {
        return 'id';
    }

    public function getAuthIdentifier()
    {
        return $this->id;
    }

    public function getAuthPassword()
    {
        return '';
    }

    public function getRememberToken()
    {
        return '';
    }

    public function setRememberToken($value)
    {
    }

    public function getRememberTokenName()
    {
        return 'remember';
    }

    public static function findByPhone(string $phone): ?self
    {
        /** @var self|null $personalData */
        $personalData = self::query()
                            ->where('phone', $phone)
                            ->first();

        return $personalData;
    }

    public function setLoanerDataFromCoreResponse(LoanerDto $dto): void
    {
        $this->core_loaner_id        = $dto->getId();
        $this->core_personal_data_id = $dto->getPersonalData()->getId();
        $this->core_hash             = $dto->getPersonalData()->getHash();

        $this->save();
    }

    public function getPayload(): array
    {
        if (null === $this->payload) {
            return [];
        }

        return json_decode($this->payload, true);
    }

    public function setPayload(array $payload): void
    {
        $this->payload = json_encode($payload, JSON_UNESCAPED_UNICODE);
    }

    public function markAsConversion(): void
    {
        $this->conversion    = true;
        $this->conversion_at = now();
        $this->save();
    }


}
