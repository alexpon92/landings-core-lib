<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ModifySellingReports extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::table('selling_reports', function (Blueprint $table) {
            $table->string('type')->default('write_off');

            $table->string('transaction_amount')->nullable()->change();
            $table->string('transaction_currency')->nullable()->change();
            $table->string('transaction_local_amount')->nullable()->change();
            $table->string('transaction_local_currency')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::table('selling_reports', function (Blueprint $table) {
            $table->dropColumn('type');

            $table->string('transaction_amount')->nullable(false)->change();
            $table->string('transaction_currency')->nullable(false)->change();
            $table->string('transaction_local_amount')->nullable(false)->change();
            $table->string('transaction_local_currency')->nullable(false)->change();
        });
    }
}
