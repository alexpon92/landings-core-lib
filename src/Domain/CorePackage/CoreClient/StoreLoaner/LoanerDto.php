<?php
declare(strict_types=1);

namespace LandingsCore\Domain\CorePackage\CoreClient\StoreLoaner;

class LoanerDto
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var PersonalDataDto
     */
    private $personalData;

    /**
     * LoanerDto constructor.
     *
     * @param int             $id
     * @param PersonalDataDto $personalData
     */
    public function __construct(int $id, PersonalDataDto $personalData)
    {
        $this->id           = $id;
        $this->personalData = $personalData;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return PersonalDataDto
     */
    public function getPersonalData(): PersonalDataDto
    {
        return $this->personalData;
    }
}