<?php

namespace LandingsCore\Console\Commands;

use LandingsCore\Domain\CorePackage\CoreClient\ClientExceptions\BadResponseCode;
use LandingsCore\Domain\CorePackage\CoreClient\ClientExceptions\BadResponseFormat;
use LandingsCore\Domain\Services\Cabinet\SyncLoanersFeed;
use Illuminate\Console\Command;

class SynchLoanersFeedCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'core:synch-loaners-feed';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Synch loaners event feed from core';

    /**
     * @param SyncLoanersFeed $service
     *
     * @throws BadResponseCode
     * @throws BadResponseFormat
     */
    public function handle(SyncLoanersFeed $service)
    {
        $service->execute();
    }
}
