<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class SessionLoanerInfo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sessions', function (Blueprint $table) {
             $table->bigInteger('core_loaner_id')->nullable();
             $table->bigInteger('core_personal_data_id')->nullable();
             $table->string('core_hash', 10)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sessions', function (Blueprint $table) {
            $table->dropColumn('core_loaner_id');
            $table->dropColumn('core_personal_data_id');
            $table->dropColumn('core_hash');
        });
    }
}
