<?php
declare(strict_types=1);

namespace LandingsCore\Domain\Dto;

use Carbon\Carbon;

class OfferVendorDuplicateCheck implements \JsonSerializable
{
    public const STATUS_EXISTS     = 'exists';
    public const STATUS_NOT_EXISTS = 'not_exists';

    /**
     * @var string
     */
    private $status;

    /**
     * @var string
     */
    private $serviceKey;

    /**
     * @var Carbon
     */
    private $checkTime;

    /**
     * OfferVendorDuplicateCheck constructor.
     *
     * @param string $status
     * @param string $serviceKey
     * @param Carbon $checkTime
     */
    public function __construct(string $status, string $serviceKey, Carbon $checkTime)
    {
        $this->status     = $status;
        $this->serviceKey = $serviceKey;
        $this->checkTime  = $checkTime;
    }

    /**
     * @return string
     */
    public function getStatus(): string
    {
        return $this->status;
    }

    /**
     * @return string
     */
    public function getServiceKey(): string
    {
        return $this->serviceKey;
    }

    /**
     * @return Carbon
     */
    public function getCheckTime(): Carbon
    {
        return $this->checkTime;
    }

    public function jsonSerialize()
    {
        return [
            'status'     => $this->status,
            'service'    => $this->serviceKey,
            'check_time' => $this->checkTime->toDateTimeString()
        ];
    }


}