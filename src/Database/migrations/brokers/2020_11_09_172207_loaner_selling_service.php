<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class LoanerSellingService extends Migration
{
    /**
     * {@inheritDoc}
     */
    public function up(): void
    {
        Schema::create('loaner_selling_services', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('external_id');
            $table->string('type');
            $table->string('name')->nullable();
            $table->string('description')->nullable();
            $table->string('logo')->nullable();
            $table->timestamps();
        });
    }

    /**
     * {@inheritDoc}
     */
    public function down(): void
    {
        Schema::dropIfExists('loaner_selling_services');
    }
}
