<?php

declare(strict_types=1);

namespace LandingsCore\Domain\CorePackage\CoreClient;

use Carbon\Carbon;

class TrafficSourceDto
{
    private int     $id;
    private string  $name;
    private ?string $description;
    private ?string $type;
    private ?string $access_type;
    private Carbon  $createdAt;

    /**
     * TrafficSourceDto constructor.
     *
     * @param int         $id
     * @param string      $name
     * @param string|null $description
     * @param string|null $type
     * @param string|null $access_type
     * @param Carbon      $createdAt
     */
    public function __construct(
        int $id,
        string $name,
        ?string $description,
        ?string $type,
        ?string $access_type,
        Carbon $createdAt
    ) {
        $this->id          = $id;
        $this->name        = $name;
        $this->description = $description;
        $this->type        = $type;
        $this->access_type = $access_type;
        $this->createdAt   = $createdAt;
    }


    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @return Carbon
     */
    public function getCreatedAt(): Carbon
    {
        return $this->createdAt;
    }

    /**
     * @return string|null
     */
    public function getType(): ?string
    {
        return $this->type;
    }

    /**
     * @return string|null
     */
    public function getAccessType(): ?string
    {
        return $this->access_type;
    }
}