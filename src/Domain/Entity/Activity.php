<?php
declare(strict_types=1);

namespace LandingsCore\Domain\Entity;

use Illuminate\Database\Eloquent\Model;

class Activity extends Model
{
    protected $guarded = ['id'];

    public function setCities(array $cities): void
    {
        $this->city = json_encode($cities, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);
    }

    public function setNames(array $names): void
    {
        $this->names = json_encode($names, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);
    }

    public function setAmounts(array $amounts): void
    {
        $this->amount = json_encode($amounts);
    }

}
