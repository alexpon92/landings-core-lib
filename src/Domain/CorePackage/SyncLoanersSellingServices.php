<?php
declare(strict_types=1);

namespace LandingsCore\Domain\CorePackage;

use LandingsCore\Domain\CorePackage\CoreClient\ClientExceptions\BadResponseCode;
use LandingsCore\Domain\CorePackage\CoreClient\ClientExceptions\BadResponseFormat;
use LandingsCore\Domain\CorePackage\CoreClient\ICoreClient;
use LandingsCore\Domain\CorePackage\CoreClient\SellingServices\SellingServiceDto;
use LandingsCore\Domain\Entity\LoanerSellingService;

class SyncLoanersSellingServices
{
    /**
     * @var ICoreClient
     */
    private $client;

    /**
     * @var ImgStorageService
     */
    private $imgStorageService;

    /**
     * SyncLoanersSellingServices constructor.
     * @param ICoreClient       $client
     * @param ImgStorageService $imgStorageService
     */
    public function __construct(ICoreClient $client, ImgStorageService $imgStorageService)
    {
        $this->client            = $client;
        $this->imgStorageService = $imgStorageService;
    }

    /**
     * @throws BadResponseCode
     * @throws BadResponseFormat
     */
    public function execute(): void
    {
        $services   = $this->client->getLoanerSellingServices();
        $serviceIds = array_map(
            static function (SellingServiceDto $elem) {
                return $elem->getExternalId();
            },
            $services
        );

        $time = time();

        /** @var SellingServiceDto $service */
        foreach ($services as $service) {
            $imgToDelete = null;

            $sellingServiceEntity = LoanerSellingService::findByExternalId($service->getExternalId());
            if (!$sellingServiceEntity) {
                $sellingServiceEntity = new LoanerSellingService();
            }

            $sellingServiceEntity->external_id = $service->getExternalId();
            $sellingServiceEntity->name        = $service->getName();
            $sellingServiceEntity->type        = $service->getType();
            $sellingServiceEntity->description = $service->getDescription();

            if (null !== $service->getLogoUrl()) {
                $imgUrl = $this->imgStorageService->storeSellingServiceImage(
                    $service->getLogoUrl(),
                    $service->getExternalId()
                );

                $sellingServiceEntity->logo = $imgUrl . '?t=' . $time;
            }
            $sellingServiceEntity->save();
        }

        $sellingServiceQuery = LoanerSellingService::whereNotIn('external_id', $serviceIds);
        $sellingServiceToDelete = $sellingServiceQuery->get();

        /** @var LoanerSellingService $item */
        foreach ($sellingServiceToDelete as $item) {
            [$path, $stamp] = explode('?', $item->logo);
            $this->imgStorageService->deleteImage($path);
        }

        $sellingServiceQuery->delete();

        LoanerSellingService::whereNotIn('external_id', $serviceIds)->delete();

    }

}