<?php
declare(strict_types=1);

namespace LandingsCore\Domain\Services\OfferVendorsDuplicatesCheck\CheckServices;

class VendorsEnum
{
    public const TYPE_RU_CREDIT_PLUS = 'creditplus';
}