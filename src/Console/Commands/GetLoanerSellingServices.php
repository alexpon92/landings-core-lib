<?php

namespace LandingsCore\Console\Commands;


use LandingsCore\Domain\CorePackage\CoreClient\ClientExceptions\BadResponseCode;
use LandingsCore\Domain\CorePackage\CoreClient\ClientExceptions\BadResponseFormat;
use Illuminate\Console\Command;
use LandingsCore\Domain\CorePackage\SyncLoanersSellingServices;

class GetLoanerSellingServices extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'core:get-loaner-selling-services';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Synchronize loaner selling services from core';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @param SyncLoanersSellingServices $syncLoanersSellingServices
     * @throws BadResponseCode
     * @throws BadResponseFormat
     */
    public function handle(SyncLoanersSellingServices $syncLoanersSellingServices)
    {
        $syncLoanersSellingServices->execute();
    }
}
