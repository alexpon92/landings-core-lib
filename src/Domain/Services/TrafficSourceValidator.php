<?php
declare(strict_types=1);

namespace LandingsCore\Domain\Services;

use LandingsCore\Domain\Entity\TrafficSource;

class TrafficSourceValidator
{
    public function isValidSource(?string $utmSource, ?string $umtCampaign, ?string $utmMedium): bool
    {
        if (null === $umtCampaign || null === $utmSource || null === $utmMedium) {
            return false;
        }

        $trafficSources = TrafficSource::getAll()
            ->groupBy('name')
            ->all();

        return isset($trafficSources[$utmSource]);
    }
}