<?php

declare(strict_types=1);

namespace LandingsCore\Domain\CorePackage\Enums;

final class OffersOrderingEnum
{
    public const PROBABILITY = 'probability';
    public const EPC = 'epc';

    public static function getAll(): array
    {
        return [
            self::PROBABILITY,
            self::EPC,
        ];
    }
}
