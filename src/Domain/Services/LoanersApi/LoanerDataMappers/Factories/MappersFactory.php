<?php
declare(strict_types=1);

namespace LandingsCore\Domain\Services\LoanersApi\LoanerDataMappers\Factories;

use LandingsCore\Domain\Services\LoanersApi\LoanerDataMappers\IMapper;
use LandingsCore\Domain\Services\LoanersApi\LoanerDataMappers\Mappers\RuDataMapper;
use LandingsCore\Domain\Services\LoanersApi\LoanerDataMappers\Mappers\UaDataMapper;

class MappersFactory
{
    public function buildByGeo(string $geo): ?IMapper
    {
        switch ($geo) {
            case 'UA':
                return new UaDataMapper();
            case 'RU':
                return new RuDataMapper();
            default:
                return null;
        }
    }
}