<?php
declare(strict_types=1);

namespace LandingsCore\Domain\CorePackage\CoreClient;

use Carbon\Carbon;
use LandingsCore\Domain\CorePackage\CoreClient\ClientExceptions\BadResponseCode;
use LandingsCore\Domain\CorePackage\CoreClient\ClientExceptions\BadResponseFormat;
use LandingsCore\Domain\CorePackage\CoreClient\StoreLoaner\LoanerDto as LoanerResponse;

interface ICoreClient
{
    /**
     * @return array|LeaveReasonDto[]
     * @throws BadResponseCode
     * @throws BadResponseFormat
     */
    public function getLeaveReasons(): array;

    /**
     * @return array|OfferDto[]
     * @throws BadResponseCode
     * @throws BadResponseFormat
     */
    public function getOffers(): array;

    /**
     * @return OfferProfitabilityDataDto[]
     */
    public function getOffersProfitability(): array;

    /**
     * @param LoanerDto $loanerDto
     *
     * @throws BadResponseCode
     * @throws BadResponseFormat
     */
    public function storeLoaner(LoanerDto $loanerDto): ?LoanerResponse;

    /**
     * @param int|null $limit
     *
     * @return array
     * @throws BadResponseCode
     * @throws BadResponseFormat
     */
    public function getSignedOffLoaners(?int $limit = 500): array;

    /**
     * @param string $phone
     * @param string $type
     *
     * @throws BadResponseCode
     * @throws BadResponseFormat
     */
    public function signOffLoaner(string $phone, string $type): void;

    /**
     * Validate email on core
     *
     * @param string $email
     *
     * @return bool
     */
    public function validateEmail(string $email): bool;

    /**
     * @param string     $phone
     * @param string     $vendorId
     * @param string     $token
     * @param string     $amount
     * @param string     $commissionAmount
     * @param string     $currency
     * @param string     $paymentSystem
     * @param array|null $data
     * @param Carbon     $dateTime
     *
     * @return void
     */
    public function storeTransaction(
        string $phone,
        string $vendorId,
        string $token,
        string $amount,
        string $commissionAmount,
        string $currency,
        string $paymentSystem,
        ?array $data,
        Carbon $dateTime
    ): void;

    public function storeFailedTransaction(
        string $phone,
        string $amount,
        string $currency,
        string $vendorId,
        string $type,
        string $failReason,
        ?string $cardFirstSix,
        ?string $cardLastFour,
        string $paymentSystem,
        ?array $data,
        Carbon $dateTime
    ): void;

    /**
     * @param string      $sessionId
     * @param string      $utmSource
     * @param string|null $utmMedium
     * @param string|null $utmCampaign
     * @param string|null $landingUri
     * @param string|null $referer
     * @param string|null $ipAddress
     * @param array|null  $data
     * @param Carbon      $visitedAt
     */
    public function storeTransition(
        string $sessionId,
        string $utmSource,
        ?string $utmMedium,
        ?string $utmCampaign,
        ?string $landingUri,
        ?string $referer,
        ?string $ipAddress,
        ?array $data,
        Carbon $visitedAt
    ): void;

    /**
     * @return array
     */
    public function getLandingSettings(): array;

    /**
     * @param string $phone
     *
     * @throws BadResponseCode
     * @throws BadResponseFormat
     */
    public function signOffLoanerSmsSubscription(string $phone): void;

    /**
     * @return array|TrafficSourceDto[]
     *
     * @throws BadResponseCode
     * @throws BadResponseFormat
     */
    public function getTrafficSources(): array;

    /**
     * @param int|null $lastId
     *
     * @return array
     *
     * @throws BadResponseCode
     * @throws BadResponseFormat
     */
    public function getLoanersFeed(?int $lastId): array;

    /**
     * Track offer click
     *
     * @param int         $offerId
     * @param Carbon      $triggeredAt
     * @param string|null $sessionId
     * @param string|null $utmSource
     * @param string|null $utmCampaign
     * @param string|null $utmMedium
     */
    public function trackOfferClick(
        int $offerId,
        Carbon $triggeredAt,
        ?string $sessionId = null,
        ?string $utmSource = null,
        ?string $utmCampaign = null,
        ?string $utmMedium = null
    ): void;

    /**
     * @return array
     */
    public function getLoanerSellingServices(): array;

    /**
     * @param int|null $lastId
     * @return array
     */
    public function getSellingReports(?int $lastId): array;

    public function checkIp(string $ip): IPCheckResponseDto;
}
