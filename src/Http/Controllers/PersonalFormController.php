<?php
declare(strict_types=1);

namespace LandingsCore\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use LandingsCore\Domain\Entity\Session;

class PersonalFormController extends Controller
{
    public function redirectWithData(Request $request, $id)
    {
        $session = Session::findApiByLoanerHash($id);
        if ($session) {
            $payload = $session->getPayloadAsDotArray();
            foreach ($payload as $key => $item) {
                if ($key === 'utm_medium') {
                    continue;
                }
                $request->session()->put($key, $item);
            }
        }

        return view('landings-core-lib::web.redirect', [
            'route' => route(config('landings-core-lib.loaners_api.form_route_name'))
        ]);
    }
}