<?php
declare(strict_types=1);

namespace LandingsCore\Services;

use LandingsCore\Mail\Feedback;
use Illuminate\Support\Facades\Mail;

class FeedbackService
{
    public function execute(string $phone, string $name, string $message, ?string $email = null): void
    {
        Mail::to(config('landings-core-lib.feedback_email'))
            ->send(new Feedback(
                    [
                        'phone'   => $phone,
                        'name'    => $name,
                        'email'   => $email ?? ' - ',
                        'message' => $message,
                    ]
                )
            );
    }

}
