<?php
declare(strict_types=1);

namespace LandingsCore\Domain\Services\LoanersApi\LoanerDataMappers;

use LandingsCore\Domain\Entity\Session;

interface IMapper
{
    public function map(array $data, Session $session): Session;
}