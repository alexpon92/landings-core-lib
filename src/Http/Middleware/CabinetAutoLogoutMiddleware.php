<?php
declare(strict_types=1);

namespace LandingsCore\Http\Middleware;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Closure;
use LandingsCore\Domain\Entity\Session;
use Illuminate\Support\Facades\Auth;

class CabinetAutoLogoutMiddleware
{

    public function handle(Request $request, Closure $next)
    {

        $sessionEntity = Session::find($request->session()->getId());

        if (null !== $sessionEntity) {

            $logoutBorder = Carbon::now()->sub(new \DateInterval(config('landings-core-lib.sessions.cabinet_logout_idle')))->timestamp;

            if ($sessionEntity->last_activity > $logoutBorder && Auth::check()) {
                Auth::logout();
            }

        }

        return $next($request);
    }
}
