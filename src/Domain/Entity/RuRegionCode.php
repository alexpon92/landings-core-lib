<?php

namespace LandingsCore\Domain\Entity;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Domain\Entity\Loaners\RuRegionCode
 *
 * @property int $id
 * @property string $region_name
 * @property string $gibdd_code
 * @property string $okato_code
 * @property string|null $oktmo_code
 * @method static Builder|RuRegionCode newModelQuery()
 * @method static Builder|RuRegionCode newQuery()
 * @method static Builder|RuRegionCode query()
 * @method static Builder|RuRegionCode whereGibddCode($value)
 * @method static Builder|RuRegionCode whereId($value)
 * @method static Builder|RuRegionCode whereOkatoCode($value)
 * @method static Builder|RuRegionCode whereOktmoCode($value)
 * @method static Builder|RuRegionCode whereRegionName($value)
 * @mixin \Eloquent
 */
class RuRegionCode extends Model
{
    protected $table   = 'ru_region_codes';
    public $timestamps = false;

    public static function checkOkatoCode(string $region): bool
    {
        return self::query()->where('okato_code', $region)->exists();
    }

    public static function checkGibddCode(string $region): bool
    {
        return self::query()->where('gibdd_code', $region)->exists();
    }
}
