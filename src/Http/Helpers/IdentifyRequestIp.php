<?php
declare(strict_types=1);

namespace LandingsCore\Http\Helpers;

class IdentifyRequestIp
{
    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return string
     */
    public function getIp($request): ?string
    {
        if ($request->header('CF-Connecting-IP')) {
            return $this->resolveIp($request->header('CF-Connecting-IP'));
        }

        if ($request->header('X-Forwarded-For')) {
            return $this->resolveIp($request->header('X-Forwarded-For'));
        }

        if ($request->ip()) {
            return $this->resolveIp($request->ip());
        }

        return null;
    }

    private function resolveIp(string $ip): ?string
    {
        $explodedIp = explode(',', $ip);
        return empty($explodedIp[0]) ? null : $explodedIp[0];
    }
}