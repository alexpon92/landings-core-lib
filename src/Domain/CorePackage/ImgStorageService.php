<?php
declare(strict_types=1);

namespace LandingsCore\Domain\CorePackage;

use Psr\Log\LoggerInterface;
use RuntimeException;

class ImgStorageService
{
    /**
     * @var LoggerInterface
     */
    private $logger;

    public const OFFERS_IMG_DIR_NAME           = 'offers-img';
    public const SELLING_SERVICES_IMG_DIR_NAME = 'selling-services-img';

    /**
     * ImgStorageService constructor.
     *
     * @param LoggerInterface $logger
     */
    public function __construct(LoggerInterface $logger)
    {
        $this->logger      = $logger;
    }

    /**
     * @param string $base64Content
     * @param int    $offerId
     *
     * @return string
     */
    public function storeOfferImage(string $base64Content, int $offerId): string
    {
        $storagePath = storage_path('app/public/' . self::OFFERS_IMG_DIR_NAME);
        $explodedContent = explode(',', $base64Content, 2);
        if (empty($explodedContent) || !isset($explodedContent[1])) {
            $this->logger->error('Bad offer image format', ['offer_id' => $offerId]);
            throw new RuntimeException('Bad image content');
        }

        $decodedImgString = base64_decode($explodedContent[1]);

        $res = getimagesizefromstring($decodedImgString);
        if ($res === false) {
            $this->logger->error('Cannot get image size from string', ['offer_id' => $offerId]);
            throw new RuntimeException('Cannot get image size from string');
        }

        $extension = explode('/', $res['mime'])[1];

        if (!is_dir($storagePath) && !mkdir($storagePath) && !is_dir($storagePath)) {
            $this->logger->error('Cannot create image store path', ['dir' => $storagePath]);
            throw new \RuntimeException(sprintf('Directory "%s" was not created', $storagePath));
        }

        file_put_contents("{$storagePath}/{$offerId}.{$extension}", $decodedImgString);
        return '/' . self::OFFERS_IMG_DIR_NAME . "/{$offerId}.{$extension}";
    }

    public function storeSellingServiceImage(string $imageUrl, int $serviceId): string
    {
        $storagePath = storage_path('app/public/' . self::SELLING_SERVICES_IMG_DIR_NAME);

        $extension = pathinfo($imageUrl, PATHINFO_EXTENSION);

        if (!is_dir($storagePath) && !mkdir($storagePath) && !is_dir($storagePath)) {
            $this->logger->error('Cannot create image selling services path', ['dir' => $storagePath]);
            throw new \RuntimeException(sprintf('Directory "%s" was not created', $storagePath));
        }

        $image = file_get_contents($imageUrl);
        file_put_contents("{$storagePath}/{$serviceId}.{$extension}", $image);

        return '/' . self::SELLING_SERVICES_IMG_DIR_NAME . "/{$serviceId}.{$extension}";
    }

    /**
     * @param string $imagePath
     */
    public function deleteImage(string $imagePath): void
    {
        if (is_file(storage_path('app/public/' . $imagePath))) {
            unlink(storage_path('app/public/' . $imagePath));
        }
    }
}