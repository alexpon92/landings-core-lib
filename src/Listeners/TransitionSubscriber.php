<?php
declare(strict_types=1);

namespace LandingsCore\Listeners;

use LandingsCore\Domain\CorePackage\CoreClient\ICoreClient;
use LandingsCore\Domain\Entity\Session;
use LandingsCore\Domain\Events\TransitionEvent;
use Psr\Log\LoggerInterface;

class TransitionSubscriber extends AbstractListener
{
    /**
     * @var ICoreClient
     */
    private $coreClient;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * LoanerSubscriber constructor.
     *
     * @param ICoreClient     $coreClient
     * @param LoggerInterface $logger
     */
    public function __construct(ICoreClient $coreClient, LoggerInterface $logger)
    {
        $this->coreClient = $coreClient;
        $this->logger     = $logger;
    }

    public function onTransition(TransitionEvent $event): void
    {
        $session = Session::find($event->getSessionId());

        if (null === $session) {
            return;
        }

        $this->coreClient->storeTransition(
            $event->getSessionId(),
            $event->getUtmSource(),
            $event->getUtmMedium(),
            $event->getUtmCampaign(),
            $event->getLandingUri(),
            $event->getReferer(),
            $event->getIpAddress(),
            $event->getData(),
            $event->getVisitedAt()
        );
    }
    public function getConnection(): string
    {
        return (string)config('landings-core-lib.transition_send_queue.connection');
    }

    public function getQueue(): string
    {
        return (string)config('landings-core-lib.transition_send_queue.name');
    }

    /**
     * Register the listeners for the subscriber.
     *
     * @param \Illuminate\Events\Dispatcher $events
     */
    public function subscribe($events)
    {
        $events->listen(
            TransitionEvent::class,
            self::class . '@onTransition'
        );
    }
}
