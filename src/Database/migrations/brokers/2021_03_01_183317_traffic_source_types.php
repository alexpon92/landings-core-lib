<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TrafficSourceTypes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('traffic_sources', function (Blueprint $table) {
            $table->string('type')->nullable();
            $table->string('access_type')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('traffic_sources', function (Blueprint $table) {
            $table->dropColumn('type');
            $table->dropColumn('access_type');
        });
    }
}
