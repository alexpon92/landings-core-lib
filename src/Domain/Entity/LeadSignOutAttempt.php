<?php
declare(strict_types=1);

namespace LandingsCore\Domain\Entity;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * App\Domain\Entity\LeadSignOutAttempt
 *
 * @property string $phone
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|LeadSignOutAttempt newModelQuery()
 * @method static Builder|LeadSignOutAttempt newQuery()
 * @method static Builder|LeadSignOutAttempt query()
 * @method static Builder|LeadSignOutAttempt whereCreatedAt($value)
 * @method static Builder|LeadSignOutAttempt wherePhone($value)
 * @method static Builder|LeadSignOutAttempt whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property int $id
 * @method static Builder|LeadSignOutAttempt whereId($value)
 */
class LeadSignOutAttempt extends Model
{
    public static function createBulkByPhones(array $phones): void
    {
        $now = now()->toDateTimeString();

        $insertArray = array_map(
            static function (string $phone) use ($now) {
                return [
                    'phone'      => $phone,
                    'created_at' => $now,
                    'updated_at' => $now
                ];
            },
            $phones
        );

        self::insert($insertArray);
    }

    public static function getLastSignOutAttempt(string $phone): ?self
    {
        /** @var LeadSignOutAttempt|null $leadCardLink */
        $leadCardLink = self::query()
                            ->where('phone', $phone)
                            ->orderBy('created_at', 'DESC')
                            ->first();

        return $leadCardLink;
    }
}