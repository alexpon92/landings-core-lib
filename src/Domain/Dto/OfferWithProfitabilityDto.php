<?php
declare(strict_types=1);

namespace LandingsCore\Domain\Dto;

use LandingsCore\Domain\Entity\Offer;

final class OfferWithProfitabilityDto
{
    /**
     * @var Offer
     */
    private $offer;

    /**
     * @var float|null
     */
    private $epc;

    /**
     * @var int
     */
    private $displayProbability;

    /**
     * OfferWithProfitabilityDto constructor.
     *
     * @param Offer      $offer
     * @param float|null $epc
     */
    public function __construct(Offer $offer, ?float $epc)
    {
        $this->offer = $offer;
        $this->epc = $epc ?? (float) $offer->epc;
    }

    public function getOffer(): Offer
    {
        return $this->offer;
    }

    public function getEpc(): float
    {
        return (float) $this->epc;
    }

    public function getDisplayProbability(): int
    {
        return $this->displayProbability;
    }

    public function setDisplayProbability(int $displayProbability): OfferWithProfitabilityDto
    {
        $this->displayProbability = $displayProbability;

        return $this;
    }
}
