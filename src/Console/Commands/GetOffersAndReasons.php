<?php

namespace LandingsCore\Console\Commands;


use LandingsCore\Domain\CorePackage\SynchOffersService;
use LandingsCore\Domain\CorePackage\SynchReasonsService;
use Illuminate\Console\Command;

class GetOffersAndReasons extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'core:get-offers-and-reasons';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Synchronize offers and leave reasons from core';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @param SynchReasonsService $synchReasonsService
     * @param SynchOffersService  $synchOffersService
     *
     * @throws \Exception
     */
    public function handle(SynchReasonsService $synchReasonsService, SynchOffersService $synchOffersService)
    {
        $synchReasonsService->synch();
        $synchOffersService->synch();
    }
}
